package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.GovernmentMortgages;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetGovernmentMortgages;
import com.coherentlogic.cusip.global.services.model.GetGovernmentMortgagesResponse;
import com.coherentlogic.cusip.global.services.model.GovernmentMortgageRequest;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GovernmentMortgagesBuilder
    extends AbstractQueryBuilder<GovernmentMortgages, GetGovernmentMortgages, GetGovernmentMortgagesResponse> {

    private static final Logger log = LoggerFactory.getLogger(GovernmentMortgagesBuilder.class);

    @Autowired
    public GovernmentMortgagesBuilder(
        @Qualifier(CacheConfiguration.GOVERNMENT_MORTGAGES_CACHE)
            CacheServiceProviderSpecification<String, GovernmentMortgages> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetGovernmentMortgages, GetGovernmentMortgagesResponse>
            genericCGSQueryBuilder,
        InOutAdapterSpecification<GetGovernmentMortgagesResponse, GovernmentMortgages> adapter,
        GetGovernmentMortgages request) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public GovernmentMortgages newInstance() {
        return new GovernmentMortgages();
    }

    public GovernmentMortgagesBuilder withAgencyCode (String agencyCode) {

        log.info ("method begins; agencyCode: " + agencyCode);

        GetGovernmentMortgages request = getRequest();

        GovernmentMortgageRequest governmentMortgageRequest = request.getRequest();

        governmentMortgageRequest.setAgencyCode(agencyCode);

        return this;
    }

    public GovernmentMortgagesBuilder withParentCUSIP (String parentCUSIP) {

        log.info ("method begins; parentCUSIP: " + parentCUSIP);

        GetGovernmentMortgages request = getRequest();

        GovernmentMortgageRequest governmentMortgageRequest = request.getRequest();

        governmentMortgageRequest.setParentCUSIP(parentCUSIP);

        return this;
    }

    public GovernmentMortgagesBuilder withParentPoolNumber (String parentPoolNumber) {

        log.info ("method begins; parentPoolNumber: " + parentPoolNumber);

        GetGovernmentMortgages request = getRequest();

        GovernmentMortgageRequest governmentMortgageRequest = request.getRequest();

        governmentMortgageRequest.setParentPoolNumber(parentPoolNumber);

        return this;
    }

    public GovernmentMortgagesBuilder withPoolNumber (String poolNumber) {

        log.info ("method begins; poolNumber: " + poolNumber);

        GetGovernmentMortgages request = getRequest();

        GovernmentMortgageRequest governmentMortgageRequest = request.getRequest();

        governmentMortgageRequest.setPoolNumber(poolNumber);

        return this;
    }

    public GovernmentMortgagesBuilder withUnitNumber (String unitNumber) {

        log.info ("method begins; unitNumber: " + unitNumber);

        GetGovernmentMortgages request = getRequest();

        GovernmentMortgageRequest governmentMortgageRequest = request.getRequest();

        governmentMortgageRequest.setUnitNumber(unitNumber);

        return this;
    }
}
