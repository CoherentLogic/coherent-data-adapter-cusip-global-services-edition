package com.coherentlogic.coherent.data.adapter.cgs.core.configuration;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.coherentlogic.cusip.global.services.model.CurrenciesRequest;
import com.coherentlogic.cusip.global.services.model.CusipAttributesPortfolioRequest;
import com.coherentlogic.cusip.global.services.model.CusipPortfolioRequest;
import com.coherentlogic.cusip.global.services.model.CusipRequest;
import com.coherentlogic.cusip.global.services.model.CusipTickerRequest;
import com.coherentlogic.cusip.global.services.model.DomicilesRequest;
import com.coherentlogic.cusip.global.services.model.GetCurrencies;
import com.coherentlogic.cusip.global.services.model.GetCusipAttributesPortfolioData;
import com.coherentlogic.cusip.global.services.model.GetCusipData;
import com.coherentlogic.cusip.global.services.model.GetCusipDataForTicker;
import com.coherentlogic.cusip.global.services.model.GetCusipPortfolioData;
import com.coherentlogic.cusip.global.services.model.GetDomiciles;
import com.coherentlogic.cusip.global.services.model.GetGovernmentMortgages;
import com.coherentlogic.cusip.global.services.model.GetInternationalIdentifiers;
import com.coherentlogic.cusip.global.services.model.GetIssueAttributes;
import com.coherentlogic.cusip.global.services.model.GetLinkedIssuesData;
import com.coherentlogic.cusip.global.services.model.GetOptionsData;
import com.coherentlogic.cusip.global.services.model.GetSblData;
import com.coherentlogic.cusip.global.services.model.GetStates;
import com.coherentlogic.cusip.global.services.model.GovernmentMortgageRequest;
import com.coherentlogic.cusip.global.services.model.InternationalIdsRequest;
import com.coherentlogic.cusip.global.services.model.IssueAttributesRequest;
import com.coherentlogic.cusip.global.services.model.LinkedIssuesRequest;
import com.coherentlogic.cusip.global.services.model.ObjectFactory;
import com.coherentlogic.cusip.global.services.model.OptionsRequest;
import com.coherentlogic.cusip.global.services.model.SblRequest;
import com.coherentlogic.cusip.global.services.model.StatesRequest;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
public class RequestConfiguration {

    private final ObjectFactory objectFactory = new ObjectFactory();

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetCurrencies getGetCurrencies() {

        GetCurrencies getCurrenciesRequest = objectFactory.createGetCurrencies();

        CurrenciesRequest currenciesRequest = objectFactory.createCurrenciesRequest();

        getCurrenciesRequest.setRequest(currenciesRequest);

        return getCurrenciesRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetCusipAttributesPortfolioData getCusipAttributesPortfolioData() {

        GetCusipAttributesPortfolioData getCusipAttributesPortfolioData = objectFactory
            .createGetCusipAttributesPortfolioData();

        CusipAttributesPortfolioRequest request = objectFactory.createCusipAttributesPortfolioRequest();

        getCusipAttributesPortfolioData.setRequest(request);

        return getCusipAttributesPortfolioData;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetCusipData getCusipData() {

        GetCusipData getCusipDataRequest = objectFactory.createGetCusipData();

        CusipRequest cusipRequest = objectFactory.createCusipRequest();

        getCusipDataRequest.setCusipRequest(cusipRequest);

        return getCusipDataRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetCusipDataForTicker getCusipDataForTicker() {

        GetCusipDataForTicker request = objectFactory.createGetCusipDataForTicker();

        CusipTickerRequest cusipTickerRequest = objectFactory.createCusipTickerRequest();

        request.setRequest(cusipTickerRequest);

        return request;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetCusipPortfolioData getCusipPortfolioData() {

        GetCusipPortfolioData getCusipPortfolioDataRequest = objectFactory.createGetCusipPortfolioData();

        CusipPortfolioRequest cusipPortfolioRequest = objectFactory.createCusipPortfolioRequest();

        getCusipPortfolioDataRequest.setRequest(cusipPortfolioRequest);

        return getCusipPortfolioDataRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetStates getStates() {

        GetStates getStatesRequest = objectFactory.createGetStates();

        StatesRequest statesRequest = objectFactory.createStatesRequest();

        getStatesRequest.setRequest(statesRequest);

        return getStatesRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetDomiciles getDomiciles() {

        GetDomiciles getDomicilesRequest = objectFactory.createGetDomiciles();

        DomicilesRequest domicilesRequest = objectFactory.createDomicilesRequest();

        getDomicilesRequest.setRequest(domicilesRequest);

        return getDomicilesRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetGovernmentMortgages getGovernmentMortgages() {

        GetGovernmentMortgages getGovernmentMortgagesRequest = objectFactory.createGetGovernmentMortgages();

        GovernmentMortgageRequest governmentMortgageRequest = objectFactory.createGovernmentMortgageRequest();

        getGovernmentMortgagesRequest.setRequest(governmentMortgageRequest);

        return getGovernmentMortgagesRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetIssueAttributes getIssueAttributes() {

        GetIssueAttributes getIssueAttributesRequest = objectFactory.createGetIssueAttributes();

        IssueAttributesRequest issueAttributes = objectFactory.createIssueAttributesRequest();

        getIssueAttributesRequest.setRequest(issueAttributes);

        return getIssueAttributesRequest;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetLinkedIssuesData getGetLinkedIssuesData () {

        GetLinkedIssuesData getLinkedIssuesData = objectFactory.createGetLinkedIssuesData();

        LinkedIssuesRequest linkedIssuesRequest = objectFactory.createLinkedIssuesRequest();

        getLinkedIssuesData.setRequest(linkedIssuesRequest);

        return getLinkedIssuesData;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetOptionsData getGetOptionsData () {

        GetOptionsData getOptionsData = objectFactory.createGetOptionsData();

        OptionsRequest request = objectFactory.createOptionsRequest();

        getOptionsData.setRequest(request);

        return getOptionsData;
    }

    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetSblData getSblData () {

        GetSblData getSblDataRequest = objectFactory.createGetSblData();

        SblRequest sblRequest = objectFactory.createSblRequest();

        getSblDataRequest.setRequest(sblRequest);

        return getSblDataRequest;
    }

    /**
     * @deprecated This functionality is not supported by CGS.
     */
    @Deprecated
    @Bean
    @Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public GetInternationalIdentifiers getGetInternationalIdentifiers () {

        GetInternationalIdentifiers getInternationalIdentifiers = objectFactory.createGetInternationalIdentifiers();

        InternationalIdsRequest request = objectFactory.createInternationalIdsRequest();

        getInternationalIdentifiers.setRequest(request);

        return getInternationalIdentifiers;
    }
}
