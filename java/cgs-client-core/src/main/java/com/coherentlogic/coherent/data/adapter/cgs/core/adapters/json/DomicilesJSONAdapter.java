package com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domicile;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domiciles;
import com.coherentlogic.coherent.data.model.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.rproject.integration.dataframe.builders.JDataFrameBuilder;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DomicilesJSONAdapter implements InReturnAdapterSpecification<Domiciles, String> {

    @Override
    public String adapt(Domiciles domiciles) {

        List<String> codes = new ArrayList<String> ();
        List<String> descriptions = new ArrayList<String> ();
        List<String> isoCodes = new ArrayList<String> ();

        domiciles
            .getDomiciles()
            .forEach(
                (Domicile domicile) -> {
                    codes.add(domicile.getCode());
                    descriptions.add(domicile.getDescription());
                    isoCodes.add(domicile.getIsoCode());
                }
            );

        return (String) new JDataFrameBuilder (JDataFrameBuilder.DEFAULT_REMOTE_ADAPTER)
            .addColumn("Codes", codes.toArray())
            .addColumn("ISO Codes", isoCodes.toArray())
            .addColumn("Descriptions", descriptions.toArray())
            .serialize();
    }
}
