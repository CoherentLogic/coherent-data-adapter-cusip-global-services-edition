package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Currencies;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Currencies.CurrencyDetail;
import com.coherentlogic.cusip.global.services.model.CurrenciesResponse;
import com.coherentlogic.cusip.global.services.model.ErrorList;
import com.coherentlogic.cusip.global.services.model.GetCurrenciesResponse;

/**
 * 
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class CurrenciesAdapter extends AbstractAdapter<GetCurrenciesResponse, Currencies> {

    private static final Logger log = LoggerFactory.getLogger(CurrenciesAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetCurrenciesResponse source, Currencies currencies) {

        final List<CurrencyDetail> currencyDetails = currencies.getCurrencyDetails();

        CurrenciesResponse currenciesResponse = source.getReturn();

        ErrorList errorList = currenciesResponse.getErrorList();

        errorBeanAdapter.adapt(errorList, currencies);

        source
            .getReturn()
            .getCurrencyList()
            .getCurrencyDetail()
            .forEach (
                modelCurrency -> {
                    currencyDetails.add (
                        new CurrencyDetail (
                            Integer.valueOf(modelCurrency.getCurrencyCode()),
                            modelCurrency.getCurrencyDescr(),
                            modelCurrency.getCurrencyIso()
                        )
                    );
                }
            );
    }
}
