package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.SBLData;
import com.coherentlogic.cusip.global.services.model.ErrorList;
import com.coherentlogic.cusip.global.services.model.FacilityList;
import com.coherentlogic.cusip.global.services.model.GetSblDataResponse;

/**
 * 
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class SBLDataAdapter extends AbstractAdapter<GetSblDataResponse, SBLData> {

    private static final Logger log = LoggerFactory.getLogger(SBLDataAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetSblDataResponse source, SBLData target) {

        log.warn("adapt: method begins; source: " + source.getReturn().getDealList().getDealDetail().get(0).getAgent());

        ErrorList errorList = source.getReturn().getErrorList();

        errorBeanAdapter.adapt(errorList, target);

        source
            .getReturn()
            .getDealList()
            .getDealDetail()
            .forEach(
                entry -> {
                    com.coherentlogic.coherent.data.adapter.cgs.core.domain.DealDetail dealDetail =
                        new com.coherentlogic.coherent.data.adapter.cgs.core.domain.DealDetail ();

                    target.getDealDetails().add(dealDetail);

                    adapt (entry, dealDetail);
                }
            );
    }

    void adapt (
        com.coherentlogic.cusip.global.services.model.DealDetail mDealDetail,
        com.coherentlogic.coherent.data.adapter.cgs.core.domain.DealDetail dDealDetail
    ) {

        String agent = mDealDetail.getAgent ();

        log.debug ("agent: " +  agent);

        dDealDetail.setAgent(agent);

        String amendedCreditAgreementDate = mDealDetail.getAmendedCreditAgreementDate ();

        log.debug ("amendedCreditAgreementDate: " +  amendedCreditAgreementDate);

        dDealDetail.setAmendedCreditAgreementDate(amendedCreditAgreementDate);

        String borrowerIsoCountryCode = mDealDetail.getBorrowerIsoCountryCode ();

        log.debug ("borrowerIsoCountryCode: " +  borrowerIsoCountryCode);

        dDealDetail.setBorrowerIsoCountryCode(borrowerIsoCountryCode);

        String borrowerName = mDealDetail.getBorrowerName ();

        log.debug ("borrowerName: " +  borrowerName);

        dDealDetail.setBorrowerName(borrowerName);

        String borrowerStateCode = mDealDetail.getBorrowerStateCode ();

        log.debug ("borrowerStateCode: " +  borrowerStateCode);

        dDealDetail.setBorrowerStateCode(borrowerStateCode);

        String creditAgreementDate = mDealDetail.getCreditAgreementDate ();

        log.debug ("creditAgreementDate: " +  creditAgreementDate);

        dDealDetail.setCreditAgreementDate(creditAgreementDate);

        String dealAdl = mDealDetail.getDealAdl ();

        log.debug ("dealAdl: " +  dealAdl);

        dDealDetail.setDealAdl(dealAdl);

        String dealAmount = mDealDetail.getDealAmount ();

        log.debug ("dealAmount: " +  dealAmount);

        dDealDetail.setDealAmount(dealAmount);

        String dealEstimatedFlag = mDealDetail.getDealEstimatedFlag ();

        log.debug ("dealEstimatedFlag: " +  dealEstimatedFlag);

        dDealDetail.setDealEstimatedFlag(dealEstimatedFlag);

        String dealIsoCurrencyCode = mDealDetail.getDealIsoCurrencyCode ();

        log.debug ("dealIsoCurrencyCode: " +  dealIsoCurrencyCode);

        dDealDetail.setDealIsoCurrencyCode(dealIsoCurrencyCode);

        String dealLinkage = mDealDetail.getDealLinkage ();

        log.debug ("dealLinkage: " +  dealLinkage);

        dDealDetail.setDealLinkage(dealLinkage);

        String dealReleaseDate = mDealDetail.getDealReleaseDate ();

        log.debug ("dealReleaseDate: " +  dealReleaseDate);

        dDealDetail.setDealReleaseDate(dealReleaseDate);

        FacilityList facilityList
            = mDealDetail.getFacilityList ();

        log.debug ("facilityList: " +  facilityList);

        final List<com.coherentlogic.cusip.global.services.model.FacilityDetail> mFacilityDetails =
            facilityList.getFacilityDetail();

        final List<com.coherentlogic.coherent.data.adapter.cgs.core.domain.FacilityDetail> dFacilityDetails =
            dDealDetail.getFacilityDetails();

        mFacilityDetails.forEach(
            entry -> {

                com.coherentlogic.coherent.data.adapter.cgs.core.domain.FacilityDetail dFacilityDetail =
                    new com.coherentlogic.coherent.data.adapter.cgs.core.domain.FacilityDetail ();

                dFacilityDetails.add(dFacilityDetail);

                adapt(entry, dFacilityDetail);
            }
        );

        String issuerNumber = mDealDetail.getIssuerNumber ();

        log.debug ("issuerNumber: " +  issuerNumber);

        dDealDetail.setIssuerNumber(issuerNumber);

        String legalJurisdiction = mDealDetail.getLegalJurisdiction ();

        log.debug ("legalJurisdiction: " +  legalJurisdiction);

        dDealDetail.setLegalJurisdiction(legalJurisdiction);
    }

    void adapt (
        com.coherentlogic.cusip.global.services.model.FacilityDetail mFacilityDetail,
        com.coherentlogic.coherent.data.adapter.cgs.core.domain.FacilityDetail dFacilityDetail
    ) {
        String amount = mFacilityDetail.getAmount ();

        log.debug ("amount: " +  amount);

        dFacilityDetail.setAmount(amount);

        String facilityEstimatedFlag = mFacilityDetail.getFacilityEstimatedFlag ();

        log.debug ("facilityEstimatedFlag: " +  facilityEstimatedFlag);

        dFacilityDetail.setFacilityEstimatedFlag(facilityEstimatedFlag);

        String facilityPurpose = mFacilityDetail.getFacilityPurpose ();

        log.debug ("facilityPurpose: " +  facilityPurpose);

        dFacilityDetail.setFacilityPurpose(facilityPurpose);

        String facilityType = mFacilityDetail.getFacilityType ();

        log.debug ("facilityType: " +  facilityType);

        dFacilityDetail.setFacilityType(facilityType);

        String isoCurrencyCode = mFacilityDetail.getIsoCurrencyCode ();

        log.debug ("isoCurrencyCode: " +  isoCurrencyCode);

        dFacilityDetail.setIsoCurrencyCode(isoCurrencyCode);

        String issueCheck = mFacilityDetail.getIssueCheck ();

        log.debug ("issueCheck: " +  issueCheck);

        dFacilityDetail.setIssueCheck(issueCheck);

        String issueIsinNumber = mFacilityDetail.getIssueIsinNumber ();

        log.debug ("issueIsinNumber: " +  issueIsinNumber);

        dFacilityDetail.setIssueIsinNumber(issueIsinNumber);

        String issueMaturityDate = mFacilityDetail.getIssueMaturityDate ();

        log.debug ("issueMaturityDate: " +  issueMaturityDate);

        dFacilityDetail.setIssueMaturityDate(issueMaturityDate);

        String issueNumber = mFacilityDetail.getIssueNumber ();

        log.debug ("issueNumber: " +  issueNumber);

        dFacilityDetail.setIssueNumber(issueNumber);

        String issueStatus = mFacilityDetail.getIssueStatus ();

        log.debug ("issueStatus: " +  issueStatus);

        dFacilityDetail.setIssueStatus(issueStatus);

        String releaseDate = mFacilityDetail.getReleaseDate ();

        log.debug ("releaseDate: " +  releaseDate);

        dFacilityDetail.setReleaseDate(releaseDate);
    }
}
