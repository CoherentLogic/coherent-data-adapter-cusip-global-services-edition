package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 *
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class IssueDetail extends IdentityBean {

    private String issueAdditionalInfo;

    public void setIssueAdditionalInfo (String issueAdditionalInfo) {
        this.issueAdditionalInfo = issueAdditionalInfo;
    }

    public String getIssueAdditionalInfo () {
        return issueAdditionalInfo;
    }

    private String issueCheck;

    public void setIssueCheck (String issueCheck) {
        this.issueCheck = issueCheck;
    }

    public String getIssueCheck () {
        return issueCheck;
    }

    private String issueCurrencyCode;

    public void setIssueCurrencyCode (String issueCurrencyCode) {
        this.issueCurrencyCode = issueCurrencyCode;
    }

    public String getIssueCurrencyCode () {
        return issueCurrencyCode;
    }

    private String issueDate;

    public void setIssueDate (String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIssueDate () {
        return issueDate;
    }

    private String issueDatedDate;

    public void setIssueDatedDate (String issueDatedDate) {
        this.issueDatedDate = issueDatedDate;
    }

    public String getIssueDatedDate () {
        return issueDatedDate;
    }

    private String issueDescription;

    public void setIssueDescription (String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public String getIssueDescription () {
        return issueDescription;
    }

    private String issueDomicileCode;

    public void setIssueDomicileCode (String issueDomicileCode) {
        this.issueDomicileCode = issueDomicileCode;
    }

    public String getIssueDomicileCode () {
        return issueDomicileCode;
    }

    private String issueGroup;

    public void setIssueGroup (String issueGroup) {
        this.issueGroup = issueGroup;
    }

    public String getIssueGroup () {
        return issueGroup;
    }

    /**
     * An International Securities Identification Number (ISIN) uniquely identifies a security. Its structure is
     * defined in ISO 6166. Securities for which ISINs are issued include bonds, commercial paper, stocks and
     * warrants.
     *
     * @see <a href="https://en.wikipedia.org/wiki/International_Securities_Identification_Number">International
     * Securities Identification Number</a>
     *
     * @see <a href="http://www.isin.org/isin/">International Securities Identification Number (ISIN)</a>
     */
    private String issueISINNumber;

    public void setIssueISINNumber (String issueISINNumber) {
        this.issueISINNumber = issueISINNumber;
    }

    public String getIssueISINNumber () {
        return issueISINNumber;
    }

    private String issueLogDate;

    public void setIssueLogDate (String issueLogDate) {
        this.issueLogDate = issueLogDate;
    }

    public String getIssueLogDate () {
        return issueLogDate;
    }

    private String issueMaturityDate;

    public void setIssueMaturityDate (String issueMaturityDate) {
        this.issueMaturityDate = issueMaturityDate;
    }

    public String getIssueMaturityDate () {
        return issueMaturityDate;
    }

    private String issueNumber;

    public void setIssueNumber (String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public String getIssueNumber () {
        return issueNumber;
    }

    private String issuePartialMaturity;

    public void setIssuePartialMaturity (String issuePartialMaturity) {
        this.issuePartialMaturity = issuePartialMaturity;
    }

    public String getIssuePartialMaturity () {
        return issuePartialMaturity;
    }

    private String issueRate;

    public void setIssueRate (String issueRate) {
        this.issueRate = issueRate;
    }

    public String getIssueRate () {
        return issueRate;
    }

    private String issueReleaseDate;

    public void setIssueReleaseDate (String issueReleaseDate) {
        this.issueReleaseDate = issueReleaseDate;
    }

    public String getIssueReleaseDate () {
        return issueReleaseDate;
    }

    private String issueStatus;

    public void setIssueStatus (String issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getIssueStatus () {
        return issueStatus;
    }

    private String issueTypeCode;

    public void setIssueTypeCode (String issueTypeCode) {
        this.issueTypeCode = issueTypeCode;
    }

    public String getIssueTypeCode () {
        return issueTypeCode;
    }

    @Override
    public String toString() {
        return "IssueDetail [issueAdditionalInfo=" + issueAdditionalInfo + ", issueCheck=" + issueCheck
            + ", issueCurrencyCode=" + issueCurrencyCode + ", issueDate=" + issueDate + ", issueDatedDate="
            + issueDatedDate + ", issueDescription=" + issueDescription + ", issueDomicileCode="
            + issueDomicileCode + ", issueGroup=" + issueGroup + ", issueISINNumber=" + issueISINNumber
            + ", issueLogDate=" + issueLogDate + ", issueMaturityDate=" + issueMaturityDate + ", issueNumber="
            + issueNumber + ", issuePartialMaturity=" + issuePartialMaturity + ", issueRate=" + issueRate
            + ", issueReleaseDate=" + issueReleaseDate + ", issueStatus=" + issueStatus + ", issueTypeCode="
            + issueTypeCode + "]";
    }
}