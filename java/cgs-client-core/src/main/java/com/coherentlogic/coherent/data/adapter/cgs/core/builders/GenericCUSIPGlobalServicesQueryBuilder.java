package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.SoapMessage;

import com.coherentlogic.coherent.data.model.core.builders.soap.AbstractSOAPQueryBuilder;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <T>
 * @param <R>
 */
public class GenericCUSIPGlobalServicesQueryBuilder<T, R> extends AbstractSOAPQueryBuilder<T, R> {

    private static final Logger log = LoggerFactory.getLogger(GenericCUSIPGlobalServicesQueryBuilder.class);

    private final HeaderAugmentingCallback headerAugmentingCallback;

    public GenericCUSIPGlobalServicesQueryBuilder(
        WebServiceTemplate webServiceTemplate,
        CacheServiceProviderSpecification<T, R> cache,
        String userId,
        String binarySecurityToken
    ) {
        super(webServiceTemplate, cache);
        this.headerAugmentingCallback = new HeaderAugmentingCallback (userId, binarySecurityToken);
    }

    public GenericCUSIPGlobalServicesQueryBuilder(
        WebServiceTemplate webServiceTemplate,
        String userId,
        String binarySecurityToken
    ) {
        super(webServiceTemplate);
        this.headerAugmentingCallback = new HeaderAugmentingCallback (userId, binarySecurityToken);
    }

    @XmlRootElement(
        namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", name="Security")
    static class SecurityDefinition {

        @XmlElement(
            name="BinarySecurityToken",
            namespace="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")
        private BinarySecurityToken binarySecurityToken;

        public SecurityDefinition () {
        }

        public SecurityDefinition(BinarySecurityToken binarySecurityToken) {
            this.binarySecurityToken = binarySecurityToken;
        }
    }

    static class BinarySecurityToken {

        @XmlAttribute(name="ValueType")
        private String valueType;

        @XmlValue
        private String value;

        public BinarySecurityToken () {
        }

        public BinarySecurityToken(String valueType, String value) {
            this.valueType = valueType;
            this.value = value;
        }
    }

    static class HeaderAugmentingCallback implements WebServiceMessageCallback {

        private final String userId, binarySecurityToken;

        public HeaderAugmentingCallback(String userId, String binarySecurityToken) {
            this.userId = userId;
            this.binarySecurityToken = binarySecurityToken;
        }

        @Override
        public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {

            SoapMessage soapMessage = (SoapMessage) message;

            SoapHeader soapHeader = soapMessage.getSoapHeader();

            JAXBContext context = null;

            try {
                context = JAXBContext.newInstance (SecurityDefinition.class);
            } catch (JAXBException e) {
                throw new RuntimeException("Unable to create a newInstance of SecurityDefinition.", e);
            }

            SecurityDefinition securityDefinition =
                new SecurityDefinition(new BinarySecurityToken ("AccessManagerSSOSecurityToken", binarySecurityToken));

            try {
                context.createMarshaller().marshal(securityDefinition, soapHeader.getResult());
            } catch (JAXBException e) {
                throw new RuntimeException("Attempting to marshall the securityDefinition has failed!", e);
            }

            SoapHeaderElement userIdElement =
                soapHeader.addHeaderElement(new QName ("http://sp.com/idhub/schemas/ref", "ref:UserId"));

            if (userId == null)
                throw new NullPointerException("The userId is null!");

            userIdElement.setText(userId);
        }
    }

    /**
     * @see http://www.scriptscoop.net/t/7880e8675ebd/java-add-custom-element-to-soap-header-in-spring-ws.html
     * @see http://stackoverflow.com/questions/25292485/add-custom-element-to-soap-header-in-spring-ws
     * @see http://krams915.blogspot.com/2011/01/spring-ws-2-client-side-ws-security.html
     * http://stackoverflow.com/questions/3169237/setting-a-custom-http-header-dynamically-with-spring-ws-client
     */
    public R doGet(T request) {

        log.info("doGet: method begins.");

        WebServiceTemplate webServiceTemplate = getWebServiceTemplate();

        R result = (R) webServiceTemplate.marshalSendAndReceive(request, headerAugmentingCallback);

        log.info("doGet: method ends; result: " + result);

        return result;
    }

    /**
     * @TODO Refactor this class and remove this method.
     */
    @Override
    public R doGet() {
        throw new RuntimeException("Method not implemented in this class.");
    }
}
