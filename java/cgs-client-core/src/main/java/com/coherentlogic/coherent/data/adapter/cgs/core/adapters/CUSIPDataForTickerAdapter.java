package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPDataForTicker;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPTickerIssueDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPTickerIssuerDetails;
import com.coherentlogic.cusip.global.services.model.CusipTickerIssueDetail;
import com.coherentlogic.cusip.global.services.model.CusipTickerIssueList;
import com.coherentlogic.cusip.global.services.model.CusipTickerIssuerList;
import com.coherentlogic.cusip.global.services.model.CusipTickerResponse;
import com.coherentlogic.cusip.global.services.model.ErrorList;
import com.coherentlogic.cusip.global.services.model.GetCusipDataForTickerResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class CUSIPDataForTickerAdapter extends AbstractAdapter<GetCusipDataForTickerResponse, CUSIPDataForTicker> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPDataAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetCusipDataForTickerResponse source, CUSIPDataForTicker target) {

        log.info("adapt: method begins; source: " + source + ", target: " + target); 

        CusipTickerResponse cusipTickerResponse = source.getReturn();

        ErrorList errorList = cusipTickerResponse.getErrorList();

        errorBeanAdapter.adapt(errorList, target);

        String exchangeCode = cusipTickerResponse.getExchangeCode();

        String exchangeName = cusipTickerResponse.getExchangeName();

        String tickerSymbol = cusipTickerResponse.getTickerSymbol();

        target.setExchangeCode(exchangeCode);
        target.setExchangeName(exchangeName);
        target.setTickerSymbol(tickerSymbol);

        CusipTickerIssuerList cusipTickerIssuerList = cusipTickerResponse.getCusipTickerIssuerList();

        if (cusipTickerIssuerList != null) {

            cusipTickerIssuerList
                .getCusipTickerIssuerDetail()
                .forEach(
                    mCUSIPTickerIssuerDetail -> {

                        final CUSIPTickerIssuerDetails dCUSIPTickerIssuerDetails
                            = new CUSIPTickerIssuerDetails ();

                        target.getCUSIPTickerIssuerDetails().add(dCUSIPTickerIssuerDetails);

                        String description = mCUSIPTickerIssuerDetail.getIssuerDescription();
                        String number = mCUSIPTickerIssuerDetail.getIssuerNumber();
                        String status = mCUSIPTickerIssuerDetail.getIssuerStatus();

                        dCUSIPTickerIssuerDetails.setDescription(description);
                        dCUSIPTickerIssuerDetails.setNumber(Integer.valueOf(number));
                        dCUSIPTickerIssuerDetails.setStatus(status);

                        CusipTickerIssueList mCusipTickerIssueList
                            = mCUSIPTickerIssuerDetail.getCusipTickerIssueList();

                        mCusipTickerIssueList.getCusipTickerIssueDetail().forEach(mInnerCusipTickerIssueDetail -> {

                            CUSIPTickerIssueDetail dCUSIPTickerIssueDetail
                                = new CUSIPTickerIssueDetail ();

                            dCUSIPTickerIssuerDetails.getCUSIPTickerIssueDetails().add(dCUSIPTickerIssueDetail);

                            adapt (mInnerCusipTickerIssueDetail, dCUSIPTickerIssueDetail);
                        });
                    }
                );
        }

        log.info("adapt: method ends; target: " + target); 
    }

    void adapt (
        CusipTickerIssueDetail mCusipTickerIssueDetail,
        CUSIPTickerIssueDetail dCUSIPTickerIssuerDetail
    ) {
        String issueCheck = mCusipTickerIssueDetail.getIssueCheck();
        String issueDescription = mCusipTickerIssueDetail.getIssueDescription();
        String issueNumber = mCusipTickerIssueDetail.getIssueNumber();
        String issueStatus = mCusipTickerIssueDetail.getIssueStatus();

        dCUSIPTickerIssuerDetail.setIssueCheck(Integer.valueOf(issueCheck));
        dCUSIPTickerIssuerDetail.setDescription(issueDescription);
        dCUSIPTickerIssuerDetail.setNumber(Integer.valueOf(issueNumber));
        dCUSIPTickerIssuerDetail.setStatus(issueStatus);
    }
}
