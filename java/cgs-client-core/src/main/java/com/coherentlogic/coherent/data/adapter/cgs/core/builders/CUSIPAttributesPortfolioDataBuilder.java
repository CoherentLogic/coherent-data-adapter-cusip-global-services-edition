package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPAttributesPortfolioData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetCusipAttributesPortfolioData;
import com.coherentlogic.cusip.global.services.model.GetCusipAttributesPortfolioDataResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CUSIPAttributesPortfolioDataBuilder extends AbstractQueryBuilder<
    CUSIPAttributesPortfolioData, GetCusipAttributesPortfolioData, GetCusipAttributesPortfolioDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPAttributesPortfolioDataBuilder.class);

    @Autowired
    public CUSIPAttributesPortfolioDataBuilder(
        @Qualifier(CacheConfiguration.CUSIP_ATTRIBUTES_PORTFOLIO_DATA_CACHE)
            CacheServiceProviderSpecification<String, CUSIPAttributesPortfolioData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetCusipAttributesPortfolioData, GetCusipAttributesPortfolioDataResponse>
            genericCGSQueryBuilder,
            InOutAdapterSpecification<GetCusipAttributesPortfolioDataResponse, CUSIPAttributesPortfolioData> adapter,
        GetCusipAttributesPortfolioData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public CUSIPAttributesPortfolioDataBuilder withCUSIP (String cusip) {

        log.info ("method begins; cusip: " + cusip);

        GetCusipAttributesPortfolioData request = getRequest();

        request.getRequest().getCusip().add(cusip);

        return this;
    }

    @Override
    public CUSIPAttributesPortfolioData newInstance() {
        return new CUSIPAttributesPortfolioData ();
    }
}
