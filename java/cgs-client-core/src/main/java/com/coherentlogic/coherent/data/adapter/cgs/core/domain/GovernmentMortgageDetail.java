package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class GovernmentMortgageDetail extends IdentityBean {

    private static final long serialVersionUID = -8646577328435990937L;

    private String agencyCode;

    /**
     * Setter method for the agencyCode property.
     */
    public void setAgencyCode (String agencyCode) {
        this.agencyCode = agencyCode;
    }

    /**
     * Getter method for the agencyCode property.
     */
    public String getAgencyCode () {
        return agencyCode;
    }

    private String agencyDescription;

    /**
     * Setter method for the agencyDescription property.
     */
    public void setAgencyDescription (String agencyDescription) {
        this.agencyDescription = agencyDescription;
    }

    /**
     * Getter method for the agencyDescription property.
     */
    public String getAgencyDescription () {
        return agencyDescription;
    }

    private String issueCheck;

    /**
     * Setter method for the issueCheck property.
     */
    public void setIssueCheck (String issueCheck) {
        this.issueCheck = issueCheck;
    }

    /**
     * Getter method for the issueCheck property.
     */
    public String getIssueCheck () {
        return issueCheck;
    }

    private String issueCurrencyCode;

    /**
     * Setter method for the issueCurrencyCode property.
     */
    public void setIssueCurrencyCode (String issueCurrencyCode) {
        this.issueCurrencyCode = issueCurrencyCode;
    }

    /**
     * Getter method for the issueCurrencyCode property.
     */
    public String getIssueCurrencyCode () {
        return issueCurrencyCode;
    }

    private String issueDate;

    /**
     * Setter method for the issueDate property.
     */
    public void setIssueDate (String issueDate) {
        this.issueDate = issueDate;
    }

    /**
     * Getter method for the issueDate property.
     */
    public String getIssueDate () {
        return issueDate;
    }

    private String issueDescription;

    /**
     * Setter method for the issueDescription property.
     */
    public void setIssueDescription (String issueDescription) {
        this.issueDescription = issueDescription;
    }

    /**
     * Getter method for the issueDescription property.
     */
    public String getIssueDescription () {
        return issueDescription;
    }

    private String issueDomicileCode;

    /**
     * Setter method for the issueDomicileCode property.
     */
    public void setIssueDomicileCode (String issueDomicileCode) {
        this.issueDomicileCode = issueDomicileCode;
    }

    /**
     * Getter method for the issueDomicileCode property.
     */
    public String getIssueDomicileCode () {
        return issueDomicileCode;
    }

    private String issueGroup;

    /**
     * Setter method for the issueGroup property.
     */
    public void setIssueGroup (String issueGroup) {
        this.issueGroup = issueGroup;
    }

    /**
     * Getter method for the issueGroup property.
     */
    public String getIssueGroup () {
        return issueGroup;
    }

    private String issueIsinNumber;

    /**
     * Setter method for the issueIsinNumber property.
     */
    public void setIssueIsinNumber (String issueIsinNumber) {
        this.issueIsinNumber = issueIsinNumber;
    }

    /**
     * Getter method for the issueIsinNumber property.
     */
    public String getIssueIsinNumber () {
        return issueIsinNumber;
    }

    private String issueNumber;

    /**
     * Setter method for the issueNumber property.
     */
    public void setIssueNumber (String issueNumber) {
        this.issueNumber = issueNumber;
    }

    /**
     * Getter method for the issueNumber property.
     */
    public String getIssueNumber () {
        return issueNumber;
    }

    private String issueTypeCode;

    /**
     * Setter method for the issueTypeCode property.
     */
    public void setIssueTypeCode (String issueTypeCode) {
        this.issueTypeCode = issueTypeCode;
    }

    /**
     * Getter method for the issueTypeCode property.
     */
    public String getIssueTypeCode () {
        return issueTypeCode;
    }

    private String issuerDescription;

    /**
     * Setter method for the issuerDescription property.
     */
    public void setIssuerDescription (String issuerDescription) {
        this.issuerDescription = issuerDescription;
    }

    /**
     * Getter method for the issuerDescription property.
     */
    public String getIssuerDescription () {
        return issuerDescription;
    }

    private String issuerNumber;

    /**
     * Setter method for the issuerNumber property.
     */
    public void setIssuerNumber (String issuerNumber) {
        this.issuerNumber = issuerNumber;
    }

    /**
     * Getter method for the issuerNumber property.
     */
    public String getIssuerNumber () {
        return issuerNumber;
    }

    private String lotNumber;

    /**
     * Setter method for the lotNumber property.
     */
    public void setLotNumber (String lotNumber) {
        this.lotNumber = lotNumber;
    }

    /**
     * Getter method for the lotNumber property.
     */
    public String getLotNumber () {
        return lotNumber;
    }

    private String parentCUSIP;

    /**
     * Setter method for the parentCUSIP property.
     */
    public void setParentCUSIP (String parentCUSIP) {
        this.parentCUSIP = parentCUSIP;
    }

    /**
     * Getter method for the parentCUSIP property.
     */
    public String getParentCUSIP () {
        return parentCUSIP;
    }

    private String parentPoolNumber;

    /**
     * Setter method for the parentPoolNumber property.
     */
    public void setParentPoolNumber (String parentPoolNumber) {
        this.parentPoolNumber = parentPoolNumber;
    }

    /**
     * Getter method for the parentPoolNumber property.
     */
    public String getParentPoolNumber () {
        return parentPoolNumber;
    }

    private String poolNumber;

    /**
     * Setter method for the poolNumber property.
     */
    public void setPoolNumber (String poolNumber) {
        this.poolNumber = poolNumber;
    }

    /**
     * Getter method for the poolNumber property.
     */
    public String getPoolNumber () {
        return poolNumber;
    }

    private String unitNumber;

    /**
     * Setter method for the unitNumber property.
     */
    public void setUnitNumber (String unitNumber) {
        this.unitNumber = unitNumber;
    }

    /**
     * Getter method for the unitNumber property.
     */
    public String getUnitNumber () {
        return unitNumber;
    }
}
