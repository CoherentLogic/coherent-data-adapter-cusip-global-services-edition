package com.coherentlogic.coherent.data.adapter.cgs.core.configuration;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CacheConfiguration {

	public static final String LOGIN_CACHE = "loginCache";

    public static final String CURRENCIES_CACHE = "currenciesCache";

    public static final String CUSIP_ATTRIBUTES_PORTFOLIO_DATA_CACHE = "cusipAttributesPortfolioDataCache";

    public static final String CUSIP_DATA_CACHE = "cusipDataCache";

    public static final String CUSIP_DATA_FOR_TICKER_CACHE= "cusipDataForTickerCache";

    public static final String CUSIP_PORTFOLIO_DATA_CACHE = "cusipPortfolioDataCache";

    public static final String STATES_CACHE = "statesCache";

    public static final String DOMICILES_CACHE = "domicilesCache";

    public static final String GOVERNMENT_MORTGAGES_CACHE = "governmentMortgagesCache";

    public static final String ISSUE_ATTRIBUTES_CACHE = "issueAttributesCache";

    public static final String LINKED_ISSUES_DATA_CACHE = "linkedIssuesDataCache";

    public static final String OPTIONS_DATA_CACHE = "optionsDataCache";

    public static final String SBL_DATA_CACHE = "sblDataCache";

    /**
     * @deprecated This is no longer supported at CGS.
     */
    public static final String INTERNATIONAL_IDENTIFIERS_CACHE = "internationalIdentifiersCache";

    /**
     * @deprecated Not currently supported at CGS.
     */
    public static final String AI_DATA_CACHE = "aiDataCache";
}
