package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Currencies extends ErrorBean {

    /**
     * @todo Extract into it's own class.
     * @todo Check the other beans for code, description, and isoCode -- IIRC these appear elsewhere so the class should
     * be removed and only one used.
     *
     * @author <a href="mailto:support@coherentlogic.com">Support</a>
     */
    public static class CurrencyDetail extends ErrorBean {

        private Integer code;

        private String description, isoCode;

        public CurrencyDetail () {
        }

        public CurrencyDetail(Integer code, String description, String isoCode) {
            super();
            this.code = code;
            this.description = description;
            this.isoCode = isoCode;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIsoCode() {
            return isoCode;
        }

        public void setIsoCode(String isoCode) {
            this.isoCode = isoCode;
        }

        @Override
        public String toString() {
            return "CurrencyDetail [code=" + code + ", description=" + description + ", isoCode=" + isoCode + "]";
        }
    }

    private final List<CurrencyDetail> currencyDetails;

    public Currencies() {
        this (new ArrayList<CurrencyDetail> ());
    }

    public Currencies(List<CurrencyDetail> currencyDetails) {
        this.currencyDetails = currencyDetails;
    }

    public List<CurrencyDetail> getCurrencyDetails() {
        return currencyDetails;
    }

    @Override
    public String toString() {
        return "Currencies [currencyDetails=" + currencyDetails + "]";
    }
}
