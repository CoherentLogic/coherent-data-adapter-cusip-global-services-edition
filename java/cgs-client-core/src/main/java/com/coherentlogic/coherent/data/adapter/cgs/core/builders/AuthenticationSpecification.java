package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

public interface AuthenticationSpecification<T> {

    T withUserId (String userId);

    T withPassword (String password);
}
