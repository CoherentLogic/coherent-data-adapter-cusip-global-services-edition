package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.coherent.data.model.core.exceptions.MethodNotSupportedException;

/**
 * [1] "An International Securities Identification Number (ISIN) uniquely identifies a security. Its structure is
 * defined in ISO 6166. Securities for which ISINs are issued include bonds, commercial paper, stocks and warrants."
 *
 * [2] "There are three parts to an ISIN as exemplified by US-049580485-1 (the dashes don't count, they are there to add
 * clarity). The code can be broken down as follows:
 *
 * * A two-letter country code, drawn from a list (ISO 6166) prepared by the International Organization for
 * Standardization (ISO). This code is assigned according to the location of a company's head office. A special code,
 * 'XS'. is used for international securities cleared through pan-European clearing systems like Euroclear and CEDEL.
 * Depository receipt ISIN usage is unique in that the country code for the security is that of the receipt issuer, not
 * that of the underlying security.
 *
 * * A nine-digit numeric identifier, called the National Securities Identifying Number (NSIN), and assigned by each
 * country's or region's . If a national number is composed of less than nine digits, it is padded with leading zeros to
 * become a NSIN. The numeric identifier has no intrinsic meaning it is essentially a serial number.
 *
 * * A single check-digit. The digit is calculated based upon the preceding 11 characters/digits and uses a sum modulo
 * 10 algorithm and helps ensure against counterfeit numbers."
 *
 * @see [1] <a href="https://en.wikipedia.org/wiki/International_Securities_Identification_Number">International
 * Securities Identification Number</a>
 *
 * @see [2] <a href="http://www.isin.org/isin/">International Securities Identification Number (ISIN)</a>
 */
public class ISIN extends IdentityBean implements ValidatableSpecification<String> {

    private String countryCode;

    private String identifier;

    private String checkDigit;

    @Override
    public String validate(String t) {
        throw new MethodNotSupportedException("Method implementation details required.");
    }
}
