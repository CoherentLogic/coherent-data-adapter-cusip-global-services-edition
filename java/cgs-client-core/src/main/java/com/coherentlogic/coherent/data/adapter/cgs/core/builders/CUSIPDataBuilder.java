package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.CusipRequest;
import com.coherentlogic.cusip.global.services.model.GetCusipData;
import com.coherentlogic.cusip.global.services.model.GetCusipDataResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CUSIPDataBuilder extends AbstractQueryBuilder<CUSIPData, GetCusipData, GetCusipDataResponse> {

    @Autowired
    public CUSIPDataBuilder(
        @Qualifier(CacheConfiguration.CUSIP_DATA_CACHE)
            CacheServiceProviderSpecification<String, CUSIPData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetCusipData, GetCusipDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetCusipDataResponse, CUSIPData> adapter,
        GetCusipData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    /**
     * 
     * @param cusip
     * @return
     */
    public CUSIPDataBuilder withCUSIP (String cusip) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setCusip(cusip);

        return this;
    }

    public CUSIPDataBuilder withIssueCurrencyCode (String issueCurrencyCode) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueCurrencyCode(issueCurrencyCode);

        return this;
    }

    static String asString (Date date) {
        throw new RuntimeException("Method not yet implemented!");
    }

    public CUSIPDataBuilder withIssueDatedDateFrom (String issueDatedDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueDatedDateFrom(issueDatedDateFrom);

        return this;
    }

    public CUSIPDataBuilder withIssueDatedDateFrom (Date issueDatedDateFrom) {
        return withIssueDatedDateFrom (asString (issueDatedDateFrom));
    }

    public CUSIPDataBuilder withIssueDatedDateTo (String issueDatedDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueDatedDateTo(issueDatedDateTo);

        return this;
    }

    public CUSIPDataBuilder withIssueDatedDateTo (Date issueDatedDateTo) {
        return withIssueDatedDateTo (asString (issueDatedDateTo));
    }

    public CUSIPDataBuilder withIssueDescription (String issueDescription) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueDescription(issueDescription);

        return this;
    }

    public CUSIPDataBuilder withIssueISINNumber (String issueISINNumber) {

        CusipRequest mCusipRequest = getRequest().getCusipRequest();

        mCusipRequest.setIssueIsinNumber(issueISINNumber);

        return this;
    }

    /**
     * 
     * @param issueLogDateFrom
     * @return
     *
     * @todo Overload
     */
    public CUSIPDataBuilder withIssueLogDateFrom (String issueLogDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueLogDateFrom(issueLogDateFrom);

        return this;
    }

    /**
     * 
     * @param issueLogDateTo
     * @return
     *
     * @todo Overload
     */
    public CUSIPDataBuilder withIssueLogDateTo (String issueLogDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueLogDateTo(issueLogDateTo);

        return this;
    }

    /**
     * 
     * @param issueMaturityDateFrom
     * @return
     *
     * @todo Overload
     */
    public CUSIPDataBuilder withIssueMaturityDateFrom (String issueMaturityDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueLogDateFrom(issueMaturityDateFrom);

        return this;
    }

    /**
     * 
     * @param issueMaturityDateTo
     * @return
     *
     * @todo Overload
     */
    public CUSIPDataBuilder withIssueMaturityDateTo (String issueMaturityDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueLogDateTo(issueMaturityDateTo);

        return this;
    }

    /**
     * 
     * @param issuePartialMaturityFrom
     * @return
     *
     * @todo Overload
     */
    public CUSIPDataBuilder withIssuePartialMaturityFrom (String issuePartialMaturityFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuePartialMaturityFrom(issuePartialMaturityFrom);

        return this;
    }

    /**
     * 
     * @param issuePartialMaturityTo
     * @return
     *
     * @todo Overload
     */
    public CUSIPDataBuilder withIssuePartialMaturityTo (String issuePartialMaturityTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuePartialMaturityTo(issuePartialMaturityTo);

        return this;
    }

    public CUSIPDataBuilder withIssueRate (String issueRate) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueRate(issueRate);

        return this;
    }

    /**
     * Gen begins here.
     *
     * @param issueRateFrom
     * @return
     */
    public CUSIPDataBuilder withIssueRateFrom (String issueRateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueRateFrom(issueRateFrom);

        return this;
    }

    public CUSIPDataBuilder withIssueRateTo (String issueRateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueRateTo(issueRateTo);

        return this;
    }

    public CUSIPDataBuilder withIssueReleaseDateFrom (String issueReleaseDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueReleaseDateFrom(issueReleaseDateFrom);

        return this;
    }

    public CUSIPDataBuilder withIssueReleaseDateTo (String issueReleaseDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueReleaseDateTo(issueReleaseDateTo);

        return this;
    }

    public CUSIPDataBuilder withIssueSort (String issueSort) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssueSort().add(issueSort);

        return this;
    }

    public CUSIPDataBuilder withIssueStatus (String issueStatus) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssueStatus().add(issueStatus);

        return this;
    }

    public CUSIPDataBuilder withIssueTypeCode (String issueTypeCode) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueTypeCode(issueTypeCode);

        return this;
    }

    public CUSIPDataBuilder withIssueUpdateType (String issueUpdateType) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssueUpdateType(issueUpdateType);

        return this;
    }

    public CUSIPDataBuilder withIssuerDateFrom (String issuerDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerDateFrom(issuerDateFrom);

        return this;
    }

    public CUSIPDataBuilder withIssuerDateTo (String issuerDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerDateTo(issuerDateTo);

        return this;
    }

    public CUSIPDataBuilder withIssuerDescription (String issuerDescription) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerDescription(issuerDescription);

        return this;
    }

    public CUSIPDataBuilder withIssuerDescriptionStartsWith (String issuerDescriptionStartsWith) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerDescriptionStartsWith(issuerDescriptionStartsWith);

        return this;
    }

    public CUSIPDataBuilder withIssuerDomicileCode (String issuerDomicileCode) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssuerDomicileCode().add(issuerDomicileCode);

        return this;
    }

    public CUSIPDataBuilder withIssuerGroup (String issuerGroup) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssuerGroup ().add(issuerGroup);

        return this;
    }

    public CUSIPDataBuilder withIssuerLogDateFrom (String issuerLogDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerLogDateFrom(issuerLogDateFrom);

        return this;
    }

    public CUSIPDataBuilder withIssuerLogDateTo (String issuerLogDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerLogDateTo(issuerLogDateTo);

        return this;
    }

    public CUSIPDataBuilder withIssuerReleaseDateFrom (String issuerReleaseDateFrom) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerReleaseDateFrom(issuerReleaseDateFrom);

        return this;
    }

    public CUSIPDataBuilder withIssuerReleaseDateTo (String issuerReleaseDateTo) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerReleaseDateTo(issuerReleaseDateTo);

        return this;
    }

    public CUSIPDataBuilder withIssuerSort (String issuerSort) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssuerSort().add(issuerSort);

        return this;
    }

    public CUSIPDataBuilder withIssuerStateCode (String issuerStateCode) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerStateCode(issuerStateCode);

        return this;
    }

    public CUSIPDataBuilder withIssuerStatus (String issuerStatus) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssuerStatus().add(issuerStatus);

        return this;
    }

    public CUSIPDataBuilder withIssuerTypeCode (String issuerTypeCode) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.getIssuerTypeCode().add(issuerTypeCode);

        return this;
    }

    public CUSIPDataBuilder withIssuerUpdateType (String issuerUpdateType) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setIssuerUpdateType(issuerUpdateType);

        return this;
    }

    public CUSIPDataBuilder withMaxResults (int maxResults) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setMaxResults(maxResults);

        return this;
    }

    public CUSIPDataBuilder withReturnIssuersOnlyFlag (String returnIssuersOnlyFlag) {

        CusipRequest cusipRequest = getRequest().getCusipRequest();

        cusipRequest.setReturnIssuersOnlyFlag(returnIssuersOnlyFlag);

        return this;
    }

    @Override
    public CUSIPData newInstance() {
        return new CUSIPData();
    }
}
