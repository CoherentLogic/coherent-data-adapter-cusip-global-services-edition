package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

/**
 * @deprecated This is no longer supported at CGS!
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InternationalIdentifiers extends ErrorBean {

    public InternationalIdentifiers() {
    }
}
