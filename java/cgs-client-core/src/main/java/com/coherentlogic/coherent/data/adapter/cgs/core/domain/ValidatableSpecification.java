package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

/**
 * A specification for data which must be validated (checked for the proper format and length, for instance).
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface ValidatableSpecification<T> {

	/**
	 * Method 
	 *
	 * @param t The data.
	 *
	 * @return A reference to t.
	 *
	 * @throws RuntimeException 
	 */
    T validate (T t);
}
