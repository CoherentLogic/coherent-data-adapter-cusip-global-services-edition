package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.builders.CacheableQueryBuilder;
import com.coherentlogic.coherent.data.model.core.builders.soap.RequestMethodSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <I> The type of domain class that is returned by the call to {@link #doGet()}.
 * @param <G> The model request type.
 * @param <R> The model response type.
 * @param <D> The domain class that is returned by the adapter (probably the same as I).
 */
public abstract class AbstractQueryBuilder<I extends IdentityBean, G, R> extends CacheableQueryBuilder<String, I>
    implements RequestMethodSpecification<I> {

    private static final Logger log = LoggerFactory.getLogger(AbstractQueryBuilder.class);

    protected static final String METHOD_NOT_YET_SUPPORTED = "Method not supported in this release.",
        METHOD_NOT_YET_IMPLEMENTED = "Method not yet implemented.";

    // Model references where G = Get, R = Response, D = domain object the adapter creates.
    private final GenericCUSIPGlobalServicesQueryBuilder<G, R> genericCGSQueryBuilder;

    private final InOutAdapterSpecification<R, I> adapter;

    private final G request;

    public AbstractQueryBuilder(
        CacheServiceProviderSpecification<String, I> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<G, R> genericCGSQueryBuilder,
        InOutAdapterSpecification<R, I> adapter,
        G request
    ) {
        super(cacheServiceProvider);
        this.genericCGSQueryBuilder = genericCGSQueryBuilder;
        this.adapter = adapter;
        this.request = request;
    }

    public GenericCUSIPGlobalServicesQueryBuilder<G, R> getCUSIPGlobalServicesQueryBuilder() {
        return genericCGSQueryBuilder;
    }

    public InOutAdapterSpecification<R, I> getAdapter() {
        return adapter;
    }

    public G getRequest() {
        return request;
    }

    /**
     * @return A new instance of the appropriate domain class.
     *
     * @deprecated Use a factory instead.
     */
    public abstract I newInstance ();

    @Override
    public I doGet() {

        log.info("doGet: method begins.");

        G request = getRequest();

        String key = String.valueOf(request.hashCode());

        I result = getCache().get(key);

        if (result == null) {

            log.info("The cache did not contain an entry for the key: " + key + " so one will be created and added.");

            R response = getCUSIPGlobalServicesQueryBuilder().doGet(request);

            result = newInstance();

            getAdapter().adapt(response, result);

            getCache().put(key, result);

        } else {
            log.info("The cache contained an entry for the key: " + key + " and value: " + result);
        }

        log.info("doGet: method ends; result: " + result);

        return result;
    }
}
