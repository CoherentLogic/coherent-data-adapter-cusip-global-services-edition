package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CUSIPDataForTicker extends ErrorBean {

    private List<CUSIPTickerIssuerDetails> cusipTickerIssuerDetails;

    private String exchangeCode, exchangeName, tickerSymbol;

    public CUSIPDataForTicker() {
        this(new ArrayList<CUSIPTickerIssuerDetails> (), null, null, null);
    }

    public CUSIPDataForTicker(
        List<CUSIPTickerIssuerDetails> cusipTickerIssuerDetails,
        String exchangeCode,
        String exchangeName,
        String tickerSymbol
    ) {
        super();
        this.cusipTickerIssuerDetails = cusipTickerIssuerDetails;
        this.exchangeCode = exchangeCode;
        this.exchangeName = exchangeName;
        this.tickerSymbol = tickerSymbol;
    }

    public List<CUSIPTickerIssuerDetails> getCUSIPTickerIssuerDetails() {
        return cusipTickerIssuerDetails;
    }

    public void setCUSIPTickerIssuerDetails(List<CUSIPTickerIssuerDetails> cusipTickerIssuerDetails) {
        this.cusipTickerIssuerDetails = cusipTickerIssuerDetails;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getTickerSymbol() {
        return tickerSymbol;
    }

    public void setTickerSymbol(String tickerSymbol) {
        this.tickerSymbol = tickerSymbol;
    }

    @Override
    public String toString() {
        return "CUSIPDataForTicker [cusipTickerIssuerDetails=" + cusipTickerIssuerDetails + ", exchangeCode="
                + exchangeCode + ", exchangeName=" + exchangeName + ", tickerSymbol=" + tickerSymbol + "]";
    }
}
