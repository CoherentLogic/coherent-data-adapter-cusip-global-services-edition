package com.coherentlogic.coherent.data.adapter.cgs.core.factories;

import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesIdHubQueryBuilder;
import com.coherentlogic.coherent.data.model.core.factories.Factory;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Deprecated//("This should now be managed by the Spring container.")
public class CUSIPGlobalServicesQueryBuilderFactory implements Factory {

//    private final String binarySecurityToken;
//
//    public CUSIPGlobalServicesQueryBuilderFactory (String binarySecurityToken) {
//        this.binarySecurityToken = binarySecurityToken;
//    }
//
    @Override
    public <T> T getInstance() {
        throw new RuntimeException("MNYI!");
    }
//
//    @Override
//    public <CUSIPGlobalServicesIdHubQueryBuilder> CUSIPGlobalServicesIdHubQueryBuilder getInstance() {
//
//        CUSIPGlobalServicesIdHubQueryBuilder result = new CUSIPGlobalServicesIdHubQueryBuilder ();
//
//        result.setBinarySecurityToken(binarySecurityToken);
//
//        return result;
//    }
}
