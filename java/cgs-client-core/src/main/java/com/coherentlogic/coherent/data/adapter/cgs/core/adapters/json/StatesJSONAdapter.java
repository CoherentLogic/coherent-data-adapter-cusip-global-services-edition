package com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.States;
import com.coherentlogic.coherent.data.model.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.rproject.integration.dataframe.builders.JDataFrameBuilder;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class StatesJSONAdapter implements InReturnAdapterSpecification<States, String> {

    @Override
    public String adapt(States states) {

        final List<String> codeList = new ArrayList<String> ();
        final List<String> descriptionList = new ArrayList<String> ();

        states
            .getStateList()
            .getStateDetails()
            .forEach(
                (StateDetail stateDetail) -> {
                    codeList.add(stateDetail.getCode ());
                    descriptionList.add(stateDetail.getDescription());
                }
            );

        Object[] codes = codeList.toArray();
        Object[] descriptions = descriptionList.toArray();

        return (String) new JDataFrameBuilder (JDataFrameBuilder.DEFAULT_REMOTE_ADAPTER)
            .addColumn("Type", new Object[] {"stateDetail"})
            .addColumn("Codes", codes)
            .addColumn("Descriptions", descriptions)
            .serialize();
    }
}
