package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.States;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetStates;
import com.coherentlogic.cusip.global.services.model.GetStatesResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class StatesBuilder extends AbstractQueryBuilder<States, GetStates, GetStatesResponse> {

    @Autowired
    public StatesBuilder(
        @Qualifier(CacheConfiguration.STATES_CACHE)
            CacheServiceProviderSpecification<String, States> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetStates, GetStatesResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetStatesResponse, States> adapter,
        GetStates request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public States newInstance() {
        return new States ();
    }
}
