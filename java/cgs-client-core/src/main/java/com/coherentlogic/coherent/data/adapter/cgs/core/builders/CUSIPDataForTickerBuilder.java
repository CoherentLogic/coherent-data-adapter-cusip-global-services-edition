package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPDataForTicker;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.CusipTickerRequest;
import com.coherentlogic.cusip.global.services.model.GetCusipDataForTicker;
import com.coherentlogic.cusip.global.services.model.GetCusipDataForTickerResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CUSIPDataForTickerBuilder
    extends AbstractQueryBuilder<CUSIPDataForTicker, GetCusipDataForTicker, GetCusipDataForTickerResponse> {

    @Autowired
    public CUSIPDataForTickerBuilder(
        @Qualifier(CacheConfiguration.CUSIP_DATA_FOR_TICKER_CACHE)
            CacheServiceProviderSpecification<String, CUSIPDataForTicker> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetCusipDataForTicker, GetCusipDataForTickerResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetCusipDataForTickerResponse, CUSIPDataForTicker> adapter,
        GetCusipDataForTicker request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public CUSIPDataForTickerBuilder withExchangeCode (String exchangeCode) {

        CusipTickerRequest cusipTickerRequest = getRequest().getRequest();

        cusipTickerRequest.setExchangeCode(exchangeCode);

        return this;
    }

    public CUSIPDataForTickerBuilder withTickerSymbol (String tickerSymbol) {

        CusipTickerRequest cusipTickerRequest = getRequest().getRequest();

        cusipTickerRequest.setTickerSymbol(tickerSymbol);

        return this;
    }

    @Override
    public CUSIPDataForTicker newInstance() {
        return new CUSIPDataForTicker ();
    }
}
