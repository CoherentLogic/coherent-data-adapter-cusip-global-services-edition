package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class IssuerDetail extends IdentityBean {

    private List<IssueDetail> issueDetailList;

    public IssuerDetail () {
        this (new ArrayList<IssueDetail> ());
    }

    public IssuerDetail (List<IssueDetail> issueDetailList) {
        this.issueDetailList = issueDetailList;
    }

    public List<IssueDetail> getIssueDetailList() {
        return issueDetailList;
    }

    public void setIssueDetailList(List<IssueDetail> issueDetailList) {
        this.issueDetailList = issueDetailList;
    }

    private String issuerAdditionalInfo;

    public void setIssuerAdditionalInfo (String issuerAdditionalInfo) {
        this.issuerAdditionalInfo = issuerAdditionalInfo;
    }

    public String getIssuerAdditionalInfo () {
        return issuerAdditionalInfo;
    }

    private String issuerBankBranchIndicator;

    public void setIssuerBankBranchIndicator (String issuerBankBranchIndicator) {
        this.issuerBankBranchIndicator = issuerBankBranchIndicator;
    }

    public String getIssuerBankBranchIndicator () {
        return issuerBankBranchIndicator;
    }

    private String issuerCPInstitutionType;

    public void setIssuerCPInstitutionType (String issuerCPInstitutionType) {
        this.issuerCPInstitutionType = issuerCPInstitutionType;
    }

    public String getIssuerCPInstitutionType () {
        return issuerCPInstitutionType;
    }

    private String issuerDate;

    public void setIssuerDate (String issuerDate) {
        this.issuerDate = issuerDate;
    }

    public String getIssuerDate () {
        return issuerDate;
    }

    private String issuerDescription;

    public void setIssuerDescription (String issuerDescription) {
        this.issuerDescription = issuerDescription;
    }

    public String getIssuerDescription () {
        return issuerDescription;
    }

    private String issuerDomicileCode;

    public void setIssuerDomicileCode (String issuerDomicileCode) {
        this.issuerDomicileCode = issuerDomicileCode;
    }

    public String getIssuerDomicileCode () {
        return issuerDomicileCode;
    }

    private String issuerGroup;

    public void setIssuerGroup (String issuerGroup) {
        this.issuerGroup = issuerGroup;
    }

    public String getIssuerGroup () {
        return issuerGroup;
    }

    private String issuerLogDate;

    public void setIssuerLogDate (String issuerLogDate) {
        this.issuerLogDate = issuerLogDate;
    }

    public String getIssuerLogDate () {
        return issuerLogDate;
    }

    private String issuerNumber;

    public void setIssuerNumber (String issuerNumber) {
        this.issuerNumber = issuerNumber;
    }

    public String getIssuerNumber () {
        return issuerNumber;
    }

    private String issuerReleaseDate;

    public void setIssuerReleaseDate (String issuerReleaseDate) {
        this.issuerReleaseDate = issuerReleaseDate;
    }

    public String getIssuerReleaseDate () {
        return issuerReleaseDate;
    }

    private String issuerStateCode;

    public void setIssuerStateCode (String issuerStateCode) {
        this.issuerStateCode = issuerStateCode;
    }

    public String getIssuerStateCode () {
        return issuerStateCode;
    }

    private String issuerStatus;

    public void setIssuerStatus (String issuerStatus) {
        this.issuerStatus = issuerStatus;
    }

    public String getIssuerStatus () {
        return issuerStatus;
    }

    private String issuerTransactionCode;

    public void setIssuerTransactionCode (String issuerTransactionCode) {
        this.issuerTransactionCode = issuerTransactionCode;
    }

    public String getIssuerTransactionCode () {
        return issuerTransactionCode;
    }

    private String issuerTypeCode;

    public void setIssuerTypeCode (String issuerTypeCode) {
        this.issuerTypeCode = issuerTypeCode;
    }

    public String getIssuerTypeCode () {
        return issuerTypeCode;
    }

    @Override
    public String toString() {
        return "IssuerDetail [issueDetailList=" + issueDetailList + ", issuerAdditionalInfo=" + issuerAdditionalInfo
            + ", issuerBankBranchIndicator=" + issuerBankBranchIndicator + ", issuerCPInstitutionType="
            + issuerCPInstitutionType + ", issuerDate=" + issuerDate + ", issuerDescription="
            + issuerDescription + ", issuerDomicileCode=" + issuerDomicileCode + ", issuerGroup=" + issuerGroup
            + ", issuerLogDate=" + issuerLogDate + ", issuerNumber=" + issuerNumber + ", issuerReleaseDate="
            + issuerReleaseDate + ", issuerStateCode=" + issuerStateCode + ", issuerStatus=" + issuerStatus
            + ", issuerTransactionCode=" + issuerTransactionCode + ", issuerTypeCode=" + issuerTypeCode + "]";
    }
}