package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class StateList extends IdentityBean {

    private static final long serialVersionUID = 2945720436436686357L;

    private Long stateCount;

    private List<StateDetail> stateDetails;

    public StateList () {
        this (new ArrayList<StateDetail> ());
    }

    public StateList (List<StateDetail> stateDetails) {
        this.stateDetails = stateDetails;
    }

    public Long getStateCount() {
        return stateCount;
    }

    public void setStateCount(Long stateCount) {
        this.stateCount = stateCount;
    }

    public List<StateDetail> getStateDetails() {
        return stateDetails;
    }

    public void setStateDetails(List<StateDetail> stateDetails) {
        this.stateDetails = stateDetails;
    }

    @Override
    public String toString() {
        return "StateList [stateCount=" + stateCount + ", stateDetails=" + stateDetails + "]";
    }
}