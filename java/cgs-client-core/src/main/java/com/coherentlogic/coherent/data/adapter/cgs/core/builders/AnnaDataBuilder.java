package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AnnaData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.AnnaRequest;
import com.coherentlogic.cusip.global.services.model.GetAnnaData;
import com.coherentlogic.cusip.global.services.model.GetAnnaDataResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AnnaDataBuilder extends AbstractQueryBuilder<AnnaData, GetAnnaData, GetAnnaDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(AnnaDataBuilder.class);

    public AnnaDataBuilder(
        CacheServiceProviderSpecification<String, AnnaData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetAnnaData, GetAnnaDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetAnnaDataResponse, AnnaData> adapter,
        GetAnnaData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public AnnaDataBuilder withIssueCountryCode (String issueCountryCode) {

        log.info ("method begins; issueCountryCode: " + issueCountryCode);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssueCountryCode(issueCountryCode);

        return this;
    }

    public AnnaDataBuilder withIssueDescription (String issueDescription) {

        log.info ("method begins; issueDescription: " + issueDescription);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssueDescription(issueDescription);

        return this;
    }

    public AnnaDataBuilder withIssueISINNumber (String issueISINNumber) {

        log.info ("method begins; issueISINNumber: " + issueISINNumber);

        GetAnnaData request = getRequest();

        AnnaRequest mAnnaRequest = request.getRequest();

        mAnnaRequest.setIssueIsinNumber(issueISINNumber);

        return this;
    }

    public AnnaDataBuilder withIssueIsoCurrencyCode (String issueIsoCurrencyCode) {

        log.info ("method begins; issueIsoCurrencyCode: " + issueIsoCurrencyCode);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssueIsoCurrencyCode(issueIsoCurrencyCode);

        return this;
    }

    public AnnaDataBuilder withIssueMaturityDateFrom (String issueMaturityDateFrom) {

        log.info ("method begins; issueMaturityDateFrom: " + issueMaturityDateFrom);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssueMaturityDateFrom(issueMaturityDateFrom);

        return this;
    }

    public AnnaDataBuilder withIssueMaturityDateTo (String issueMaturityDateTo) {

        log.info ("method begins; issueMaturityDateTo: " + issueMaturityDateTo);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssueMaturityDateTo(issueMaturityDateTo);

        return this;
    }

    public AnnaDataBuilder withIssueRate (String issueRate) {

        log.info ("method begins; issueRate: " + issueRate);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssueRate(issueRate);

        return this;
    }

    public AnnaDataBuilder withIssueSort (String issueSort) {

        log.info ("method begins; issueSort: " + issueSort);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.getIssueSort().add(issueSort);

        return this;
    }

    public AnnaDataBuilder withIssuerDescription (String issuerDescription) {

        log.info ("method begins; issuerDescription: " + issuerDescription);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setIssuerDescription(issuerDescription);

        return this;
    }

    public AnnaDataBuilder withMaxResults (int maxResults) {

        log.info ("method begins; maxResults: " + maxResults);

        GetAnnaData request = getRequest();

        AnnaRequest annaRequest = request.getRequest();

        annaRequest.setMaxResults(maxResults);

        return this;
    }

    @Override
    public AnnaData newInstance() {
        return new AnnaData();
    }

    @Override
    public AnnaData doGet() {
        throw new RuntimeException (METHOD_NOT_YET_SUPPORTED);
    }
}
