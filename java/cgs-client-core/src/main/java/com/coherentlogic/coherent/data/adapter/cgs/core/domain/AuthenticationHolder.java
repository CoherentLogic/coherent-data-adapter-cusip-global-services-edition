package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * The purpose of this class is to hold the userId and binarySecurityToken, which are used for all operations after
 * authenticating with CGS. This class is a Spring component and when the application is started the userId and
 * binarySecurityToken will be null and they will need to be set outside of the Spring container.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class AuthenticationHolder extends SerializableBean {

    private static final long serialVersionUID = 5225800426726916913L;

    private String userId, binarySecurityToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {

        String oldValue = this.userId;

        if (oldValue == null || !(oldValue.equals(userId))) {
            this.userId = userId;

            firePropertyChange("userId", oldValue, userId);
        }
    }

    public String getBinarySecurityToken() {
        return binarySecurityToken;
    }

    public void setBinarySecurityToken(String binarySecurityToken) {

        String oldValue = this.binarySecurityToken;

        if (oldValue == null || !(oldValue.equals(binarySecurityToken))) {
            this.binarySecurityToken = binarySecurityToken;

            firePropertyChange("binarySecurityToken", oldValue, binarySecurityToken);
        }
    }
}
