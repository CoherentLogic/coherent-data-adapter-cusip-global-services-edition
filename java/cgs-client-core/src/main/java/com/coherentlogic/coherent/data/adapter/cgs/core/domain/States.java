package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
public class States extends ErrorBean {

    private static final long serialVersionUID = 3051693128723407704L;

    private StateList stateList;

    public States() {
        this (new StateList ());
    }

    public States(StateList stateList) {
        this.stateList = stateList;
    }

    /**
     * @deprecated Refactor/rename this method to getStates. We do not need the StateList as it keeps the size/lenth
     *  and this can be inferred from the list size so it's not serving a purpose.
     */
    @Deprecated
    public StateList getStateList() {
        return stateList;
    }

    /**
     * @deprecated Refactor/rename this method to getStates. We do not need the StateList as it keeps the size/lenth
     *  and this can be inferred from the list size so it's not serving a purpose.
     */
    @Deprecated
    public void setStateList(StateList stateList) {
        this.stateList = stateList;
    }

    @Override
    public String toString() {
        return "States [stateList=" + stateList + "]";
    }
}
