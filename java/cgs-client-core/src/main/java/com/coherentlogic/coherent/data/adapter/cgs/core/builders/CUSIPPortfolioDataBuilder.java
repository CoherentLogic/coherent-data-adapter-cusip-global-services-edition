package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPPortfolioData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.CusipPortfolioRequest;
import com.coherentlogic.cusip.global.services.model.GetCusipPortfolioData;
import com.coherentlogic.cusip.global.services.model.GetCusipPortfolioDataResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CUSIPPortfolioDataBuilder
    extends AbstractQueryBuilder<CUSIPPortfolioData, GetCusipPortfolioData, GetCusipPortfolioDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPPortfolioDataBuilder.class);

    @Autowired
    public CUSIPPortfolioDataBuilder(
        @Qualifier(CacheConfiguration.CUSIP_PORTFOLIO_DATA_CACHE)
            CacheServiceProviderSpecification<String, CUSIPPortfolioData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<
            GetCusipPortfolioData,
            GetCusipPortfolioDataResponse
        > genericCGSQueryBuilder,
        InOutAdapterSpecification<GetCusipPortfolioDataResponse, CUSIPPortfolioData> adapter,
        GetCusipPortfolioData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public CUSIPPortfolioData newInstance() {
        return new CUSIPPortfolioData ();
    }

    public CUSIPPortfolioDataBuilder withCUSIP (String cusip) {

        log.info ("method begins; cusip: " + cusip);

        GetCusipPortfolioData getCusipPortfolioData = getRequest();

        CusipPortfolioRequest request = getCusipPortfolioData.getRequest();

        request.getCusip().add(cusip);

        return this;
    }

    public CUSIPPortfolioDataBuilder withCUSIP6 (String cusip6) {

        log.info ("method begins; cusip6: " + cusip6);

        GetCusipPortfolioData getCusipPortfolioData = getRequest();

        CusipPortfolioRequest request = getCusipPortfolioData.getRequest();

        request.getCusip6().add(cusip6);

        return this;
    }

    public CUSIPPortfolioDataBuilder withISIN (String isin) {

        log.info ("method begins; isin: " + isin);

        GetCusipPortfolioData getCusipPortfolioData = getRequest();

        CusipPortfolioRequest request = getCusipPortfolioData.getRequest();

        request.getIsin().add(isin);

        return this;
    }
}
