package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class OptionDetail extends IdentityBean {

    private static final long serialVersionUID = -8435756936532591272L;

    private String actionIndDesc;

    public void setActionIndDesc (String actionIndDesc) {
        this.actionIndDesc = actionIndDesc;
    }

    public String getActionIndDesc () {
        return actionIndDesc;
    }

    private String actionIndicator;

    public void setActionIndicator (String actionIndicator) {
        this.actionIndicator = actionIndicator;
    }

    public String getActionIndicator () {
        return actionIndicator;
    }

    private String addedD;

    public void setAddedD (String addedD) {
        this.addedD = addedD;
    }

    public String getAddedD () {
        return addedD;
    }

    private String assetClass;

    public void setAssetClass (String assetClass) {
        this.assetClass = assetClass;
    }

    public String getAssetClass () {
        return assetClass;
    }

    private String baseCurrency;

    public void setBaseCurrency (String baseCurrency) {
        this.baseCurrency = baseCurrency;
    }

    public String getBaseCurrency () {
        return baseCurrency;
    }

    private String cC9;

    public void setCC9 (String cC9) {
        this.cC9 = cC9;
    }

    public String getCC9 () {
        return cC9;
    }

    private String contractDelistDate;

    public void setContractDelistDate (String contractDelistDate) {
        this.contractDelistDate = contractDelistDate;
    }

    public String getContractDelistDate () {
        return contractDelistDate;
    }

    private String contractLevelFoIndicator;

    public void setContractLevelFoIndicator (String contractLevelFoIndicator) {
        this.contractLevelFoIndicator = contractLevelFoIndicator;
    }

    public String getContractLevelFoIndicator () {
        return contractLevelFoIndicator;
    }

    private String contractName;

    public void setContractName (String contractName) {
        this.contractName = contractName;
    }

    public String getContractName () {
        return contractName;
    }

    private String contractNo;

    public void setContractNo (String contractNo) {
        this.contractNo = contractNo;
    }

    public String getContractNo () {
        return contractNo;
    }

    private String contractSize;

    public void setContractSize (String contractSize) {
        this.contractSize = contractSize;
    }

    public String getContractSize () {
        return contractSize;
    }

    private String contractStartDate;

    public void setContractStartDate (String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    public String getContractStartDate () {
        return contractStartDate;
    }

    private String csbIsin;

    public void setCsbIsin (String csbIsin) {
        this.csbIsin = csbIsin;
    }

    public String getCsbIsin () {
        return csbIsin;
    }

    private String dateType;

    public void setDateType (String dateType) {
        this.dateType = dateType;
    }

    public String getDateType () {
        return dateType;
    }

    private String exchangeCode;

    public void setExchangeCode (String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public String getExchangeCode () {
        return exchangeCode;
    }

    private String exchangeSymbol;

    public void setExchangeSymbol (String exchangeSymbol) {
        this.exchangeSymbol = exchangeSymbol;
    }

    public String getExchangeSymbol () {
        return exchangeSymbol;
    }

    private String expirationDate;

    public void setExpirationDate (String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getExpirationDate () {
        return expirationDate;
    }

    private String foIndicator;

    public void setFoIndicator (String foIndicator) {
        this.foIndicator = foIndicator;
    }

    public String getFoIndicator () {
        return foIndicator;
    }

    private String instrumentType;

    public void setInstrumentType (String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getInstrumentType () {
        return instrumentType;
    }

    private String isoCfiCode;

    public void setIsoCfiCode (String isoCfiCode) {
        this.isoCfiCode = isoCfiCode;
    }

    public String getIsoCfiCode () {
        return isoCfiCode;
    }

    private String isoMicCode;

    public void setIsoMicCode (String isoMicCode) {
        this.isoMicCode = isoMicCode;
    }

    public String getIsoMicCode () {
        return isoMicCode;
    }

    private String occCode;

    public void setOccCode (String occCode) {
        this.occCode = occCode;
    }

    public String getOccCode () {
        return occCode;
    }

    private String opraOsiCode;

    public void setOpraOsiCode (String opraOsiCode) {
        this.opraOsiCode = opraOsiCode;
    }

    public String getOpraOsiCode () {
        return opraOsiCode;
    }

    private String opraSymbol;

    public void setOpraSymbol (String opraSymbol) {
        this.opraSymbol = opraSymbol;
    }

    public String getOpraSymbol () {
        return opraSymbol;
    }

    private String optionPremiumPayment;

    public void setOptionPremiumPayment (String optionPremiumPayment) {
        this.optionPremiumPayment = optionPremiumPayment;
    }

    public String getOptionPremiumPayment () {
        return optionPremiumPayment;
    }

    private String optionSettlementType;

    public void setOptionSettlementType (String optionSettlementType) {
        this.optionSettlementType = optionSettlementType;
    }

    public String getOptionSettlementType () {
        return optionSettlementType;
    }

    private String productType;

    public void setProductType (String productType) {
        this.productType = productType;
    }

    public String getProductType () {
        return productType;
    }

    private String promptDate;

    public void setPromptDate (String promptDate) {
        this.promptDate = promptDate;
    }

    public String getPromptDate () {
        return promptDate;
    }

    private String promptMonthCode;

    public void setPromptMonthCode (String promptMonthCode) {
        this.promptMonthCode = promptMonthCode;
    }

    public String getPromptMonthCode () {
        return promptMonthCode;
    }

    private String quoteFraction;

    public void setQuoteFraction (String quoteFraction) {
        this.quoteFraction = quoteFraction;
    }

    public String getQuoteFraction () {
        return quoteFraction;
    }

    private String settlementCurrency;

    public void setSettlementCurrency (String settlementCurrency) {
        this.settlementCurrency = settlementCurrency;
    }

    public String getSettlementCurrency () {
        return settlementCurrency;
    }

    private String settlementType;

    public void setSettlementType (String settlementType) {
        this.settlementType = settlementType;
    }

    public String getSettlementType () {
        return settlementType;
    }

    private String status;

    public void setStatus (String status) {
        this.status = status;
    }

    public String getStatus () {
        return status;
    }

    private String strikePrice;

    public void setStrikePrice (String strikePrice) {
        this.strikePrice = strikePrice;
    }

    public String getStrikePrice () {
        return strikePrice;
    }

    private String tickSize;

    public void setTickSize (String tickSize) {
        this.tickSize = tickSize;
    }

    public String getTickSize () {
        return tickSize;
    }

    private String tickSizeDenominator;

    public void setTickSizeDenominator (String tickSizeDenominator) {
        this.tickSizeDenominator = tickSizeDenominator;
    }

    public String getTickSizeDenominator () {
        return tickSizeDenominator;
    }

    private String tickSizeNumerator;

    public void setTickSizeNumerator (String tickSizeNumerator) {
        this.tickSizeNumerator = tickSizeNumerator;
    }

    public String getTickSizeNumerator () {
        return tickSizeNumerator;
    }

    private String tickValue;

    public void setTickValue (String tickValue) {
        this.tickValue = tickValue;
    }

    public String getTickValue () {
        return tickValue;
    }

    private String underlyingCusip;

    public void setUnderlyingCusip (String underlyingCusip) {
        this.underlyingCusip = underlyingCusip;
    }

    public String getUnderlyingCusip () {
        return underlyingCusip;
    }

    private String underlyingIsin;

    public void setUnderlyingIsin (String underlyingIsin) {
        this.underlyingIsin = underlyingIsin;
    }

    public String getUnderlyingIsin () {
        return underlyingIsin;
    }

    private String underlyingSymbol;

    public void setUnderlyingSymbol (String underlyingSymbol) {
        this.underlyingSymbol = underlyingSymbol;
    }

    public String getUnderlyingSymbol () {
        return underlyingSymbol;
    }

    private String updatedD;

    public void setUpdatedD (String updatedD) {
        this.updatedD = updatedD;
    }

    public String getUpdatedD () {
        return updatedD;
    }

    private String usCfiCode;

    public void setUsCfiCode (String usCfiCode) {
        this.usCfiCode = usCfiCode;
    }

    public String getUsCfiCode () {
        return usCfiCode;
    }
}