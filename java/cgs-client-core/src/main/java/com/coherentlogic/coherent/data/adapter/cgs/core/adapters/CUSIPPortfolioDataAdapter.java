package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPPortfolioData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.cusip.global.services.model.GetCusipPortfolioDataResponse;
import com.coherentlogic.cusip.global.services.model.IssuerList;

@Component
public class CUSIPPortfolioDataAdapter implements InOutAdapterSpecification<GetCusipPortfolioDataResponse, CUSIPPortfolioData> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPPortfolioDataAdapter.class);

    @Autowired
    private CUSIPDataAdapter cusipDataAdapter;

    @Override
    public void adapt(GetCusipPortfolioDataResponse source, CUSIPPortfolioData target) {

        IssuerList issuerList = source.getReturn().getIssuerList();

        cusipDataAdapter.adapt(issuerList, target);
    }
}
