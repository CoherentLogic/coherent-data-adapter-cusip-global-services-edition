package com.coherentlogic.coherent.data.adapter.cgs.examples;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json.DomicilesJSONAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json.ErrorListJSONAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json.StatesJSONAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesIdHubQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesAuthenticationQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AuthenticationHolder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPAttributesPortfolioData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPDataForTicker;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Currencies;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domiciles;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttributes;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssuerDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.States;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * -Djava.net.preferIPv4Stack=true
 */
//@SpringBootApplication(scanBasePackages="com.coherentlogic.coherent.data.adapter.cgs")
//@ImportResource({"classpath*:spring/jpa-beans.xml"})
public class BuilderExampleApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(BuilderExampleApplication.class);

    @Autowired
    private ErrorListJSONAdapter errorListJSONAdapter;

    @Autowired
    private CUSIPGlobalServicesAuthenticationQueryBuilder cgsAuthenticationQueryBuilder;

    @Autowired
    private CUSIPGlobalServicesIdHubQueryBuilder cgsIdHubQueryBuilder;

    @Autowired
    private AuthenticationHolder authenticationHolder;

    @Autowired
    private StatesJSONAdapter statesJSONAdapter;

    @Autowired
    private DomicilesJSONAdapter domicilesJSONAdapter;

    @Autowired
    private String userId;

    @Autowired
    private String password;

    static final String ERROR_TEXT = "This is designed to break.";

    @Override
    public void run(String... args) { 

        String binarySecurityToken =
            cgsAuthenticationQueryBuilder
                .login()
                .withUserId(userId)
                .withPassword(password)
                .doGet()
                .getBinarySecurityToken();

        log.info("binarySecurityToken: " + binarySecurityToken);

        authenticationHolder.setUserId(userId);
        authenticationHolder.setBinarySecurityToken(binarySecurityToken);

        CUSIPAttributesPortfolioData cusipAttributesPortfolioData =
            cgsIdHubQueryBuilder
                .cusipAttributesPortfolioData()
                .withCUSIP("023135106")
                .doGet();

        log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ cusipAttributesPortfolioData: " + cusipAttributesPortfolioData);

        IssueAttributes issueAttributes =
            cgsIdHubQueryBuilder
                .issueAttributes()
                .withCUSIP("023135106")
                .doGet();

        log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ issueAttributes: " + issueAttributes);

        CUSIPDataForTicker cusipDataForTicker =
            cgsIdHubQueryBuilder
                .cusipDataForTicker()
                .withTickerSymbol("MSFT")
                .doGet();

        log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ cusipDataForTicker: " + cusipDataForTicker);

        Currencies currencies = cgsIdHubQueryBuilder.currencies().doGet();

        log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ currencies: " + currencies);

        States states = cgsIdHubQueryBuilder
            .states()
            .doGet();

        String statesJson = statesJSONAdapter.adapt(states);

        log.info("statesJson: " + statesJson);

        states
            .getStateList()
            .getStateDetails()
            .forEach(
                state -> {
                    log.info("State: " + state);
                }
            );

        Domiciles domiciles = cgsIdHubQueryBuilder
            .domiciles()
            .doGet();

        String domicilesJson = domicilesJSONAdapter.adapt(domiciles);

        log.info("/////////////////////////////////// domicilesJson: " + domicilesJson);

        CUSIPData cusipData = cgsIdHubQueryBuilder
            .cusipData()
            .withIssueISINNumber("US0378331005" + ERROR_TEXT)
            .doGet();

        String json = errorListJSONAdapter.adapt(cusipData.getErrors());

        log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! errors: " + json);

        List<IssuerDetail> issuerDetails = cusipData.getIssuerDetails();

        IssuerDetail issuerDetail = null;

        if (issuerDetails != null && 0 < issuerDetails.size())
            issuerDetail = issuerDetails.get(0);

        log.info("cusipData.issuerDetail: " + issuerDetail);
        log.info("cusipData: " + cusipData);

        cusipData
            .getErrors()
            .forEach(
                error -> {
                    log.info ("----- error: " + error);
                }
            );
    }

    public static void main(String[] args) {
        SpringApplication.run(BuilderExampleApplication.class, args);
    }
}
