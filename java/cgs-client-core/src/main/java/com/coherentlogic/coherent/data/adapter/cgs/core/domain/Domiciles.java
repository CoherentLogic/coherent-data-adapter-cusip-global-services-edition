package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Domiciles extends IdentityBean {

    private List<Domicile> domiciles;

    public Domiciles () {
        this (new ArrayList<Domicile> ());
    }

    public Domiciles (List<Domicile> domiciles) {
        this.domiciles = domiciles;
    }

    public List<Domicile> getDomiciles() {
        return domiciles;
    }

    public void setDomiciles(List<Domicile> domiciles) {
        this.domiciles = domiciles;
    }

    @Override
    public String toString() {
        return "Domiciles [domiciles=" + domiciles + "]";
    }
}
