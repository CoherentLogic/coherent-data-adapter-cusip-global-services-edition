package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CUSIPAttributesPortfolioData extends ErrorBean {

    private final List<IssueAttribute> issueAttributes;

    public CUSIPAttributesPortfolioData() {
        this (new ArrayList<IssueAttribute> ());
    }

    public CUSIPAttributesPortfolioData(List<IssueAttribute> issueAttributes) {
        this.issueAttributes = issueAttributes;
    }

    public List<IssueAttribute> getIssueAttributes() {
        return issueAttributes;
    }

    @Override
    public String toString() {
        return "CUSIPAttributesPortfolioData [issueAttributes=" + issueAttributes + "]";
    }
}
