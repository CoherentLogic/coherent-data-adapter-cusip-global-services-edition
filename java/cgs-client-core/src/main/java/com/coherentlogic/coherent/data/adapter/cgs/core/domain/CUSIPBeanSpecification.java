package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

/**
 * "A CUSIP is a nine-character alphanumeric code that identifies a North American financial security for the
 * purposes of facilitating clearing and settlement of trades. The CUSIP was adopted as an American National
 * Standard under Accredited Standards X9.6. The CUSIP system is owned by the American Bankers Association,
 * and is operated by S&P Capital IQ. The operating body, CUSIP Global Services (CGS) also serves as the
 * national numbering agency (NNA) for North America, and the CUSIP serves as the National Securities
 * Identification Number (NSIN) for products issued from both the United States and Canada. In its role as the
 * NNA, CUSIP Global Services (CGS) also assigns all US-based ISINs.
 *
 * @see <a href="https://en.wikipedia.org/wiki/CUSIP">CUSIP</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface CUSIPBeanSpecification {

    String getCUSIP ();

    void setCUSIP (String cusip);
}
