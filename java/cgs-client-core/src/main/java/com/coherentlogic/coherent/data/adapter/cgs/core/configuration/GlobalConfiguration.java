package com.coherentlogic.coherent.data.adapter.cgs.core.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.coherentlogic.coherent.data.adapter.cgs.core.builders.GenericCUSIPGlobalServicesQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.factories.GenericCUSIPGlobalServicesQueryBuilderFactory;
import com.coherentlogic.cusip.global.services.model.GetCurrencies;
import com.coherentlogic.cusip.global.services.model.GetCurrenciesResponse;
import com.coherentlogic.cusip.global.services.model.GetCusipAttributesPortfolioData;
import com.coherentlogic.cusip.global.services.model.GetCusipAttributesPortfolioDataResponse;
import com.coherentlogic.cusip.global.services.model.GetCusipData;
import com.coherentlogic.cusip.global.services.model.GetCusipDataForTicker;
import com.coherentlogic.cusip.global.services.model.GetCusipDataForTickerResponse;
import com.coherentlogic.cusip.global.services.model.GetCusipDataResponse;
import com.coherentlogic.cusip.global.services.model.GetCusipPortfolioData;
import com.coherentlogic.cusip.global.services.model.GetCusipPortfolioDataResponse;
import com.coherentlogic.cusip.global.services.model.GetDomiciles;
import com.coherentlogic.cusip.global.services.model.GetDomicilesResponse;
import com.coherentlogic.cusip.global.services.model.GetGovernmentMortgages;
import com.coherentlogic.cusip.global.services.model.GetGovernmentMortgagesResponse;
import com.coherentlogic.cusip.global.services.model.GetInternationalIdentifiers;
import com.coherentlogic.cusip.global.services.model.GetInternationalIdentifiersResponse;
import com.coherentlogic.cusip.global.services.model.GetIssueAttributes;
import com.coherentlogic.cusip.global.services.model.GetIssueAttributesResponse;
import com.coherentlogic.cusip.global.services.model.GetLinkedIssuesData;
import com.coherentlogic.cusip.global.services.model.GetLinkedIssuesDataResponse;
import com.coherentlogic.cusip.global.services.model.GetOptionsData;
import com.coherentlogic.cusip.global.services.model.GetOptionsDataResponse;
import com.coherentlogic.cusip.global.services.model.GetSblData;
import com.coherentlogic.cusip.global.services.model.GetSblDataResponse;
import com.coherentlogic.cusip.global.services.model.GetStates;
import com.coherentlogic.cusip.global.services.model.GetStatesResponse;
import com.coherentlogic.cusip.global.services.model.Login;
import com.coherentlogic.cusip.global.services.model.LoginResponse;
import com.coherentlogic.cusip.global.services.model.OptionsDetail;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
public class GlobalConfiguration {

    @Bean(name="accessManagerWebServiceTemplate")
    public WebServiceTemplate getAccessManagerWebServiceTemplate () {

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

        marshaller.setClassesToBeBound(
            Login.class,
            LoginResponse.class
        );

        WebServiceTemplate webServiceTemplate = new WebServiceTemplate ();

        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(marshaller);

        webServiceTemplate.setDefaultUri("http://204.8.134.55:21111/Access_Manager_Token_Service");

        return webServiceTemplate;
    }

    @Bean(name="idHubWebServiceTemplate")
    public WebServiceTemplate getIdHubWebServiceTemplate () {

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

        marshaller.setClassesToBeBound(
            GetCurrencies.class,
            GetCurrenciesResponse.class,
            GetCusipAttributesPortfolioData.class,
            GetCusipAttributesPortfolioDataResponse.class,
            GetCusipData.class,
            GetCusipDataResponse.class,
            GetCusipDataForTicker.class,
            GetCusipDataForTickerResponse.class,
            GetCusipPortfolioData.class,
            GetCusipPortfolioDataResponse.class,
            GetStates.class,
            GetStatesResponse.class,
            GetDomiciles.class,
            GetDomicilesResponse.class,
            GetGovernmentMortgages.class,
            GetGovernmentMortgagesResponse.class,
            GetIssueAttributes.class,
            GetIssueAttributesResponse.class,
            GetLinkedIssuesData.class,
            GetLinkedIssuesDataResponse.class,
            GetOptionsData.class,
            GetOptionsDataResponse.class,
            GetSblData.class,
            GetSblDataResponse.class,
            // The international identifier is no longer supported at CGS.
            GetInternationalIdentifiers.class,
            // The international identifier is no longer supported at CGS.
            GetInternationalIdentifiersResponse.class

        );

        WebServiceTemplate webServiceTemplate = new WebServiceTemplate ();

        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(marshaller);

        webServiceTemplate.setDefaultUri("http://idhub.cusip.com:80/idhub/IdHub");

        return webServiceTemplate;
    }

    @Autowired
    private GenericCUSIPGlobalServicesQueryBuilderFactory
        genericCUSIPGlobalServicesQueryBuilderFactory;

    @Bean
    @Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public <T, R> GenericCUSIPGlobalServicesQueryBuilder<T, R>
        getCurrenciesCUSIPGlobalServicesQueryBuilder() {

        return genericCUSIPGlobalServicesQueryBuilderFactory.getInstance();
    }
}
