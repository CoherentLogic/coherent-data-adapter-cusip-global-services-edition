package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.InternationalIdentifiers;
import com.coherentlogic.cusip.global.services.model.GetInternationalIdentifiersResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class InternationalIdentifiersAdapter
    extends AbstractAdapter<GetInternationalIdentifiersResponse, InternationalIdentifiers> {

    @Override
    public void adapt(GetInternationalIdentifiersResponse source, InternationalIdentifiers target) {
        throw new RuntimeException("MNYI!");
    }
}
