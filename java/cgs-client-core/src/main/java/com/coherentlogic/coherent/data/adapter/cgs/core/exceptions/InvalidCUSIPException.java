package com.coherentlogic.coherent.data.adapter.cgs.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InvalidCUSIPException extends NestedRuntimeException {

    public InvalidCUSIPException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InvalidCUSIPException(String msg) {
        super(msg);
    }
}
