package com.coherentlogic.coherent.data.adapter.cgs.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InvalidISINException extends NestedRuntimeException {

    public InvalidISINException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InvalidISINException(String msg) {
        super(msg);
    }
}
