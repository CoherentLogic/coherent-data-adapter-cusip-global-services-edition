package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Currencies;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetCurrencies;
import com.coherentlogic.cusip.global.services.model.GetCurrenciesResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CurrenciesBuilder extends AbstractQueryBuilder<Currencies, GetCurrencies, GetCurrenciesResponse> {

    @Autowired
    public CurrenciesBuilder(
        @Qualifier(CacheConfiguration.CURRENCIES_CACHE) CacheServiceProviderSpecification<String, Currencies> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetCurrencies, GetCurrenciesResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetCurrenciesResponse, Currencies> adapter,
        GetCurrencies request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public Currencies newInstance() {
        return new Currencies ();
    }
}
