package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class OptionsData extends ErrorBean {

    private static final long serialVersionUID = 8577242101019815967L;

    /**
     * OptionsDetail on the wsdl.
     */
    private List<OptionDetail> optionDetails = null;

    public OptionsData() {
        this (new ArrayList<OptionDetail> ());
    }

    public OptionsData(List<OptionDetail> optionDetails) {
        this.optionDetails = optionDetails;
    }

    public List<OptionDetail> getOptionDetails() {
        return optionDetails;
    }

    public void setOptionsDetails(List<OptionDetail> optionDetails) {
        this.optionDetails = optionDetails;
    }
}
