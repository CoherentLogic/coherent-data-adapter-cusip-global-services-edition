package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AIData;
import com.coherentlogic.cusip.global.services.model.GetAIDataResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class AIDataAdapter extends AbstractAdapter<GetAIDataResponse, AIData> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPDataAdapter.class);

    @Override
    public void adapt(GetAIDataResponse source, AIData target) {
        throw new RuntimeException("MNYE");
    }
}
