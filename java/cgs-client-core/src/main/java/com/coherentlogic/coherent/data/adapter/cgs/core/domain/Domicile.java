package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

/**
 * Maps to DomicileDetail.
 *
 * @deprecated We may refactor this class to use java.util.Locale so, while the class will not be removed, consider
 *  yourself warned.
 *
 * 
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Domicile extends ErrorBean {

    /**
     * @todo Consider converting to java.util.Locale.
     */
    private String code;

    private String description;

    private String isoCode;

    public Domicile() {

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    @Override
    public String toString() {
        return "Domicile [code=" + code + ", description=" + description + ", isoCode=" + isoCode + "]";
    }
}
