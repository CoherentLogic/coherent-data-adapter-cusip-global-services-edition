package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.examples.Application.BasicHttpComponentsMessageSender;
import com.coherentlogic.coherent.data.model.core.builders.soap.AbstractSOAPQueryBuilder;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.Login;
import com.coherentlogic.cusip.global.services.model.LoginResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LoginBuilder
    extends AbstractSOAPQueryBuilder<String, com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login>
    implements AuthenticationSpecification<LoginBuilder> {

    private static final Logger log = LoggerFactory.getLogger(LoginBuilder.class);

    private String userId, password;

    /**
     * 
     */
    @Autowired
    public LoginBuilder(
        WebServiceTemplate accessManagerWebServiceTemplate,
        @Qualifier(CacheConfiguration.LOGIN_CACHE) CacheServiceProviderSpecification
            <String, com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login> cacheServiceProvider
    ) {
        super(accessManagerWebServiceTemplate, cacheServiceProvider);
    }

    /**
     * <b>You</b> are required to provide your own userId and password in order to authenticate with CUSIP Global
     * Services (CGS) -- if you do not have a license with CGS one can be acquired by 
     * <a href="mailto:CGS_license@cusip.com?subject=License%20request%20to%20use%20the%20Coherent%20Data%20Adapter:%20CUSIP%20Global%20Services%20Web%20Edition">contacting CGS via email</a>.
     */
    @Override
    public LoginBuilder withUserId (String userId) {

        this.userId = userId;

        return this;
    }

    /**
     * <b>You</b> are required to provide your own userId and password in order to authenticate with CUSIP Global
     * Services (CGS) -- if you do not have a license with CGS one can be acquired by 
     * <a href="mailto:CGS_license@cusip.com?subject=License%20request%20to%20use%20the%20Coherent%20Data%20Adapter:%20CUSIP%20Global%20Services%20Web%20Edition">contacting CGS via email</a>.
     */
    @Override
    public LoginBuilder withPassword (String password) {

        this.password = password;

        return this;
    }

    @Override
    public com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login doGet() {

        log.info ("userId: " + ((userId == null) ? "null" : "not null") + ", password: " +
            ((password == null) ? "null" : "not null"));

        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials (userId, password);

        HttpComponentsMessageSender sender = new BasicHttpComponentsMessageSender (credentials);

        sender.setCredentials(credentials);

        Login login = new Login ();

        WebServiceTemplate accessManagerWebServiceTemplate = getWebServiceTemplate();

        accessManagerWebServiceTemplate.setMessageSender(sender);

        LoginResponse loginResponse = (LoginResponse) accessManagerWebServiceTemplate.marshalSendAndReceive(
            login,
            new WebServiceMessageCallback() {
                // http://stackoverflow.com/questions/14571273/spring-ws-client-not-setting-soapaction-header
                public void doWithMessage(WebServiceMessage message) {
                    ((SoapMessage)message).setSoapAction("http://sso.soapstation.actional.com/login");
                }
            }
        );

        log.info ("...done!");

        String binarySecurityToken = loginResponse.getLoginReturn();

        log.info ("binarySecurityToken: " + binarySecurityToken);

        com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login result =
            new com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login ();

        result.setBinarySecurityToken(binarySecurityToken);

        return result;
    }
}
