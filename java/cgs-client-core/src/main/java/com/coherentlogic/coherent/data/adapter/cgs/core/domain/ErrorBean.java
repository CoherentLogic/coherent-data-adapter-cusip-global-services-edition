package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @todo Rename this class to Error or something similar.
 */
public class ErrorBean extends IdentityBean {

    /**
     * 
     */
    private static final long serialVersionUID = 3203437822359938609L;

    private List<Error> errors;

    public ErrorBean () {
        this(new ArrayList<Error> ());
    }

    public ErrorBean(List<Error> errors) {
        super();
        this.errors = errors;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
}
