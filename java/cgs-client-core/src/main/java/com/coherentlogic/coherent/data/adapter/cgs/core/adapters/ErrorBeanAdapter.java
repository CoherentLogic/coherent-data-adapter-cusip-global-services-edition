package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error.FaultType;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.ErrorBean;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.cusip.global.services.model.ErrorList;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class ErrorBeanAdapter
    implements InOutAdapterSpecification<com.coherentlogic.cusip.global.services.model.ErrorList, ErrorBean> {

    private static final Logger log = LoggerFactory.getLogger(ErrorBeanAdapter.class);

    public ErrorBeanAdapter() {
    }

    @Override
    public void adapt(ErrorList source, ErrorBean target) {

        source
            .getErrorDetail()
            .forEach(
                errorDetail -> {

                  String actor = errorDetail.getFaultActor();
                  String code = errorDetail.getFaultCode();
                  // Potential bug here in that the faultString in SoapUI is non-null, whereas here it's null, and
                  // this appears to be a problem in the generated code.
                  String description = errorDetail.getFaultString();
                  String faultType = errorDetail.getFaultType();

                  if (description == null)
                      log.warn("The description is null and this may be a possible defect.");

                  FaultType eFaultType = FaultType.valueOf(faultType);

                  Error error = new Error(actor, code, eFaultType, description);

                  target.getErrors().add(error);
                }
            );
    }
}
