package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;

public abstract class AbstractAdapter<S, T> implements InOutAdapterSpecification<S, T> {

    private static final Logger log = LoggerFactory.getLogger(AbstractAdapter.class);

//    /**
//     * 
//     * @param source
//     * @param target
//     * 
//     * @todo Complete the implementation of this method.
//     */
//    protected static List<Error> adapt(
//        com.coherentlogic.cusip.global.services.model.ErrorList source
//    ) {
//        int errorCount = source.getErrorCount();
//
//        List<ErrorDetail> sourceErrorDetails = source.getErrorDetail();
//
//        List<Error> errors = new ArrayList<Error> ();
//
//        sourceErrorDetails.forEach(
//            sourceErrorDetail -> {
//                String actor = sourceErrorDetail.getFaultActor();
//                String code = sourceErrorDetail.getFaultCode();
//                String description = sourceErrorDetail.getFaultString();
//                String faultType = sourceErrorDetail.getFaultType();
//
//                log.error("((((((((((((((((((((((((((((((((((((((((( description: " + description);
//
//                FaultType eFaultType = FaultType.valueOf(faultType);
//
//                com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error errorDetail =
//                    new com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error(
//                        actor, code, eFaultType, description);
//
//                errors.add(errorDetail);
//            }
//        );
//
//        return errors;
//    }
}
