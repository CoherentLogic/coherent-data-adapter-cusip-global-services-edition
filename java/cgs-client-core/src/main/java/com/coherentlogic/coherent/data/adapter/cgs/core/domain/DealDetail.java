package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DealDetail extends IdentityBean {

    private static final long serialVersionUID = -4765607538722152838L;

    private List<FacilityDetail> facilityDetails = null;

    public DealDetail () {
        this (new ArrayList<FacilityDetail> ());
    }

    public DealDetail (List<FacilityDetail> facilityDetails) {
        this.facilityDetails = facilityDetails;
    }

    public List<FacilityDetail> getFacilityDetails() {
        return facilityDetails;
    }

    public void setFacilityDetails(List<FacilityDetail> facilityDetails) {
        this.facilityDetails = facilityDetails;
    }

    private String agent;

    public void setAgent (String agent) {
        this.agent = agent;
    }

    public String getAgent () {
        return agent;
    }

    private String amendedCreditAgreementDate;

    public void setAmendedCreditAgreementDate (String amendedCreditAgreementDate) {
        this.amendedCreditAgreementDate = amendedCreditAgreementDate;
    }

    public String getAmendedCreditAgreementDate () {
        return amendedCreditAgreementDate;
    }

    private String borrowerIsoCountryCode;

    public void setBorrowerIsoCountryCode (String borrowerIsoCountryCode) {
        this.borrowerIsoCountryCode = borrowerIsoCountryCode;
    }

    public String getBorrowerIsoCountryCode () {
        return borrowerIsoCountryCode;
    }

    private String borrowerName;

    public void setBorrowerName (String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getBorrowerName () {
        return borrowerName;
    }

    private String borrowerStateCode;

    public void setBorrowerStateCode (String borrowerStateCode) {
        this.borrowerStateCode = borrowerStateCode;
    }

    public String getBorrowerStateCode () {
        return borrowerStateCode;
    }

    private String creditAgreementDate;

    public void setCreditAgreementDate (String creditAgreementDate) {
        this.creditAgreementDate = creditAgreementDate;
    }

    public String getCreditAgreementDate () {
        return creditAgreementDate;
    }

    private String dealAdl;

    public void setDealAdl (String dealAdl) {
        this.dealAdl = dealAdl;
    }

    public String getDealAdl () {
        return dealAdl;
    }

    private String dealAmount;

    public void setDealAmount (String dealAmount) {
        this.dealAmount = dealAmount;
    }

    public String getDealAmount () {
        return dealAmount;
    }

    private String dealEstimatedFlag;

    public void setDealEstimatedFlag (String dealEstimatedFlag) {
        this.dealEstimatedFlag = dealEstimatedFlag;
    }

    public String getDealEstimatedFlag () {
        return dealEstimatedFlag;
    }

    private String dealIsoCurrencyCode;

    public void setDealIsoCurrencyCode (String dealIsoCurrencyCode) {
        this.dealIsoCurrencyCode = dealIsoCurrencyCode;
    }

    public String getDealIsoCurrencyCode () {
        return dealIsoCurrencyCode;
    }

    private String dealLinkage;

    public void setDealLinkage (String dealLinkage) {
        this.dealLinkage = dealLinkage;
    }

    public String getDealLinkage () {
        return dealLinkage;
    }

    private String dealReleaseDate;

    public void setDealReleaseDate (String dealReleaseDate) {
        this.dealReleaseDate = dealReleaseDate;
    }

    public String getDealReleaseDate () {
        return dealReleaseDate;
    }

    private String facilityList;

    public void setFacilityList (String facilityList) {
        this.facilityList = facilityList;
    }

    public String getFacilityList () {
        return facilityList;
    }

    private String issuerNumber;

    public void setIssuerNumber (String issuerNumber) {
        this.issuerNumber = issuerNumber;
    }

    public String getIssuerNumber () {
        return issuerNumber;
    }

    private String legalJurisdiction;

    public void setLegalJurisdiction (String legalJurisdiction) {
        this.legalJurisdiction = legalJurisdiction;
    }

    public String getLegalJurisdiction () {
        return legalJurisdiction;
    }
}
