package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.InternationalIdentifiers;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetInternationalIdentifiers;
import com.coherentlogic.cusip.global.services.model.GetInternationalIdentifiersResponse;

/**
 * @deprecated This is no longer supported at CGS!
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class InternationalIdentifiersBuilder extends
    AbstractQueryBuilder<InternationalIdentifiers, GetInternationalIdentifiers, GetInternationalIdentifiersResponse> {

    private static final Logger log = LoggerFactory.getLogger(InternationalIdentifiersBuilder.class);

    @Autowired
    public InternationalIdentifiersBuilder(
        @Qualifier(CacheConfiguration.INTERNATIONAL_IDENTIFIERS_CACHE)
            CacheServiceProviderSpecification<String, InternationalIdentifiers> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetInternationalIdentifiers, GetInternationalIdentifiersResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetInternationalIdentifiersResponse, InternationalIdentifiers> adapter,
        GetInternationalIdentifiers request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public InternationalIdentifiers newInstance() {
        return new InternationalIdentifiers ();
    }
}
