package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class SBLData extends ErrorBean {

    private static final long serialVersionUID = 4417464168073386232L;

    private List<DealDetail> dealDetails;

    public SBLData() {
        this (new ArrayList<DealDetail> ());
    }

    public SBLData(List<DealDetail> dealDetails) {
        this.dealDetails = dealDetails;
    }

    public List<DealDetail> getDealDetails() {
        return dealDetails;
    }

    public void setDealDetails(List<DealDetail> dealDetails) {
        this.dealDetails = dealDetails;
    }
}
