package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domiciles;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetDomiciles;
import com.coherentlogic.cusip.global.services.model.GetDomicilesResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DomicilesBuilder extends AbstractQueryBuilder<Domiciles, GetDomiciles, GetDomicilesResponse> {

    @Autowired
    public DomicilesBuilder(
        @Qualifier(CacheConfiguration.DOMICILES_CACHE)
            CacheServiceProviderSpecification<String, Domiciles> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetDomiciles, GetDomicilesResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetDomicilesResponse, Domiciles> adapter,
        GetDomiciles request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public Domiciles newInstance() {
        return new Domiciles ();
    }
}
