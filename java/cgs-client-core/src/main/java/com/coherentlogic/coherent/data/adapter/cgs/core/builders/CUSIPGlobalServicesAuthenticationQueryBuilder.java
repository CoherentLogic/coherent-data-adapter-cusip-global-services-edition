package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * A builder which is used specifically for authentication with CUSIP Global Services.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component//("cusipGlobalServicesAuthenticationQueryBuilder")
public class CUSIPGlobalServicesAuthenticationQueryBuilder {

    private static final Logger log =
        LoggerFactory.getLogger(CUSIPGlobalServicesAuthenticationQueryBuilder.class);

    @Autowired
    private ApplicationContext applicationContext;

    /**
     * <b>You</b> are required to provide your own userId and password in order to authenticate with CUSIP Global
     * Services (CGS) -- if you do not have a license with CGS one can be acquired by 
     * <a href="mailto:CGS_license@cusip.com?subject=License%20request%20to%20use%20the%20Coherent%20Data%20Adapter:%20CUSIP%20Global%20Services%20Web%20Edition">contacting CGS via email</a>.
     */
    public LoginBuilder login () {

        LoginBuilder loginBuilder = applicationContext.getBean(LoginBuilder.class);

        log.info("loginBuilder: " + loginBuilder);

        return loginBuilder;// applicationContext.getBean(LoginBuilder.class);
    }
}
