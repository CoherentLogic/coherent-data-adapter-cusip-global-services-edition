package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateList;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.States;
import com.coherentlogic.cusip.global.services.model.GetStatesResponse;
import com.coherentlogic.cusip.global.services.model.StatesResponse;

@Component
public class StatesAdapter extends AbstractAdapter<GetStatesResponse, States> {

    private static final Logger log = LoggerFactory.getLogger(StatesAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetStatesResponse source, States target) {

        StatesResponse statesResponse = source.getReturn();

        com.coherentlogic.cusip.global.services.model.ErrorList sErrorList = statesResponse.getErrorList();

        com.coherentlogic.cusip.global.services.model.StateList sStateList = statesResponse.getStateList();

        com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateList tStateList =
            new com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateList ();

        long stateCount = sStateList.getStateCount();

        tStateList.setStateCount(stateCount);

        errorBeanAdapter.adapt(sErrorList, target);

        adapt(sStateList, tStateList);

        target.setStateList(tStateList);
    }

    void adapt(
        com.coherentlogic.cusip.global.services.model.StateList source,
        com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateList target
    ) {
        final List<com.coherentlogic.cusip.global.services.model.StateDetail> sStateDetails = source.getStateDetail();

        sStateDetails.forEach(
            sStateDetail -> {

                com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateDetail tStateDetail = new
                    com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateDetail();

                String stateCodeText = sStateDetail.getStateCode();
                String stateDescr = sStateDetail.getStateDescr();

                log.info("Adding state details with stateCodeText: " + stateCodeText + ", stateDescr: " + stateDescr);

                tStateDetail.setCode(stateCodeText);
                tStateDetail.setDescription(stateDescr);

                target.getStateDetails().add(tStateDetail);
            }
        );
    }
}
