package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Error extends IdentityBean {

    private static final long serialVersionUID = 7458289596383760178L;

    public static final String ERROR = "error", INFORMATIONAL = "informational";

    public static enum FaultType {

        E("E"), I("I");

        private String value;

        private FaultType(String value) {
            this.value = value;
//            if ("e".equals (value) || "E".equals (value))
//                this.value = ERROR;
//            else if ("i".equals (value) || "I".equals (value))
//                this.value = INFORMATIONAL;
//            else
//                throw new IllegalStateException("The value '" + value + "' is invalid.");
        }

        @Override
        public String toString() {

            String result = null;

            if ("e".equals (value) || "E".equals (value))
                result = ERROR;
            else if ("i".equals (value) || "I".equals (value))
                result = INFORMATIONAL;
          else
              throw new IllegalStateException("The value '" + value + "' is invalid.");

            return result;
        }
    }

    private String actor;

    /**
     * http://www.w3.org/TR/2000/NOTE-SOAP-20000508/#_Toc478383510
     */
    private String code;

    private FaultType faultType;

    private String description;

    public Error() {
    }

    public Error(String actor, String code, FaultType faultType, String description) {
        super();
        this.actor = actor;
        this.code = code;
        this.faultType = faultType;
        this.description = description;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public FaultType getFaultType() {
        return faultType;
    }

    public void setFaultType(FaultType faultType) {
        this.faultType = faultType;
    }

    public synchronized String getDescription() {
        return description;
    }

    public synchronized void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Error [actor=" + actor + ", code=" + code + ", faultType=" + faultType + ", description=" + description
            + "]";
    }
}
