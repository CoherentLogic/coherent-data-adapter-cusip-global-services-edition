package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.OptionsData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetOptionsData;
import com.coherentlogic.cusip.global.services.model.GetOptionsDataResponse;
import com.coherentlogic.cusip.global.services.model.OptionsRequest;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class OptionsDataBuilder extends AbstractQueryBuilder<OptionsData, GetOptionsData, GetOptionsDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(OptionsDataBuilder.class);

    @Autowired
    public OptionsDataBuilder(
        @Qualifier(CacheConfiguration.OPTIONS_DATA_CACHE)
            CacheServiceProviderSpecification<String, OptionsData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetOptionsData, GetOptionsDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetOptionsDataResponse, OptionsData> adapter,
        GetOptionsData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public OptionsData newInstance() {
        return new OptionsData();
    }

    public OptionsDataBuilder withActiveContractsOnly (String activeContractsOnly) {

        log.info ("method begins; activeContractsOnly: " + activeContractsOnly);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setActiveContractsOnly(activeContractsOnly);

        return this;
    }

    public OptionsDataBuilder withContractNameContains (String contractNameContains) {

        log.info ("method begins; contractNameContains: " + contractNameContains);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setContractNameContains(contractNameContains);

        return this;
    }

    public OptionsDataBuilder withContractNameStartsWith (String contractNameStartsWith) {

        log.info ("method begins; contractNameStartsWith: " + contractNameStartsWith);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setContractNameStartsWith(contractNameStartsWith);

        return this;
    }

    public OptionsDataBuilder withOccCode (String occCode) {

        log.info ("method begins; occCode: " + occCode);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setOccCode(occCode);

        return this;
    }

    public OptionsDataBuilder withOptionCUSIP (String optionCUSIP) {

        log.info ("method begins; optionCUSIP: " + optionCUSIP);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setOptionCusip(optionCUSIP);

        return this;
    }

    public OptionsDataBuilder withOptionISIN (String optionISIN) {

        log.info ("method begins; optionISIN: " + optionISIN);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setOptionIsin(optionISIN);

        return this;
    }

    public OptionsDataBuilder withPromptDate (String promptDate) {

        log.info ("method begins; promptDate: " + promptDate);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setPromptDate(promptDate);

        return this;
    }

    public OptionsDataBuilder withPutCallIndicator (String putCallIndicator) {

        log.info ("method begins; putCallIndicator: " + putCallIndicator);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setPutCallIndicator(putCallIndicator);

        return this;
    }

    public OptionsDataBuilder withStrikePrice (String strikePrice) {

        log.info ("method begins; strikePrice: " + strikePrice);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setStrikePrice(strikePrice);

        return this;
    }

    public OptionsDataBuilder withUnderlyingCUSIP (String underlyingCUSIP) {

        log.info ("method begins; underlyingCUSIP: " + underlyingCUSIP);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setUnderlyingCusip(underlyingCUSIP);

        return this;
    }

    public OptionsDataBuilder withUnderlyingISIN (String underlyingISIN) {

        log.info ("method begins; underlyingISIN: " + underlyingISIN);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setUnderlyingIsin(underlyingISIN);

        return this;
    }

    public OptionsDataBuilder withUnderlyingSymbol (String underlyingSymbol) {

        log.info ("method begins; underlyingSymbol: " + underlyingSymbol);

        GetOptionsData request = getRequest();

        OptionsRequest optionsRequest = request.getRequest();

        optionsRequest.setUnderlyingSymbol(underlyingSymbol);

        return this;
    }
}
