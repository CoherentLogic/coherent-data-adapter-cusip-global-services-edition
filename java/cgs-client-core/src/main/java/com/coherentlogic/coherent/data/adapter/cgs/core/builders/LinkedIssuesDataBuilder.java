package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.LinkedIssuesData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetLinkedIssuesData;
import com.coherentlogic.cusip.global.services.model.GetLinkedIssuesDataResponse;
import com.coherentlogic.cusip.global.services.model.LinkedIssuesRequest;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class LinkedIssuesDataBuilder
    extends AbstractQueryBuilder<LinkedIssuesData, GetLinkedIssuesData, GetLinkedIssuesDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(LinkedIssuesDataBuilder.class);

    @Autowired
    public LinkedIssuesDataBuilder(
        @Qualifier(CacheConfiguration.LINKED_ISSUES_DATA_CACHE)
            CacheServiceProviderSpecification<String, LinkedIssuesData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetLinkedIssuesData, GetLinkedIssuesDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetLinkedIssuesDataResponse, LinkedIssuesData> adapter,
        GetLinkedIssuesData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    @Override
    public LinkedIssuesData newInstance() {
        return new LinkedIssuesData();
    }

    public LinkedIssuesDataBuilder withCUSIP (String cusip) {

        log.info ("method begins; cusip: " + cusip);

        GetLinkedIssuesData request = getRequest();

        LinkedIssuesRequest linkedIssuesRequest = request.getRequest();

        linkedIssuesRequest.setCusip(cusip);

        return this;
    }
}
