package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

public class CUSIPTickerIssuerDetails extends CUSIPTickerIssueDetail {

    private final List<CUSIPTickerIssueDetail> cusipTickerIssueDetails;

    public CUSIPTickerIssuerDetails () {
        this (new ArrayList<CUSIPTickerIssueDetail> ());
    }

    public CUSIPTickerIssuerDetails (List<CUSIPTickerIssueDetail> cusipTickerIssueDetails) {
        super ();
        this.cusipTickerIssueDetails = cusipTickerIssueDetails;
    }

    /**
     * @deprecated We should rename this method because to get this object we need to do this:
     *
     *     dCUSIPTickerIssuerDetails.getCUSIPTickerIssueDetails().add(dCUSIPTickerIssueDetail);
     */
    public List<CUSIPTickerIssueDetail> getCUSIPTickerIssueDetails() {
        return cusipTickerIssueDetails;
    }

    @Override
    public String toString() {
        return "CUSIPTickerIssuerDetails [cusipTickerIssueDetails=" + cusipTickerIssueDetails + ", getIssueCheck()="
            + getIssueCheck() + ", getDescription()=" + getDescription() + ", getNumber()=" + getNumber()
            + ", getStatus()=" + getStatus() + "]";
    }
}