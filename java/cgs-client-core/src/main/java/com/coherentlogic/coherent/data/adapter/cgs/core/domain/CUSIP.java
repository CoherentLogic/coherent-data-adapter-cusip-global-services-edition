package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.regex.Pattern;

import com.coherentlogic.coherent.data.adapter.cgs.core.exceptions.InvalidCUSIPException;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * [1] The first six characters are known as the base (or CUSIP-6), and uniquely identify the issuer. Issuer codes are
 * assigned alphabetically from a series that includes deliberate built-in "gaps" for future expansion. The 7th and 8th
 * digit identify the exact issue. The 9th digit is an automatically generated checksum (some clearing bodies ignore or
 * truncate the last digit). The last three characters of the issuer code can be letters, in order to provide more room
 * for expansion.
 *
 * @see <a href="https://en.wikipedia.org/wiki/CUSIP">[1]</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CUSIP extends SerializableBean implements ValidatableSpecification<String> {

    static final String VALID_CUSIP_PATTERN = "^[a-zA-Z0-9]{9}$";

    private String cusip;

    public CUSIP() {
    }

    public String getCUSIP() {
        return cusip;
    }

    public void setCUSIP(String cusip) {
        this.cusip = validate (cusip);
    }

    @Override
    public String validate (String cusip) {

        if (cusip != null && !Pattern.matches(VALID_CUSIP_PATTERN, cusip) )
            throw new InvalidCUSIPException ("The CUSIP " + cusip + " must be an alphanumeric string of length nine.");

        return cusip;
    }
}
