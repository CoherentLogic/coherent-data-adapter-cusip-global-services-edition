package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueDetail;
import com.coherentlogic.cusip.global.services.model.GetCusipDataResponse;
import com.coherentlogic.cusip.global.services.model.IssueList;
import com.coherentlogic.cusip.global.services.model.IssuerDetail;
import com.coherentlogic.cusip.global.services.model.IssuerList;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class CUSIPDataAdapter extends AbstractAdapter<GetCusipDataResponse, CUSIPData> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPDataAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetCusipDataResponse source, CUSIPData target) {

        com.coherentlogic.cusip.global.services.model.ErrorList sErrorList = source.getCusipRequest().getErrorList();

        errorBeanAdapter.adapt(sErrorList, target);

        IssuerList sIssuerList = source
            .getCusipRequest()
            .getIssuerList();

        adapt (sIssuerList, target);
    }

    void adapt (IssuerList sIssuerList, CUSIPData target) {

        List<IssuerDetail> sIssuerDetails = sIssuerList.getIssuerDetail();

        sIssuerDetails.forEach(
            nextSIssuerDetail -> {

                com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssuerDetail dIssuerDetail =
                    new com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssuerDetail();

                adapt (nextSIssuerDetail, dIssuerDetail);

                target.getIssuerDetails().add(dIssuerDetail);
            }
        );
    }

    void adapt (IssuerDetail mIssuerDetail, com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssuerDetail dIssuerDetail) {

        IssueList issueList = mIssuerDetail.getIssueList ();

        List<IssueDetail> dIssueDetails = dIssuerDetail.getIssueDetailList();

        log.debug ("issueList: " +  issueList);

        adapt (issueList, dIssueDetails);

        String issuerAdditionalInfo = mIssuerDetail.getIssuerAdditionalInfo ();

        log.debug ("issuerAdditionalInfo: " +  issuerAdditionalInfo);

        dIssuerDetail.setIssuerAdditionalInfo(issuerAdditionalInfo);

        String issuerBankBranchIndicator = mIssuerDetail.getIssuerBankBranchIndicator ();

        log.debug ("issuerBankBranchIndicator: " +  issuerBankBranchIndicator);

        dIssuerDetail.setIssuerBankBranchIndicator(issuerBankBranchIndicator);

        String issuerCPInstitutionType = mIssuerDetail.getIssuerCPInstitutionType ();

        log.debug ("issuerCPInstitutionType: " +  issuerCPInstitutionType);

        dIssuerDetail.setIssuerCPInstitutionType(issuerCPInstitutionType);

        String issuerDate = mIssuerDetail.getIssuerDate ();

        log.debug ("issuerDate: " +  issuerDate);

        dIssuerDetail.setIssuerDate(issuerDate);

        String issuerDescription = mIssuerDetail.getIssuerDescription ();

        log.debug ("issuerDescription: " +  issuerDescription);

        dIssuerDetail.setIssuerDescription(issuerDescription);

        String issuerDomicileCode = mIssuerDetail.getIssuerDomicileCode ();

        log.debug ("issuerDomicileCode: " +  issuerDomicileCode);

        dIssuerDetail.setIssuerDomicileCode(issuerDomicileCode);

        String issuerGroup = mIssuerDetail.getIssuerGroup ();

        log.debug ("issuerGroup: " +  issuerGroup);

        dIssuerDetail.setIssuerGroup(issuerGroup);

        String issuerLogDate = mIssuerDetail.getIssuerLogDate ();

        log.debug ("issuerLogDate: " +  issuerLogDate);

        dIssuerDetail.setIssuerLogDate(issuerLogDate);

        String issuerNumber = mIssuerDetail.getIssuerNumber ();

        log.debug ("issuerNumber: " +  issuerNumber);

        dIssuerDetail.setIssuerNumber(issuerNumber);

        String issuerReleaseDate = mIssuerDetail.getIssuerReleaseDate ();

        log.debug ("issuerReleaseDate: " +  issuerReleaseDate);

        dIssuerDetail.setIssuerReleaseDate(issuerReleaseDate);

        String issuerStateCode = mIssuerDetail.getIssuerStateCode ();

        log.debug ("issuerStateCode: " +  issuerStateCode);

        dIssuerDetail.setIssuerStateCode(issuerStateCode);

        String issuerStatus = mIssuerDetail.getIssuerStatus ();

        log.debug ("issuerStatus: " +  issuerStatus);

        dIssuerDetail.setIssuerStatus(issuerStatus);

        String issuerTransactionCode = mIssuerDetail.getIssuerTransactionCode ();

        log.debug ("issuerTransactionCode: " +  issuerTransactionCode);

        dIssuerDetail.setIssuerTransactionCode(issuerTransactionCode);

        String issuerTypeCode = mIssuerDetail.getIssuerTypeCode ();

        log.debug ("issuerTypeCode: " +  issuerTypeCode);

        dIssuerDetail.setIssuerTypeCode(issuerTypeCode);
    }

    void adapt (IssueList mIssueList, List<IssueDetail> dIssueDetails) {

        mIssueList
            .getIssueDetail()
            .forEach(
                mIssueDetail -> {

                    IssueDetail dIssueDetail = new IssueDetail ();

                    dIssueDetails.add(dIssueDetail);

                    String issueAdditionalInfo = mIssueDetail.getIssueAdditionalInfo ();

                    log.debug ("issueAdditionalInfo: " +  issueAdditionalInfo);

                    dIssueDetail.setIssueAdditionalInfo(issueAdditionalInfo);

                    String issueCheck = mIssueDetail.getIssueCheck ();

                    log.debug ("issueCheck: " +  issueCheck);

                    dIssueDetail.setIssueCheck(issueCheck);

                    String issueCurrencyCode = mIssueDetail.getIssueCurrencyCode ();

                    log.debug ("issueCurrencyCode: " +  issueCurrencyCode);

                    dIssueDetail.setIssueCurrencyCode(issueCurrencyCode);

                    String issueDate = mIssueDetail.getIssueDate ();

                    log.debug ("issueDate: " +  issueDate);

                    dIssueDetail.setIssueDate(issueDate);

                    String issueDatedDate = mIssueDetail.getIssueDatedDate ();

                    log.debug ("issueDatedDate: " +  issueDatedDate);

                    dIssueDetail.setIssueDatedDate(issueDatedDate);

                    String issueDescription = mIssueDetail.getIssueDescription ();

                    log.debug ("issueDescription: " +  issueDescription);

                    dIssueDetail.setIssueDescription(issueDescription);

                    String issueDomicileCode = mIssueDetail.getIssueDomicileCode ();

                    log.debug ("issueDomicileCode: " +  issueDomicileCode);

                    dIssueDetail.setIssueDomicileCode(issueDomicileCode);

                    String issueGroup = mIssueDetail.getIssueGroup ();

                    log.debug ("issueGroup: " +  issueGroup);

                    dIssueDetail.setIssueGroup(issueGroup);

                    String issueISINNumber = mIssueDetail.getIssueIsinNumber ();

                    log.debug ("issueISINNumber: " +  issueISINNumber);

                    dIssueDetail.setIssueISINNumber(issueISINNumber);

                    String issueLogDate = mIssueDetail.getIssueLogDate ();

                    log.debug ("issueLogDate: " +  issueLogDate);

                    dIssueDetail.setIssueLogDate(issueLogDate);

                    String issueMaturityDate = mIssueDetail.getIssueMaturityDate ();

                    log.debug ("issueMaturityDate: " +  issueMaturityDate);

                    dIssueDetail.setIssueMaturityDate(issueMaturityDate);

                    String issueNumber = mIssueDetail.getIssueNumber ();

                    log.debug ("issueNumber: " +  issueNumber);

                    dIssueDetail.setIssueNumber(issueNumber);

                    String issuePartialMaturity = mIssueDetail.getIssuePartialMaturity ();

                    log.debug ("issuePartialMaturity: " +  issuePartialMaturity);

                    dIssueDetail.setIssuePartialMaturity(issuePartialMaturity);

                    String issueRate = mIssueDetail.getIssueRate ();

                    log.debug ("issueRate: " +  issueRate);

                    dIssueDetail.setIssueRate(issueRate);

                    String issueReleaseDate = mIssueDetail.getIssueReleaseDate ();

                    log.debug ("issueReleaseDate: " +  issueReleaseDate);

                    dIssueDetail.setIssueReleaseDate(issueReleaseDate);

                    String issueStatus = mIssueDetail.getIssueStatus ();

                    log.debug ("issueStatus: " +  issueStatus);

                    dIssueDetail.setIssueStatus(issueStatus);

                    String issueTypeCode = mIssueDetail.getIssueTypeCode ();

                    log.debug ("issueTypeCode: " +  issueTypeCode);

                    dIssueDetail.setIssueTypeCode(issueTypeCode);
                }
            );
    }

    
//    IssueList issueList = mIssuerDetail.getIssueList ();
//
//    List<CUSIPData.IssueDetail> dIssueDetails = dIssuerDetail.getIssueList()
//
//    log.debug ("issueList: " +  issueList);
//
//    adapt (issueList, dIssueDetails);
    
    
    
//    void adapt (mIssueAttribute ) {
//        String issueList = mIssueAttribute.getIssueList ();
//
//        log.debug ("issueList: " +  issueList);
//
//        issueAttribute.setIssueList(issueList);
//
//        String issuerAdditionalInfo = mIssueAttribute.getIssuerAdditionalInfo ();
//
//        log.debug ("issuerAdditionalInfo: " +  issuerAdditionalInfo);
//
//        issueAttribute.setIssuerAdditionalInfo(issuerAdditionalInfo);
//
//        String issuerBankBranchIndicator = mIssueAttribute.getIssuerBankBranchIndicator ();
//
//        log.debug ("issuerBankBranchIndicator: " +  issuerBankBranchIndicator);
//
//        issueAttribute.setIssuerBankBranchIndicator(issuerBankBranchIndicator);
//
//        String issuerCPInstitutionType = mIssueAttribute.getIssuerCPInstitutionType ();
//
//        log.debug ("issuerCPInstitutionType: " +  issuerCPInstitutionType);
//
//        issueAttribute.setIssuerCPInstitutionType(issuerCPInstitutionType);
//
//        String issuerDate = mIssueAttribute.getIssuerDate ();
//
//        log.debug ("issuerDate: " +  issuerDate);
//
//        issueAttribute.setIssuerDate(issuerDate);
//
//        String issuerDescription = mIssueAttribute.getIssuerDescription ();
//
//        log.debug ("issuerDescription: " +  issuerDescription);
//
//        issueAttribute.setIssuerDescription(issuerDescription);
//
//        String issuerDomicileCode = mIssueAttribute.getIssuerDomicileCode ();
//
//        log.debug ("issuerDomicileCode: " +  issuerDomicileCode);
//
//        issueAttribute.setIssuerDomicileCode(issuerDomicileCode);
//
//        String issuerGroup = mIssueAttribute.getIssuerGroup ();
//
//        log.debug ("issuerGroup: " +  issuerGroup);
//
//        issueAttribute.setIssuerGroup(issuerGroup);
//
//        String issuerLogDate = mIssueAttribute.getIssuerLogDate ();
//
//        log.debug ("issuerLogDate: " +  issuerLogDate);
//
//        issueAttribute.setIssuerLogDate(issuerLogDate);
//
//        String issuerNumber = mIssueAttribute.getIssuerNumber ();
//
//        log.debug ("issuerNumber: " +  issuerNumber);
//
//        issueAttribute.setIssuerNumber(issuerNumber);
//
//        String issuerReleaseDate = mIssueAttribute.getIssuerReleaseDate ();
//
//        log.debug ("issuerReleaseDate: " +  issuerReleaseDate);
//
//        issueAttribute.setIssuerReleaseDate(issuerReleaseDate);
//
//        String issuerStateCode = mIssueAttribute.getIssuerStateCode ();
//
//        log.debug ("issuerStateCode: " +  issuerStateCode);
//
//        issueAttribute.setIssuerStateCode(issuerStateCode);
//
//        String issuerStatus = mIssueAttribute.getIssuerStatus ();
//
//        log.debug ("issuerStatus: " +  issuerStatus);
//
//        issueAttribute.setIssuerStatus(issuerStatus);
//
//        String issuerTransactionCode = mIssueAttribute.getIssuerTransactionCode ();
//
//        log.debug ("issuerTransactionCode: " +  issuerTransactionCode);
//
//        issueAttribute.setIssuerTransactionCode(issuerTransactionCode);
//
//        String issuerTypeCode = mIssueAttribute.getIssuerTypeCode ();
//
//        log.debug ("issuerTypeCode: " +  issuerTypeCode);
//
//        issueAttribute.setIssuerTypeCode(issuerTypeCode);
//
//    }
}
