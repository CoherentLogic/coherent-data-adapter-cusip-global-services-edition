package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.GovernmentMortgageDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.GovernmentMortgages;
import com.coherentlogic.cusip.global.services.model.GetGovernmentMortgagesResponse;

/**
 *
 * @see <a href="http://www.freddiemac.com/mbs/html/sd_pc_lookup.html">Freddie Mac's Mortgage Securities PC Security Lookup</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class GovernmentMortgagesAdapter extends AbstractAdapter<GetGovernmentMortgagesResponse, GovernmentMortgages> {

    private static final Logger log = LoggerFactory.getLogger(GovernmentMortgagesAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetGovernmentMortgagesResponse source, GovernmentMortgages target) {

        List<GovernmentMortgageDetail> dGovernmentMortgageDetails = target.getGovernmentMortgageDetails();

        source
            .getReturn()
            .getGovernmentMortgageDetailList()
            .getGovernmentMortgageDetail()
            .forEach(
                mGovernmentMortgageDetail -> {

                    GovernmentMortgageDetail dGovernmentMortgageDetail = new GovernmentMortgageDetail ();

                    dGovernmentMortgageDetails.add(dGovernmentMortgageDetail);

                    String agencyCode = mGovernmentMortgageDetail.getAgencyCode ();

                    log.debug ("agencyCode: " +  agencyCode);

                    dGovernmentMortgageDetail.setAgencyCode(agencyCode);

                    String agencyDescription = mGovernmentMortgageDetail.getAgencyDescription ();

                    log.debug ("agencyDescription: " +  agencyDescription);

                    dGovernmentMortgageDetail.setAgencyDescription(agencyDescription);

                    String issueCheck = mGovernmentMortgageDetail.getIssueCheck ();

                    log.debug ("issueCheck: " +  issueCheck);

                    dGovernmentMortgageDetail.setIssueCheck(issueCheck);

                    String issueCurrencyCode = mGovernmentMortgageDetail.getIssueCurrencyCode ();

                    log.debug ("issueCurrencyCode: " +  issueCurrencyCode);

                    dGovernmentMortgageDetail.setIssueCurrencyCode(issueCurrencyCode);

                    String issueDate = mGovernmentMortgageDetail.getIssueDate ();

                    log.debug ("issueDate: " +  issueDate);

                    dGovernmentMortgageDetail.setIssueDate(issueDate);

                    String issueDescription = mGovernmentMortgageDetail.getIssueDescription ();

                    log.debug ("issueDescription: " +  issueDescription);

                    dGovernmentMortgageDetail.setIssueDescription(issueDescription);

                    String issueDomicileCode = mGovernmentMortgageDetail.getIssueDomicileCode ();

                    log.debug ("issueDomicileCode: " +  issueDomicileCode);

                    dGovernmentMortgageDetail.setIssueDomicileCode(issueDomicileCode);

                    String issueGroup = mGovernmentMortgageDetail.getIssueGroup ();

                    log.debug ("issueGroup: " +  issueGroup);

                    dGovernmentMortgageDetail.setIssueGroup(issueGroup);

                    String issueIsinNumber = mGovernmentMortgageDetail.getIssueIsinNumber ();

                    log.debug ("issueIsinNumber: " +  issueIsinNumber);

                    dGovernmentMortgageDetail.setIssueIsinNumber(issueIsinNumber);

                    String issueNumber = mGovernmentMortgageDetail.getIssueNumber ();

                    log.debug ("issueNumber: " +  issueNumber);

                    dGovernmentMortgageDetail.setIssueNumber(issueNumber);

                    String issueTypeCode = mGovernmentMortgageDetail.getIssueTypeCode ();

                    log.debug ("issueTypeCode: " +  issueTypeCode);

                    dGovernmentMortgageDetail.setIssueTypeCode(issueTypeCode);

                    String issuerDescription = mGovernmentMortgageDetail.getIssuerDescription ();

                    log.debug ("issuerDescription: " +  issuerDescription);

                    dGovernmentMortgageDetail.setIssuerDescription(issuerDescription);

                    String issuerNumber = mGovernmentMortgageDetail.getIssuerNumber ();

                    log.debug ("issuerNumber: " +  issuerNumber);

                    dGovernmentMortgageDetail.setIssuerNumber(issuerNumber);

                    String lotNumber = mGovernmentMortgageDetail.getLotNumber ();

                    log.debug ("lotNumber: " +  lotNumber);

                    dGovernmentMortgageDetail.setLotNumber(lotNumber);

                    String parentCUSIP = mGovernmentMortgageDetail.getParentCUSIP ();

                    log.debug ("parentCUSIP: " +  parentCUSIP);

                    dGovernmentMortgageDetail.setParentCUSIP(parentCUSIP);

                    String parentPoolNumber = mGovernmentMortgageDetail.getParentPoolNumber ();

                    log.debug ("parentPoolNumber: " +  parentPoolNumber);

                    dGovernmentMortgageDetail.setParentPoolNumber(parentPoolNumber);

                    String poolNumber = mGovernmentMortgageDetail.getPoolNumber ();

                    log.debug ("poolNumber: " +  poolNumber);

                    dGovernmentMortgageDetail.setPoolNumber(poolNumber);

                    String unitNumber = mGovernmentMortgageDetail.getUnitNumber ();

                    log.debug ("unitNumber: " +  unitNumber);

                    dGovernmentMortgageDetail.setUnitNumber(unitNumber);
                }
            );
    }
}
