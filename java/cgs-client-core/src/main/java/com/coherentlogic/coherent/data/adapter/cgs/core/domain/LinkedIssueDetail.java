package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class LinkedIssueDetail extends IdentityBean {

    private static final long serialVersionUID = -335025360689901723L;

    private String linkIssueAdditionalInfo;

    public void setLinkIssueAdditionalInfo (String linkIssueAdditionalInfo) {
        this.linkIssueAdditionalInfo = linkIssueAdditionalInfo;
    }

    public String getLinkIssueAdditionalInfo () {
        return linkIssueAdditionalInfo;
    }

    private String linkIssueCheck;

    public void setLinkIssueCheck (String linkIssueCheck) {
        this.linkIssueCheck = linkIssueCheck;
    }

    public String getLinkIssueCheck () {
        return linkIssueCheck;
    }

    private String linkIssueDescription;

    public void setLinkIssueDescription (String linkIssueDescription) {
        this.linkIssueDescription = linkIssueDescription;
    }

    public String getLinkIssueDescription () {
        return linkIssueDescription;
    }

    private String linkIssueNumber;

    public void setLinkIssueNumber (String linkIssueNumber) {
        this.linkIssueNumber = linkIssueNumber;
    }

    public String getLinkIssueNumber () {
        return linkIssueNumber;
    }

    private String linkIssuerDescription;

    public void setLinkIssuerDescription (String linkIssuerDescription) {
        this.linkIssuerDescription = linkIssuerDescription;
    }

    public String getLinkIssuerDescription () {
        return linkIssuerDescription;
    }

    private String linkIssuerNumber;

    public void setLinkIssuerNumber (String linkIssuerNumber) {
        this.linkIssuerNumber = linkIssuerNumber;
    }

    public String getLinkIssuerNumber () {
        return linkIssuerNumber;
    }
}
/*
<java:LinkIssueAdditionalInfo xsi:nil="true"/>
<java:LinkIssueCheck>0</java:LinkIssueCheck>
<java:LinkIssueDescription>COM</java:LinkIssueDescription>
<java:LinkIssueNumber>10</java:LinkIssueNumber>
<java:LinkIssuerDescription>INTEL CORP</java:LinkIssuerDescription>
<java:LinkIssuerNumber>458140</java:LinkIssuerNumber>
*/