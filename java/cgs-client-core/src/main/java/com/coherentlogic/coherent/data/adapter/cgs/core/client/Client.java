package com.coherentlogic.coherent.data.adapter.cgs.core.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesIdHubQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesAuthenticationQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AuthenticationHolder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPData;

public class Client {

    private static final Logger log = LoggerFactory.getLogger(Client.class);

    private AbstractApplicationContext context = null;

    public Client initialize () {

        context = new ClassPathXmlApplicationContext("spring/application-context.xml");

        context.registerShutdownHook();
        context.start();

        return this;
    }

    public AbstractApplicationContext getContext() {
        return context;
    }

    public CUSIPGlobalServicesAuthenticationQueryBuilder getAuthenticationQueryBuilder() {
        return context.getBean(CUSIPGlobalServicesAuthenticationQueryBuilder.class);
    }

    public CUSIPGlobalServicesIdHubQueryBuilder getIdHubQueryBuilder() {
        return context.getBean(CUSIPGlobalServicesIdHubQueryBuilder.class);
    }

    public AuthenticationHolder getAuthenticationHolder() {
        return context.getBean(AuthenticationHolder.class);
    }

    public static void main (String[] unused) {

        String userId = System.getenv("CGS_USERID");
        String password = System.getenv("CGS_PASSWORD");

        Client client = new Client ();

        String binarySecurityToken =
            client
                .initialize()
                .getAuthenticationQueryBuilder()
                .login()
                .withUserId(userId)
                .withPassword(password)
                .doGet()
                .getBinarySecurityToken();

        log.info("binarySecurityToken: " + binarySecurityToken);

        CUSIPGlobalServicesIdHubQueryBuilder cusipGlobalServicesIdHubQueryBuilder =
            client.getIdHubQueryBuilder();

        AuthenticationHolder authenticationHolder = client.getAuthenticationHolder();

        authenticationHolder.setUserId(userId);
        authenticationHolder.setBinarySecurityToken(binarySecurityToken);

        CUSIPData cusipData = cusipGlobalServicesIdHubQueryBuilder
            .cusipData()
            .withIssueISINNumber("US0378331005")
            .doGet();

        log.info("cusipData: " + cusipData);
    }
}
