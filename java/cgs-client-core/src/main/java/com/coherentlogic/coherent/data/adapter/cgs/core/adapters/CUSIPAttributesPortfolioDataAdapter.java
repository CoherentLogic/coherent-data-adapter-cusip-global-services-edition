package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPAttributesPortfolioData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttribute;
import com.coherentlogic.cusip.global.services.model.GetCusipAttributesPortfolioDataResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class CUSIPAttributesPortfolioDataAdapter
    extends AbstractAdapter<GetCusipAttributesPortfolioDataResponse, CUSIPAttributesPortfolioData> {

    private static final Logger log = LoggerFactory.getLogger(CUSIPDataAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(GetCusipAttributesPortfolioDataResponse source, CUSIPAttributesPortfolioData target) {

        log.error("adapt: method begins; source: " + source + ", target: " + target);

        final List<IssueAttribute> issueAttributes = target.getIssueAttributes();

        source
            .getReturn()
            .getAttributesList()
            .getIssueAttributes()
            .forEach(
                mIssueAttribute -> {

                    IssueAttribute issueAttribute = new IssueAttribute ();

                    issueAttributes.add(issueAttribute);

                    String alternativeMinimumTax = mIssueAttribute.getAlternativeMinimumTax ();

                    log.debug ("alternativeMinimumTax: " +  alternativeMinimumTax);

                    issueAttribute.setAlternativeMinimumTax(alternativeMinimumTax);

                    String auditor = mIssueAttribute.getAuditor ();

                    log.debug ("auditor: " +  auditor);

                    issueAttribute.setAuditor(auditor);

                    String bankQualified = mIssueAttribute.getBankQualified ();

                    log.debug ("bankQualified: " +  bankQualified);

                    issueAttribute.setBankQualified(bankQualified);

                    String bondCounsel = mIssueAttribute.getBondCounsel ();

                    log.debug ("bondCounsel: " +  bondCounsel);

                    issueAttribute.setBondCounsel(bondCounsel);

                    String bondForm = mIssueAttribute.getBondForm ();

                    log.debug ("bondForm: " +  bondForm);

                    issueAttribute.setBondForm(bondForm);

                    String callable = mIssueAttribute.getCallable ();

                    log.debug ("callable: " +  callable);

                    issueAttribute.setCallable(callable);

                    String certified = mIssueAttribute.getCertified ();

                    log.debug ("certified: " +  certified);

                    issueAttribute.setCertified(certified);

                    String closingDate = mIssueAttribute.getClosingDate ();

                    log.debug ("closingDate: " +  closingDate);

                    issueAttribute.setClosingDate(closingDate);

                    String cusip = mIssueAttribute.getCusip ();

                    log.debug ("cusip: " +  cusip);

                    issueAttribute.setCUSIP(cusip);

                    String dateCertified = mIssueAttribute.getDateCertified ();

                    log.debug ("dateCertified: " +  dateCertified);

                    issueAttribute.setDateCertified(dateCertified);

                    String depositoryEligible = mIssueAttribute.getDepositoryEligible ();

                    log.debug ("depositoryEligible: " +  depositoryEligible);

                    issueAttribute.setDepositoryEligible(depositoryEligible);

                    String enhancements = mIssueAttribute.getEnhancements ();

                    log.debug ("enhancements: " +  enhancements);

                    issueAttribute.setEnhancements(enhancements);

                    String etfType = mIssueAttribute.getEtfType ();

                    log.debug ("etfType: " +  etfType);

                    issueAttribute.setEtfType(etfType);

                    String financialAdvisor = mIssueAttribute.getFinancialAdvisor ();

                    log.debug ("financialAdvisor: " +  financialAdvisor);

                    issueAttribute.setFinancialAdvisor(financialAdvisor);

                    String firstCouponDate = mIssueAttribute.getFirstCouponDate ();

                    log.debug ("firstCouponDate: " +  firstCouponDate);

                    issueAttribute.setFirstCouponDate(firstCouponDate);

                    String fundDistributionPolicy = mIssueAttribute.getFundDistributionPolicy ();

                    log.debug ("fundDistributionPolicy: " +  fundDistributionPolicy);

                    issueAttribute.setFundDistributionPolicy(fundDistributionPolicy);

                    String fundInvestmentPolicy = mIssueAttribute.getFundInvestmentPolicy ();

                    log.debug ("fundInvestmentPolicy: " +  fundInvestmentPolicy);

                    issueAttribute.setFundInvestmentPolicy(fundInvestmentPolicy);

                    String fundType = mIssueAttribute.getFundType ();

                    log.debug ("fundType: " +  fundType);

                    issueAttribute.setFundType(fundType);

                    String governmentStimulusProgram = mIssueAttribute.getGovernmentStimulusProgram ();

                    log.debug ("governmentStimulusProgram: " +  governmentStimulusProgram);

                    issueAttribute.setGovernmentStimulusProgram(governmentStimulusProgram);

                    String guarantee = mIssueAttribute.getGuarantee ();

                    log.debug ("guarantee: " +  guarantee);

                    issueAttribute.setGuarantee(guarantee);

                    String incomeType = mIssueAttribute.getIncomeType ();

                    log.debug ("incomeType: " +  incomeType);

                    issueAttribute.setIncomeType(incomeType);

                    String initialPublicOffering = mIssueAttribute.getInitialPublicOffering ();

                    log.debug ("initialPublicOffering: " +  initialPublicOffering);

                    issueAttribute.setInitialPublicOffering(initialPublicOffering);

                    String insuredBy = mIssueAttribute.getInsuredBy ();

                    log.debug ("insuredBy: " +  insuredBy);

                    issueAttribute.setInsuredBy(insuredBy);

                    String isoCFICode = mIssueAttribute.getIsoCFICode ();

                    log.debug ("isoCFICode: " +  isoCFICode);

                    issueAttribute.setIsoCFICode(isoCFICode);

                    String municipalSaleDate = mIssueAttribute.getMunicipalSaleDate ();

                    log.debug ("municipalSaleDate: " +  municipalSaleDate);

                    issueAttribute.setMunicipalSaleDate(municipalSaleDate);

                    String ote = mIssueAttribute.getOTE ();

                    log.debug ("ote: " +  ote);

                    issueAttribute.setOte(ote);

                    String offeringAmount = mIssueAttribute.getOfferingAmount ();

                    log.debug ("offeringAmount: " +  offeringAmount);

                    issueAttribute.setOfferingAmount(offeringAmount);

                    String offeringAmountCode = mIssueAttribute.getOfferingAmountCode ();

                    log.debug ("offeringAmountCode: " +  offeringAmountCode);

                    issueAttribute.setOfferingAmountCode(offeringAmountCode);

                    String ownershipRestrictions = mIssueAttribute.getOwnershipRestrictions ();

                    log.debug ("ownershipRestrictions: " +  ownershipRestrictions);

                    issueAttribute.setOwnershipRestrictions(ownershipRestrictions);

                    String payingAgent = mIssueAttribute.getPayingAgent ();

                    log.debug ("payingAgent: " +  payingAgent);

                    issueAttribute.setPayingAgent(payingAgent);

                    String paymentFrequencyCode = mIssueAttribute.getPaymentFrequencyCode ();

                    log.debug ("paymentFrequencyCode: " +  paymentFrequencyCode);

                    issueAttribute.setPaymentFrequencyCode(paymentFrequencyCode);

                    String paymentStatus = mIssueAttribute.getPaymentStatus ();

                    log.debug ("paymentStatus: " +  paymentStatus);

                    issueAttribute.setPaymentStatus(paymentStatus);

                    String portal = mIssueAttribute.getPortal ();

                    log.debug ("portal: " +  portal);

                    issueAttribute.setPortal(portal);

                    String preRefunded = mIssueAttribute.getPreRefunded ();

                    log.debug ("preRefunded: " +  preRefunded);

                    issueAttribute.setPreRefunded(preRefunded);

                    String preferredType = mIssueAttribute.getPreferredType ();

                    log.debug ("preferredType: " +  preferredType);

                    issueAttribute.setPreferredType(preferredType);

                    String putable = mIssueAttribute.getPutable ();

                    log.debug ("putable: " +  putable);

                    issueAttribute.setPutable(putable);

                    String rateType = mIssueAttribute.getRateType ();

                    log.debug ("rateType: " +  rateType);

                    issueAttribute.setRateType(rateType);

                    String redemption = mIssueAttribute.getRedemption ();

                    log.debug ("redemption: " +  redemption);

                    issueAttribute.setRedemption(redemption);

                    String refundable = mIssueAttribute.getRefundable ();

                    log.debug ("refundable: " +  refundable);

                    issueAttribute.setRefundable(refundable);

                    String remarketed = mIssueAttribute.getRemarketed ();

                    log.debug ("remarketed: " +  remarketed);

                    issueAttribute.setRemarketed(remarketed);

                    String saleType = mIssueAttribute.getSaleType ();

                    log.debug ("saleType: " +  saleType);

                    issueAttribute.setSaleType(saleType);

                    String sinkingFund = mIssueAttribute.getSinkingFund ();

                    log.debug ("sinkingFund: " +  sinkingFund);

                    issueAttribute.setSinkingFund(sinkingFund);

                    String sourceDocument = mIssueAttribute.getSourceDocument ();

                    log.debug ("sourceDocument: " +  sourceDocument);

                    issueAttribute.setSourceDocument(sourceDocument);

                    String sponsoring = mIssueAttribute.getSponsoring ();

                    log.debug ("sponsoring: " +  sponsoring);

                    issueAttribute.setSponsoring(sponsoring);

                    String taxable = mIssueAttribute.getTaxable ();

                    log.debug ("taxable: " +  taxable);

                    issueAttribute.setTaxable(taxable);

                    String tenderAgent = mIssueAttribute.getTenderAgent ();

                    log.debug ("tenderAgent: " +  tenderAgent);

                    issueAttribute.setTenderAgent(tenderAgent);

                    String tickerSymbol = mIssueAttribute.getTickerSymbol ();

                    log.debug ("tickerSymbol: " +  tickerSymbol);

                    issueAttribute.setTickerSymbol(tickerSymbol);

                    String transferAgent = mIssueAttribute.getTransferAgent ();

                    log.debug ("transferAgent: " +  transferAgent);

                    issueAttribute.setTransferAgent(transferAgent);

                    String underwriter = mIssueAttribute.getUnderwriter ();

                    log.debug ("underwriter: " +  underwriter);

                    issueAttribute.setUnderwriter(underwriter);

                    String usCFICode = mIssueAttribute.getUsCFICode ();

                    log.debug ("usCFICode: " +  usCFICode);

                    issueAttribute.setUsCFICode(usCFICode);

                    String votingRights = mIssueAttribute.getVotingRights ();

                    log.debug ("votingRights: " +  votingRights);

                    issueAttribute.setVotingRights(votingRights);

                    String warrantAssets = mIssueAttribute.getWarrantAssets ();

                    log.debug ("warrantAssets: " +  warrantAssets);

                    issueAttribute.setWarrantAssets(warrantAssets);

                    String warrantStatus = mIssueAttribute.getWarrantStatus ();

                    log.debug ("warrantStatus: " +  warrantStatus);

                    issueAttribute.setWarrantStatus(warrantStatus);

                    String warrantType = mIssueAttribute.getWarrantType ();

                    log.debug ("warrantType: " +  warrantType);

                    issueAttribute.setWarrantType(warrantType);

                    String whereTraded = mIssueAttribute.getWhereTraded ();

                    log.debug ("whereTraded: " +  whereTraded);

                    issueAttribute.setWhereTraded(whereTraded);
                }
            );
//            .getUnmatchedCusip()
//            .forEach(
//                cusip -> {
//                    cusip.
//                }
//            );
        
        errorBeanAdapter.adapt(source.getReturn().getErrorList(), target);
    }
}
