package com.coherentlogic.coherent.data.adapter.cgs.core.exceptions;

import org.springframework.core.NestedRuntimeException;

public class InvalidConfigurationException extends NestedRuntimeException {

    private static final long serialVersionUID = -8778786112232920065L;

    public InvalidConfigurationException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InvalidConfigurationException(String msg) {
        super(msg);
    }
}
