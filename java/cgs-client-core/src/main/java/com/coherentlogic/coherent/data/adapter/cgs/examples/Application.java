package com.coherentlogic.coherent.data.adapter.cgs.examples;

import java.net.URI;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

import com.coherentlogic.cusip.global.services.model.Login;
import com.coherentlogic.cusip.global.services.model.LoginResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
//@SpringBootApplication//scanBasePackages="com.coherentlogic.coherent.data.adapter.cgs")
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static class BasicHttpComponentsMessageSender extends HttpComponentsMessageSender {

        private final Credentials credentials;

        public BasicHttpComponentsMessageSender (Credentials credentials) {
            this.credentials = credentials;
        }

        /**
         * @see http://www.baeldung.com/httpclient-4-basic-authentication
         */
        @Override
        protected HttpContext createContext(URI uri) {

            HttpClientContext result = HttpClientContext.create();

            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

            credentialsProvider.setCredentials(AuthScope.ANY, credentials);

            result.setCredentialsProvider(credentialsProvider);

            HttpHost targetHost = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());

            AuthCache authCache = new BasicAuthCache();

            BasicScheme basicAuth = new BasicScheme();

            authCache.put(targetHost, basicAuth);

            result.setAttribute(ClientContext.AUTH_CACHE, authCache);

            return result;
        }
    }

    // -Djava.net.preferIPv4Stack=true
    @Override
    public void run(String... args) { 

        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();

        final String userId = System.getenv("CGS_USERID");
        final String password = System.getenv("CGS_PASSWORD");

        boolean isUserIdNull = userId == null;
        boolean isPasswordNull = password == null;

        log.info ("username: [redacted / is null: " + isUserIdNull + "], password: [redacted / is null: " +
            isPasswordNull + "]");

        UsernamePasswordCredentials credentials = new UsernamePasswordCredentials (userId, password);

        HttpComponentsMessageSender sender = new BasicHttpComponentsMessageSender (credentials);

        sender.setCredentials(credentials);

        Login login = new Login ();

        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

        marshaller.setClassesToBeBound(Login.class, LoginResponse.class);

        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(marshaller);

        webServiceTemplate.setDefaultUri("http://204.8.134.55:21111/Access_Manager_Token_Service");

        webServiceTemplate.setMessageSender(sender);

        log.info ("Marshal begins... ");

        LoginResponse loginResponse = (LoginResponse) webServiceTemplate.marshalSendAndReceive(
            login,
            new WebServiceMessageCallback() {
                // http://stackoverflow.com/questions/14571273/spring-ws-client-not-setting-soapaction-header
                public void doWithMessage(WebServiceMessage message) {
                    ((SoapMessage)message).setSoapAction("http://sso.soapstation.actional.com/login");
                }
            }
        );

        log.info ("...done!");

        String result = loginResponse.getLoginReturn();

        log.info ("result: " + result);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
