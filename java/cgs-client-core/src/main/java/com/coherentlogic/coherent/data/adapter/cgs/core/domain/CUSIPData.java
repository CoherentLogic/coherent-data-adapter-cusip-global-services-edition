package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @todo Document this class.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CUSIPData extends ErrorBean {

    public CUSIPData() {
        this (new ArrayList<IssuerDetail> ());
    }

    public CUSIPData(List<IssuerDetail> issuerDetails) {
        this.issuerDetails = issuerDetails;
    }

    private List<IssuerDetail> issuerDetails;

    public List<IssuerDetail> getIssuerDetails() {
        return issuerDetails;
    }

    public void setIssuerDetails(List<IssuerDetail> issuerDetails) {
        this.issuerDetails = issuerDetails;
    }

    @Override
    public String toString() {
        return "CUSIPData [issuerDetails=" + issuerDetails + "]";
    }
}
