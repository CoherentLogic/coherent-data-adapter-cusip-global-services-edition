package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.CUSIPAttributesPortfolioDataAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.CUSIPDataAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.CUSIPDataForTickerAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.CUSIPPortfolioDataAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.CurrenciesAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.DomicilesAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.IssueAttributesAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.SBLDataAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.adapters.StatesAdapter;
import com.coherentlogic.coherent.data.adapter.cgs.core.factories.GenericCUSIPGlobalServicesQueryBuilderFactory;

/**
 *
 * @see <a href="https://www.cusip.com/cusip/fact-sheets.htm">Fact Sheets</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class CUSIPGlobalServicesIdHubQueryBuilder {

    private static final Logger log = LoggerFactory.getLogger(CUSIPGlobalServicesIdHubQueryBuilder.class);

    @Autowired
    private ApplicationContext applicationContext;

//    private String userId, binarySecurityToken;
//
//    public String getBinarySecurityToken() {
//        return binarySecurityToken;
//    }
//
//    public void setBinarySecurityToken(String binarySecurityToken) {
//        this.binarySecurityToken = binarySecurityToken;
//    }
//
//    /**
//     * @ Deprecated This has to go as we end up with an object that has a multi-stage setup (ie. we have to set the
//     * userId and the BST so that all other methods (aside from login) work). This is not clean.
//     */
//    public void setUserId(String userId) {
//        this.userId = userId;
//    }
//
//    public String getUserId() {
//        return userId;
//    }

    /**
     * @deprecated 
     */
    public AIDataBuilder aiData () {
        throw new RuntimeException ("This method not yet available *at CGS* even though it appears in the WSDL!");
    }

    @Deprecated
    private AnnaDataBuilder annaData () {
        throw new RuntimeException ("This method is not supported by CGS even though it appears in the WSDL!");
    }

    @Deprecated
    private AnnaPortfolioDataBuilder annaPortfolioData () {
        throw new RuntimeException ("This method is not supported by CGS even though it appears in the WSDL!");
    }

    public CurrenciesBuilder currencies () {
        return applicationContext.getBean(CurrenciesBuilder.class);
    }

    public CUSIPAttributesPortfolioDataBuilder cusipAttributesPortfolioData () {
        return applicationContext.getBean(CUSIPAttributesPortfolioDataBuilder.class);
    }

    public CUSIPDataBuilder cusipData () {
        return applicationContext.getBean(CUSIPDataBuilder.class);
    }

    public CUSIPDataForTickerBuilder cusipDataForTicker () {
        return applicationContext.getBean(CUSIPDataForTickerBuilder.class);
    }

    public CUSIPPortfolioDataBuilder cusipPortfolioData () {
        return applicationContext.getBean(CUSIPPortfolioDataBuilder.class);
    }

    public DomicilesBuilder domiciles () {
        return applicationContext.getBean(DomicilesBuilder.class);
    }

    public GovernmentMortgagesBuilder governmentMortgages () {
        return applicationContext.getBean(GovernmentMortgagesBuilder.class);
    }

//    This method is not supported by CGS!
//    public InternationalIdentifiersBuilder internationalIdentifiers () {
//        return applicationContext.getBean(InternationalIdentifiersBuilder.class);
//    }

//    @Deprecated
//    private IsidDataBuilder isidData () {
//        throw new RuntimeException ("This method is not supported by CGS even though it appears in the WSDL!");
//    }

    public LinkedIssuesDataBuilder linkedIssuesData () {
        return applicationContext.getBean(LinkedIssuesDataBuilder.class);
    }

    public OptionsDataBuilder optionsData () {
    	return applicationContext.getBean(OptionsDataBuilder.class);
    }

    /**
     *
     * Syndicated Bank Loan
     *
     * @return
     *
     * @Question This is not in the documentation and so far we've only received a "user not authenticated" when this
     *  method is called.
     */
    public SBLDataBuilder sblData () {
        return applicationContext.getBean(SBLDataBuilder.class);
    }

    public StatesBuilder states () {
        return applicationContext.getBean(StatesBuilder.class);
    }

    public IssueAttributesBuilder issueAttributes () {
        return applicationContext.getBean(IssueAttributesBuilder.class);
    }
}
