package com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error;
import com.coherentlogic.coherent.data.model.core.adapters.InReturnAdapterSpecification;
import com.coherentlogic.rproject.integration.dataframe.builders.JDataFrameBuilder;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
public class ErrorListJSONAdapter implements InReturnAdapterSpecification<List<Error>, String> {

    @Override
    public String adapt(List<Error> errors) {

        int size = errors.size();

        List<String> actors = new ArrayList<String> (size);
        List<String> codes = new ArrayList<String> (size);
        List<String> descriptions = new ArrayList<String> (size);
        List<String> types = new ArrayList<String> (size);

        errors.forEach(
            error -> {
                actors.add(error.getActor ());
                codes.add(error.getCode());
                descriptions.add(error.getDescription());
                types.add(error.getFaultType().toString());
            }
        );

        return (String) new JDataFrameBuilder(JDataFrameBuilder.DEFAULT_REMOTE_ADAPTER)
            .addColumn("Actors", actors.toArray())
            .addColumn("Codes", codes.toArray())
            .addColumn("Descriptions", descriptions.toArray())
            .addColumn("Types", types.toArray())
            .serialize ();
    }
}
