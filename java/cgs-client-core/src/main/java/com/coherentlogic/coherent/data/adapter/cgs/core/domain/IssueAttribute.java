package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

public class IssueAttribute extends IdentityBean implements CUSIPBeanSpecification {

    private String alternativeMinimumTax;

    public void setAlternativeMinimumTax (String alternativeMinimumTax) {
        this.alternativeMinimumTax = alternativeMinimumTax;
    }

    public String getAlternativeMinimumTax () {
        return alternativeMinimumTax;
    }

    private String auditor;

    public void setAuditor (String auditor) {
        this.auditor = auditor;
    }

    public String getAuditor () {
        return auditor;
    }

    private String bankQualified;

    public void setBankQualified (String bankQualified) {
        this.bankQualified = bankQualified;
    }

    public String getBankQualified () {
        return bankQualified;
    }

    private String bondCounsel;

    public void setBondCounsel (String bondCounsel) {
        this.bondCounsel = bondCounsel;
    }

    public String getBondCounsel () {
        return bondCounsel;
    }

    private String bondForm;

    public void setBondForm (String bondForm) {
        this.bondForm = bondForm;
    }

    public String getBondForm () {
        return bondForm;
    }

    private String callable;

    public void setCallable (String callable) {
        this.callable = callable;
    }

    public String getCallable () {
        return callable;
    }

    private String certified;

    public void setCertified (String certified) {
        this.certified = certified;
    }

    public String getCertified () {
        return certified;
    }

    private String closingDate;

    public void setClosingDate (String closingDate) {
        this.closingDate = closingDate;
    }

    public String getClosingDate () {
        return closingDate;
    }

    private String cusip;

    @Override
    public void setCUSIP (String cusip) {
        this.cusip = cusip;
    }

    @Override
    public String getCUSIP () {
        return cusip;
    }

    private String dateCertified;

    public void setDateCertified (String dateCertified) {
        this.dateCertified = dateCertified;
    }

    public String getDateCertified () {
        return dateCertified;
    }

    private String depositoryEligible;

    public void setDepositoryEligible (String depositoryEligible) {
        this.depositoryEligible = depositoryEligible;
    }

    public String getDepositoryEligible () {
        return depositoryEligible;
    }

    private String enhancements;

    public void setEnhancements (String enhancements) {
        this.enhancements = enhancements;
    }

    public String getEnhancements () {
        return enhancements;
    }

    private String etfType;

    public void setEtfType (String etfType) {
        this.etfType = etfType;
    }

    public String getEtfType () {
        return etfType;
    }

    private String financialAdvisor;

    public void setFinancialAdvisor (String financialAdvisor) {
        this.financialAdvisor = financialAdvisor;
    }

    public String getFinancialAdvisor () {
        return financialAdvisor;
    }

    private String firstCouponDate;

    public void setFirstCouponDate (String firstCouponDate) {
        this.firstCouponDate = firstCouponDate;
    }

    public String getFirstCouponDate () {
        return firstCouponDate;
    }

    private String fundDistributionPolicy;

    public void setFundDistributionPolicy (String fundDistributionPolicy) {
        this.fundDistributionPolicy = fundDistributionPolicy;
    }

    public String getFundDistributionPolicy () {
        return fundDistributionPolicy;
    }

    private String fundInvestmentPolicy;

    public void setFundInvestmentPolicy (String fundInvestmentPolicy) {
        this.fundInvestmentPolicy = fundInvestmentPolicy;
    }

    public String getFundInvestmentPolicy () {
        return fundInvestmentPolicy;
    }

    private String fundType;

    public void setFundType (String fundType) {
        this.fundType = fundType;
    }

    public String getFundType () {
        return fundType;
    }

    private String governmentStimulusProgram;

    public void setGovernmentStimulusProgram (String governmentStimulusProgram) {
        this.governmentStimulusProgram = governmentStimulusProgram;
    }

    public String getGovernmentStimulusProgram () {
        return governmentStimulusProgram;
    }

    private String guarantee;

    public void setGuarantee (String guarantee) {
        this.guarantee = guarantee;
    }

    public String getGuarantee () {
        return guarantee;
    }

    private String incomeType;

    public void setIncomeType (String incomeType) {
        this.incomeType = incomeType;
    }

    public String getIncomeType () {
        return incomeType;
    }

    private String initialPublicOffering;

    public void setInitialPublicOffering (String initialPublicOffering) {
        this.initialPublicOffering = initialPublicOffering;
    }

    public String getInitialPublicOffering () {
        return initialPublicOffering;
    }

    private String insuredBy;

    public void setInsuredBy (String insuredBy) {
        this.insuredBy = insuredBy;
    }

    public String getInsuredBy () {
        return insuredBy;
    }

    private String isoCFICode;

    public void setIsoCFICode (String isoCFICode) {
        this.isoCFICode = isoCFICode;
    }

    public String getIsoCFICode () {
        return isoCFICode;
    }

    private String municipalSaleDate;

    public void setMunicipalSaleDate (String municipalSaleDate) {
        this.municipalSaleDate = municipalSaleDate;
    }

    public String getMunicipalSaleDate () {
        return municipalSaleDate;
    }

    private String ote;

    public void setOte (String ote) {
        this.ote = ote;
    }

    public String getOte () {
        return ote;
    }

    private String offeringAmount;

    public void setOfferingAmount (String offeringAmount) {
        this.offeringAmount = offeringAmount;
    }

    public String getOfferingAmount () {
        return offeringAmount;
    }

    private String offeringAmountCode;

    public void setOfferingAmountCode (String offeringAmountCode) {
        this.offeringAmountCode = offeringAmountCode;
    }

    public String getOfferingAmountCode () {
        return offeringAmountCode;
    }

    private String ownershipRestrictions;

    public void setOwnershipRestrictions (String ownershipRestrictions) {
        this.ownershipRestrictions = ownershipRestrictions;
    }

    public String getOwnershipRestrictions () {
        return ownershipRestrictions;
    }

    private String payingAgent;

    public void setPayingAgent (String payingAgent) {
        this.payingAgent = payingAgent;
    }

    public String getPayingAgent () {
        return payingAgent;
    }

    private String paymentFrequencyCode;

    public void setPaymentFrequencyCode (String paymentFrequencyCode) {
        this.paymentFrequencyCode = paymentFrequencyCode;
    }

    public String getPaymentFrequencyCode () {
        return paymentFrequencyCode;
    }

    private String paymentStatus;

    public void setPaymentStatus (String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentStatus () {
        return paymentStatus;
    }

    private String portal;

    public void setPortal (String portal) {
        this.portal = portal;
    }

    public String getPortal () {
        return portal;
    }

    private String preRefunded;

    public void setPreRefunded (String preRefunded) {
        this.preRefunded = preRefunded;
    }

    public String getPreRefunded () {
        return preRefunded;
    }

    private String preferredType;

    public void setPreferredType (String preferredType) {
        this.preferredType = preferredType;
    }

    public String getPreferredType () {
        return preferredType;
    }

    private String putable;

    public void setPutable (String putable) {
        this.putable = putable;
    }

    public String getPutable () {
        return putable;
    }

    private String rateType;

    public void setRateType (String rateType) {
        this.rateType = rateType;
    }

    public String getRateType () {
        return rateType;
    }

    private String redemption;

    public void setRedemption (String redemption) {
        this.redemption = redemption;
    }

    public String getRedemption () {
        return redemption;
    }

    private String refundable;

    public void setRefundable (String refundable) {
        this.refundable = refundable;
    }

    public String getRefundable () {
        return refundable;
    }

    private String remarketed;

    public void setRemarketed (String remarketed) {
        this.remarketed = remarketed;
    }

    public String getRemarketed () {
        return remarketed;
    }

    private String saleType;

    public void setSaleType (String saleType) {
        this.saleType = saleType;
    }

    public String getSaleType () {
        return saleType;
    }

    private String sinkingFund;

    public void setSinkingFund (String sinkingFund) {
        this.sinkingFund = sinkingFund;
    }

    public String getSinkingFund () {
        return sinkingFund;
    }

    private String sourceDocument;

    public void setSourceDocument (String sourceDocument) {
        this.sourceDocument = sourceDocument;
    }

    public String getSourceDocument () {
        return sourceDocument;
    }

    private String sponsoring;

    public void setSponsoring (String sponsoring) {
        this.sponsoring = sponsoring;
    }

    public String getSponsoring () {
        return sponsoring;
    }

    private String taxable;

    public void setTaxable (String taxable) {
        this.taxable = taxable;
    }

    public String getTaxable () {
        return taxable;
    }

    private String tenderAgent;

    public void setTenderAgent (String tenderAgent) {
        this.tenderAgent = tenderAgent;
    }

    public String getTenderAgent () {
        return tenderAgent;
    }

    private String tickerSymbol;

    public void setTickerSymbol (String tickerSymbol) {
        this.tickerSymbol = tickerSymbol;
    }

    public String getTickerSymbol () {
        return tickerSymbol;
    }

    private String transferAgent;

    public void setTransferAgent (String transferAgent) {
        this.transferAgent = transferAgent;
    }

    public String getTransferAgent () {
        return transferAgent;
    }

    private String underwriter;

    public void setUnderwriter (String underwriter) {
        this.underwriter = underwriter;
    }

    public String getUnderwriter () {
        return underwriter;
    }

    private String usCFICode;

    public void setUsCFICode (String usCFICode) {
        this.usCFICode = usCFICode;
    }

    public String getUsCFICode () {
        return usCFICode;
    }

    private String votingRights;

    public void setVotingRights (String votingRights) {
        this.votingRights = votingRights;
    }

    public String getVotingRights () {
        return votingRights;
    }

    private String warrantAssets;

    public void setWarrantAssets (String warrantAssets) {
        this.warrantAssets = warrantAssets;
    }

    public String getWarrantAssets () {
        return warrantAssets;
    }

    private String warrantStatus;

    public void setWarrantStatus (String warrantStatus) {
        this.warrantStatus = warrantStatus;
    }

    public String getWarrantStatus () {
        return warrantStatus;
    }

    private String warrantType;

    public void setWarrantType (String warrantType) {
        this.warrantType = warrantType;
    }

    public String getWarrantType () {
        return warrantType;
    }

    private String whereTraded;

    public void setWhereTraded (String whereTraded) {
        this.whereTraded = whereTraded;
    }

    public String getWhereTraded () {
        return whereTraded;
    }

    @Override
    public String toString() {
        return "IssueAttribute [alternativeMinimumTax=" + alternativeMinimumTax + ", auditor=" + auditor
            + ", bankQualified=" + bankQualified + ", bondCounsel=" + bondCounsel + ", bondForm=" + bondForm
            + ", callable=" + callable + ", certified=" + certified + ", closingDate=" + closingDate
            + ", cusip=" + cusip + ", dateCertified=" + dateCertified + ", depositoryEligible="
            + depositoryEligible + ", enhancements=" + enhancements + ", etfType=" + etfType
            + ", financialAdvisor=" + financialAdvisor + ", firstCouponDate=" + firstCouponDate
            + ", fundDistributionPolicy=" + fundDistributionPolicy + ", fundInvestmentPolicy="
            + fundInvestmentPolicy + ", fundType=" + fundType + ", governmentStimulusProgram="
            + governmentStimulusProgram + ", guarantee=" + guarantee + ", incomeType=" + incomeType
            + ", initialPublicOffering=" + initialPublicOffering + ", insuredBy=" + insuredBy + ", isoCFICode="
            + isoCFICode + ", municipalSaleDate=" + municipalSaleDate + ", ote=" + ote + ", offeringAmount="
            + offeringAmount + ", offeringAmountCode=" + offeringAmountCode + ", ownershipRestrictions="
            + ownershipRestrictions + ", payingAgent=" + payingAgent + ", paymentFrequencyCode="
            + paymentFrequencyCode + ", paymentStatus=" + paymentStatus + ", portal=" + portal
            + ", preRefunded=" + preRefunded + ", preferredType=" + preferredType + ", putable=" + putable
            + ", rateType=" + rateType + ", redemption=" + redemption + ", refundable=" + refundable
            + ", remarketed=" + remarketed + ", saleType=" + saleType + ", sinkingFund=" + sinkingFund
            + ", sourceDocument=" + sourceDocument + ", sponsoring=" + sponsoring + ", taxable=" + taxable
            + ", tenderAgent=" + tenderAgent + ", tickerSymbol=" + tickerSymbol + ", transferAgent="
            + transferAgent + ", underwriter=" + underwriter + ", usCFICode=" + usCFICode + ", votingRights="
            + votingRights + ", warrantAssets=" + warrantAssets + ", warrantStatus=" + warrantStatus
            + ", warrantType=" + warrantType + ", whereTraded=" + whereTraded + "]";
    }
}