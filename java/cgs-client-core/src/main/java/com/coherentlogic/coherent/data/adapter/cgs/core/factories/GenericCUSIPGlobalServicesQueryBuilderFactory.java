package com.coherentlogic.coherent.data.adapter.cgs.core.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.coherentlogic.coherent.data.adapter.cgs.core.builders.GenericCUSIPGlobalServicesQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AuthenticationHolder;
import com.coherentlogic.coherent.data.adapter.cgs.core.exceptions.InvalidConfigurationException;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class GenericCUSIPGlobalServicesQueryBuilderFactory {

    @Autowired
    private WebServiceTemplate idHubWebServiceTemplate;

    @Autowired
    private AuthenticationHolder authenticationHolder;

    public <K, V> GenericCUSIPGlobalServicesQueryBuilder<K, V> getInstance() {

        /* Note that the userId and binarySecurityToken must be non-null when this method is called otherwise an
         * exception will be thrown so any builder that uses an instance of GenericCUSIPGlobalServicesQueryBuilder
         * *must be* a prototype scoped bean -- if it is a singleton, then it will be created once and at that time the
         * userId and binarySecurityToken will not be available since the user has not yet authenticated with CGS.
         */

        String userId = authenticationHolder.getUserId();

        String binarySecurityToken = authenticationHolder.getBinarySecurityToken();

        if (userId == null || binarySecurityToken == null)
            throw new InvalidConfigurationException ("Either the userId is null: " + (userId == null) + " or the " +
                "binarySecurityToken is null: " + (binarySecurityToken == null) + " and these must be non-null " +
                "values -- check that these have been set in the authenticationHolder -- refer also to the comments "
                + "in this class.");

        GenericCUSIPGlobalServicesQueryBuilder<K, V> genericCGSQueryBuilder =
            new GenericCUSIPGlobalServicesQueryBuilder<K, V> (
                idHubWebServiceTemplate,
                userId,
                binarySecurityToken
            );

        return genericCGSQueryBuilder;
    }
}
