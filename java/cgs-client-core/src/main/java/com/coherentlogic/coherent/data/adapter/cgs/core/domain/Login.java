package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Login extends IdentityBean {

    private static final long serialVersionUID = 8173951931548334146L;

    private String binarySecurityToken = null;

    public Login() {
    }

    public String getBinarySecurityToken() {
        return binarySecurityToken;
    }

    public void setBinarySecurityToken(String binarySecurityToken) {
        this.binarySecurityToken = binarySecurityToken;
    }

    @Override
    public String toString() {
        return "Login [binarySecurityToken=" + binarySecurityToken + "]";
    }
}
