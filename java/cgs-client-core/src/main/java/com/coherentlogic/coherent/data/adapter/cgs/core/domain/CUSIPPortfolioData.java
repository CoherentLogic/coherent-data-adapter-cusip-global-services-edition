package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CUSIPPortfolioData extends CUSIPData {

    public CUSIPPortfolioData() {
        super();
    }

    public CUSIPPortfolioData(List<IssuerDetail> issuerDetails) {
        super(issuerDetails);
    }
}
