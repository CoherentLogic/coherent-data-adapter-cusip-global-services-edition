package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttributes;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetIssueAttributes;
import com.coherentlogic.cusip.global.services.model.GetIssueAttributesResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class IssueAttributesBuilder
    extends AbstractQueryBuilder<IssueAttributes, GetIssueAttributes, GetIssueAttributesResponse> {

    private static final Logger log = LoggerFactory.getLogger(IssueAttributesBuilder.class);

    @Autowired
    public IssueAttributesBuilder(
        @Qualifier(CacheConfiguration.ISSUE_ATTRIBUTES_CACHE)
            CacheServiceProviderSpecification<String, IssueAttributes> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetIssueAttributes, GetIssueAttributesResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetIssueAttributesResponse, IssueAttributes> adapter,
        GetIssueAttributes request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public IssueAttributesBuilder withCUSIP (String cusip) {

        log.info ("method begins; cusip: " + cusip);

        GetIssueAttributes request = getRequest();

        request.getRequest().setCusip(cusip);

        return this;
    }

    @Override
    public IssueAttributes newInstance() {
        return new IssueAttributes();
    }
}
