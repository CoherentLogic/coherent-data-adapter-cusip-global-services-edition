package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.OptionDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.OptionsData;
import com.coherentlogic.cusip.global.services.model.ErrorList;
import com.coherentlogic.cusip.global.services.model.GetOptionsDataResponse;
import com.coherentlogic.cusip.global.services.model.OptionsResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class OptionsDataAdapter extends AbstractAdapter<GetOptionsDataResponse, OptionsData> {

    private static final Logger log = LoggerFactory.getLogger(OptionsDataAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(final GetOptionsDataResponse source, final OptionsData target) {

        final OptionsResponse optionsResponse = source.getReturn();

        ErrorList errorList = optionsResponse.getErrorList();

        errorBeanAdapter.adapt(errorList, target);

        optionsResponse
            .getOptionsList()
            .getOptionsDetail()
            .forEach(
                entry -> {

                    OptionDetail optionDetail = new OptionDetail ();

                    log.info("Adding optionDetail: " + optionDetail);

                    target.getOptionDetails().add(optionDetail);

                    String actionIndDesc = entry.getActionIndDesc ();

                    log.debug ("actionIndDesc: " +  actionIndDesc);

                    optionDetail.setActionIndDesc(actionIndDesc);

                    String actionIndicator = entry.getActionIndicator ();

                    log.debug ("actionIndicator: " +  actionIndicator);

                    optionDetail.setActionIndicator(actionIndicator);

                    String addedD = entry.getAddedD ();

                    log.debug ("addedD: " +  addedD);

                    optionDetail.setAddedD(addedD);

                    String assetClass = entry.getAssetClass ();

                    log.debug ("assetClass: " +  assetClass);

                    optionDetail.setAssetClass(assetClass);

                    String baseCurrency = entry.getBaseCurrency ();

                    log.debug ("baseCurrency: " +  baseCurrency);

                    optionDetail.setBaseCurrency(baseCurrency);

                    String cc9 = entry.getCC9 ();

                    log.debug ("cc9: " +  cc9);

                    optionDetail.setCC9(cc9);

                    String contractDelistDate = entry.getContractDelistDate ();

                    log.debug ("contractDelistDate: " +  contractDelistDate);

                    optionDetail.setContractDelistDate(contractDelistDate);

                    String contractLevelFoIndicator = entry.getContractLevelFoIndicator ();

                    log.debug ("contractLevelFoIndicator: " +  contractLevelFoIndicator);

                    optionDetail.setContractLevelFoIndicator(contractLevelFoIndicator);

                    String contractName = entry.getContractName ();

                    log.debug ("contractName: " +  contractName);

                    optionDetail.setContractName(contractName);

                    String contractNo = entry.getContractNo ();

                    log.debug ("contractNo: " +  contractNo);

                    optionDetail.setContractNo(contractNo);

                    String contractSize = entry.getContractSize ();

                    log.debug ("contractSize: " +  contractSize);

                    optionDetail.setContractSize(contractSize);

                    String contractStartDate = entry.getContractStartDate ();

                    log.debug ("contractStartDate: " +  contractStartDate);

                    optionDetail.setContractStartDate(contractStartDate);

                    String csbIsin = entry.getCsbIsin ();

                    log.debug ("csbIsin: " +  csbIsin);

                    optionDetail.setCsbIsin(csbIsin);

                    String dateType = entry.getDateType ();

                    log.debug ("dateType: " +  dateType);

                    optionDetail.setDateType(dateType);

                    String exchangeCode = entry.getExchangeCode ();

                    log.debug ("exchangeCode: " +  exchangeCode);

                    optionDetail.setExchangeCode(exchangeCode);

                    String exchangeSymbol = entry.getExchangeSymbol ();

                    log.debug ("exchangeSymbol: " +  exchangeSymbol);

                    optionDetail.setExchangeSymbol(exchangeSymbol);

                    String expirationDate = entry.getExpirationDate ();

                    log.debug ("expirationDate: " +  expirationDate);

                    optionDetail.setExpirationDate(expirationDate);

                    String foIndicator = entry.getFoIndicator ();

                    log.debug ("foIndicator: " +  foIndicator);

                    optionDetail.setFoIndicator(foIndicator);

                    String instrumentType = entry.getInstrumentType ();

                    log.debug ("instrumentType: " +  instrumentType);

                    optionDetail.setInstrumentType(instrumentType);

                    String isoCfiCode = entry.getIsoCfiCode ();

                    log.debug ("isoCfiCode: " +  isoCfiCode);

                    optionDetail.setIsoCfiCode(isoCfiCode);

                    String isoMicCode = entry.getIsoMicCode ();

                    log.debug ("isoMicCode: " +  isoMicCode);

                    optionDetail.setIsoMicCode(isoMicCode);

                    String occCode = entry.getOccCode ();

                    log.debug ("occCode: " +  occCode);

                    optionDetail.setOccCode(occCode);

                    String opraOsiCode = entry.getOpraOsiCode ();

                    log.debug ("opraOsiCode: " +  opraOsiCode);

                    optionDetail.setOpraOsiCode(opraOsiCode);

                    String opraSymbol = entry.getOpraSymbol ();

                    log.debug ("opraSymbol: " +  opraSymbol);

                    optionDetail.setOpraSymbol(opraSymbol);

                    String optionPremiumPayment = entry.getOptionPremiumPayment ();

                    log.debug ("optionPremiumPayment: " +  optionPremiumPayment);

                    optionDetail.setOptionPremiumPayment(optionPremiumPayment);

                    String optionSettlementType = entry.getOptionSettlementType ();

                    log.debug ("optionSettlementType: " +  optionSettlementType);

                    optionDetail.setOptionSettlementType(optionSettlementType);

                    String productType = entry.getProductType ();

                    log.debug ("productType: " +  productType);

                    optionDetail.setProductType(productType);

                    String promptDate = entry.getPromptDate ();

                    log.debug ("promptDate: " +  promptDate);

                    optionDetail.setPromptDate(promptDate);

                    String promptMonthCode = entry.getPromptMonthCode ();

                    log.debug ("promptMonthCode: " +  promptMonthCode);

                    optionDetail.setPromptMonthCode(promptMonthCode);

                    String quoteFraction = entry.getQuoteFraction ();

                    log.debug ("quoteFraction: " +  quoteFraction);

                    optionDetail.setQuoteFraction(quoteFraction);

                    String settlementCurrency = entry.getSettlementCurrency ();

                    log.debug ("settlementCurrency: " +  settlementCurrency);

                    optionDetail.setSettlementCurrency(settlementCurrency);

                    String settlementType = entry.getSettlementType ();

                    log.debug ("settlementType: " +  settlementType);

                    optionDetail.setSettlementType(settlementType);

                    String status = entry.getStatus ();

                    log.debug ("status: " +  status);

                    optionDetail.setStatus(status);

                    String strikePrice = entry.getStrikePrice ();

                    log.debug ("strikePrice: " +  strikePrice);

                    optionDetail.setStrikePrice(strikePrice);

                    String tickSize = entry.getTickSize ();

                    log.debug ("tickSize: " +  tickSize);

                    optionDetail.setTickSize(tickSize);

                    String tickSizeDenominator = entry.getTickSizeDenominator ();

                    log.debug ("tickSizeDenominator: " +  tickSizeDenominator);

                    optionDetail.setTickSizeDenominator(tickSizeDenominator);

                    String tickSizeNumerator = entry.getTickSizeNumerator ();

                    log.debug ("tickSizeNumerator: " +  tickSizeNumerator);

                    optionDetail.setTickSizeNumerator(tickSizeNumerator);

                    String tickValue = entry.getTickValue ();

                    log.debug ("tickValue: " +  tickValue);

                    optionDetail.setTickValue(tickValue);

                    String underlyingCusip = entry.getUnderlyingCusip ();

                    log.debug ("underlyingCusip: " +  underlyingCusip);

                    optionDetail.setUnderlyingCusip(underlyingCusip);

                    String underlyingIsin = entry.getUnderlyingIsin ();

                    log.debug ("underlyingIsin: " +  underlyingIsin);

                    optionDetail.setUnderlyingIsin(underlyingIsin);

                    String underlyingSymbol = entry.getUnderlyingSymbol ();

                    log.debug ("underlyingSymbol: " +  underlyingSymbol);

                    optionDetail.setUnderlyingSymbol(underlyingSymbol);

                    String updatedD = entry.getUpdatedD ();

                    log.debug ("updatedD: " +  updatedD);

                    optionDetail.setUpdatedD(updatedD);

                    String usCfiCode = entry.getUsCfiCode ();

                    log.debug ("usCfiCode: " +  usCfiCode);

                    optionDetail.setUsCfiCode(usCfiCode);
                }
            );
    }
}
