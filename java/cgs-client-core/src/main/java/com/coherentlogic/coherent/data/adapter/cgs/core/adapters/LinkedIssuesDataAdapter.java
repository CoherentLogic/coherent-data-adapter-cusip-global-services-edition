package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.LinkedIssueDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.LinkedIssuesData;
import com.coherentlogic.cusip.global.services.model.ErrorList;
import com.coherentlogic.cusip.global.services.model.GetLinkedIssuesDataResponse;

import com.coherentlogic.cusip.global.services.model.LinkedIssuesDetailList;
import com.coherentlogic.cusip.global.services.model.LinkedIssuesResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
@Component
public class LinkedIssuesDataAdapter extends AbstractAdapter<GetLinkedIssuesDataResponse, LinkedIssuesData> {

    private static final Logger log = LoggerFactory.getLogger(LinkedIssuesDataAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(final GetLinkedIssuesDataResponse source, final LinkedIssuesData target) {

        LinkedIssuesResponse response = source.getReturn();

        ErrorList errorList = response.getErrorList();

        errorBeanAdapter.adapt(errorList, target);

        LinkedIssuesDetailList linkedIssuesDetailList = response.getLinkedIssuesDetailList();

        final List<LinkedIssueDetail> linkedIssuesDetails = target.getLinkedIssueDetails();

        linkedIssuesDetailList
            .getLinkedIssuesDetail()
            .forEach(
                entry -> {

                    LinkedIssueDetail linkedIssueDetail = new LinkedIssueDetail ();

                    linkedIssuesDetails.add(linkedIssueDetail);

                    linkedIssueDetail.setLinkIssueAdditionalInfo(entry.getLinkIssueAdditionalInfo());
                    linkedIssueDetail.setLinkIssueCheck(entry.getLinkIssueCheck());
                    linkedIssueDetail.setLinkIssueDescription(entry.getLinkIssueDescription());
                    linkedIssueDetail.setLinkIssueNumber(entry.getLinkIssueNumber());
                    linkedIssueDetail.setLinkIssuerDescription(entry.getLinkIssuerDescription());
                    linkedIssueDetail.setLinkIssuerNumber(entry.getLinkIssuerNumber());
                }
            );
    }
}
