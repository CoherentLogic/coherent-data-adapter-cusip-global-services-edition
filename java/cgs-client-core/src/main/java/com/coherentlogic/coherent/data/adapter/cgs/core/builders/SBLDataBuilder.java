package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.SBLData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.GetSblData;
import com.coherentlogic.cusip.global.services.model.GetSblDataResponse;
import com.coherentlogic.cusip.global.services.model.SblRequest;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
@Scope(scopeName=ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SBLDataBuilder extends AbstractQueryBuilder<SBLData, GetSblData, GetSblDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(SBLDataBuilder.class);

    @Autowired
    public SBLDataBuilder(
        @Qualifier(CacheConfiguration.SBL_DATA_CACHE)
            CacheServiceProviderSpecification<String, SBLData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetSblData, GetSblDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetSblDataResponse, SBLData> adapter,
        GetSblData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public SBLDataBuilder withBorrowerIsoCountryCode (String borrowerIsoCountryCode) {

        log.info ("method begins; borrowerIsoCountryCode: " + borrowerIsoCountryCode);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setBorrowerIsoCountryCode(borrowerIsoCountryCode);

        return this;
    }

    public SBLDataBuilder withBorrowerName (String borrowerName) {

        log.info ("method begins; borrowerName: " + borrowerName);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setBorrowerName(borrowerName);

        return this;
    }

    public SBLDataBuilder withBorrowerNameStartsWith (String borrowerNameStartsWith) {

        log.info ("method begins; borrowerNameStartsWith: " + borrowerNameStartsWith);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setBorrowerNameStartsWith(borrowerNameStartsWith);

        return this;
    }

    public SBLDataBuilder withBorrowerStateCode (String borrowerStateCode) {

        log.info ("method begins; borrowerStateCode: " + borrowerStateCode);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setBorrowerStateCode(borrowerStateCode);

        return this;
    }

    public SBLDataBuilder withCUSIP (String cusip) {

        log.info ("method begins; cusip: " + cusip);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setCusip(cusip);

        return this;
    }

    public SBLDataBuilder withDealIsoCurrencyCode (String dealIsoCurrencyCode) {

        log.info ("method begins; dealIsoCurrencyCode: " + dealIsoCurrencyCode);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setDealIsoCurrencyCode(dealIsoCurrencyCode);

        return this;
    }

    public SBLDataBuilder withDealLinkage (String dealLinkage) {

        log.info ("method begins; dealLinkage: " + dealLinkage);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setDealLinkage(dealLinkage);

        return this;
    }

    public SBLDataBuilder withDealSort (String dealSort) {

        log.info ("method begins; dealSort: " + dealSort);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.getDealSort ().add(dealSort);

        return this;
    }

    public SBLDataBuilder withFacilityIsoCurrencyCode (String facilityIsoCurrencyCode) {

        log.info ("method begins; facilityIsoCurrencyCode: " + facilityIsoCurrencyCode);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setFacilityIsoCurrencyCode(facilityIsoCurrencyCode);

        return this;
    }

    public SBLDataBuilder withFacilitySort (String facilitySort) {

        log.info ("method begins; facilitySort: " + facilitySort);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.getFacilitySort ().add(facilitySort);

        return this;
    }

    public SBLDataBuilder withIssueISINNumber (String issueISINNumber) {

        log.info ("method begins; issueISINNumber: " + issueISINNumber);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setIssueIsinNumber(issueISINNumber);

        return this;
    }

    public SBLDataBuilder withIssueMaturityDateFrom (String issueMaturityDateFrom) {

        log.info ("method begins; issueMaturityDateFrom: " + issueMaturityDateFrom);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setIssueMaturityDateFrom(issueMaturityDateFrom);

        return this;
    }

    public SBLDataBuilder withIssueMaturityDateTo (String issueMaturityDateTo) {

        log.info ("method begins; issueMaturityDateTo: " + issueMaturityDateTo);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setIssueMaturityDateTo(issueMaturityDateTo);

        return this;
    }

    public SBLDataBuilder withMaxResults (int maxResults) {

        log.info ("method begins; maxResults: " + maxResults);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setMaxResults(maxResults);

        return this;
    }

    public SBLDataBuilder withRequestorType (String requestorType) {

        log.info ("method begins; requestorType: " + requestorType);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setRequestorType(requestorType);

        return this;
    }

    public SBLDataBuilder withReturnDealsOnlyFlag (String returnDealsOnlyFlag) {

        log.info ("method begins; returnDealsOnlyFlag: " + returnDealsOnlyFlag);

        GetSblData getSblData = getRequest();

        SblRequest request = getSblData.getRequest();

        request.setReturnDealsOnlyFlag(returnDealsOnlyFlag);

        return this;
    }

    @Override
    public SBLData newInstance() {
        return new SBLData ();
    }
}
