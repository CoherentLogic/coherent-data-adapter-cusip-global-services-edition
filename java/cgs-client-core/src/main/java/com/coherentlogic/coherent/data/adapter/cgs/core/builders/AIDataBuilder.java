package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import com.coherentlogic.coherent.data.adapter.cgs.core.configuration.CacheConfiguration;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AIData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.AIRequest;
import com.coherentlogic.cusip.global.services.model.GetAIData;
import com.coherentlogic.cusip.global.services.model.GetAIDataResponse;

/**
 * @deprecated NOT CURRENTLY AVAILABLE AT CGS!
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AIDataBuilder extends AbstractQueryBuilder<AIData, GetAIData, GetAIDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(AIDataBuilder.class);

    public AIDataBuilder(
        @Qualifier(CacheConfiguration.AI_DATA_CACHE) CacheServiceProviderSpecification<String, AIData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetAIData, GetAIDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetAIDataResponse, AIData> adapter,
        GetAIData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public AIDataBuilder withIssuerNumber (String issuerNumber) {

        log.info ("method begins; issuerNumber: " + issuerNumber);

        GetAIData request = getRequest();

        AIRequest aiRequest = request.getRequest();

        aiRequest.getIssuerNumber().add(issuerNumber);

        return this;
    }

    public AIDataBuilder withIssuerNumberOnly (String issuerNumberOnly) {

        log.info ("method begins; issuerNumberOnly: " + issuerNumberOnly);

        GetAIData request = getRequest();

        AIRequest aiRequest = request.getRequest();

        aiRequest.setIssuerNumberOnly(issuerNumberOnly);

        return this;
    }

    @Override
    public AIData newInstance() {
        return new AIData();
    }

    @Override
    public AIData doGet() {
        throw new RuntimeException (METHOD_NOT_YET_SUPPORTED);
    }
}
