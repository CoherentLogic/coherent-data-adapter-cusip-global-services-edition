package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

public class StateDetail extends IdentityBean {

    private String code;

    private String description;

    public StateDetail () {
    }

    public StateDetail(String code, String description) {
        super();
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "StateDetail [stateCode=" + code + ", stateDescr=" + description + "]";
    }
}