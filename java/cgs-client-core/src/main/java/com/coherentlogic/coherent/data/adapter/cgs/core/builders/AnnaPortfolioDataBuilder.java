package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AnnaData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AnnaPortfolioData;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.cusip.global.services.model.AnnaPortfolioRequest;
import com.coherentlogic.cusip.global.services.model.GetAnnaDataResponse;
import com.coherentlogic.cusip.global.services.model.GetAnnaPortfolioData;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class AnnaPortfolioDataBuilder
    extends AbstractQueryBuilder<AnnaPortfolioData, GetAnnaPortfolioData, GetAnnaDataResponse> {

    private static final Logger log = LoggerFactory.getLogger(AnnaPortfolioDataBuilder.class);

    public AnnaPortfolioDataBuilder(
        CacheServiceProviderSpecification<String, AnnaPortfolioData> cacheServiceProvider,
        GenericCUSIPGlobalServicesQueryBuilder<GetAnnaPortfolioData, GetAnnaDataResponse> genericCGSQueryBuilder,
        InOutAdapterSpecification<GetAnnaDataResponse, AnnaPortfolioData> adapter,
        GetAnnaPortfolioData request
    ) {
        super(cacheServiceProvider, genericCGSQueryBuilder, adapter, request);
    }

    public AnnaPortfolioDataBuilder withISIN(String isin) {

        log.info("method begins; isin: " + isin);

        GetAnnaPortfolioData request = getRequest();

        AnnaPortfolioRequest mAnnaPortfolioRequest = request.getRequest();

        mAnnaPortfolioRequest.getIsin ().add(isin);

        return this;
    }

    @Override
    public AnnaPortfolioData newInstance() {
        return new AnnaPortfolioData ();
    }

    @Override
    public AnnaPortfolioData doGet() {
        throw new RuntimeException(METHOD_NOT_YET_SUPPORTED);
    }
}
