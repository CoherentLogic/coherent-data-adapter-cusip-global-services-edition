package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class GovernmentMortgages extends ErrorBean {

    private static final long serialVersionUID = -2370418240056548865L;

    private List<GovernmentMortgageDetail> governmentMortgageDetails;

    public GovernmentMortgages() {
    	this (new ArrayList<GovernmentMortgageDetail> ());
    }

    public GovernmentMortgages(List<GovernmentMortgageDetail> governmentMortgageDetails) {
        this.governmentMortgageDetails = governmentMortgageDetails;
    }

    public List<GovernmentMortgageDetail> getGovernmentMortgageDetails() {
        return governmentMortgageDetails;
    }

    public void setGovernmentMortgageDetails(List<GovernmentMortgageDetail> governmentMortgageDetails) {
        this.governmentMortgageDetails = governmentMortgageDetails;
    }
}
