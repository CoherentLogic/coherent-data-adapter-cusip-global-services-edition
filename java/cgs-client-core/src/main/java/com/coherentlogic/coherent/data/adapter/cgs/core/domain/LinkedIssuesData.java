package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import java.util.ArrayList;
import java.util.List;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.LinkedIssueDetail;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class LinkedIssuesData extends ErrorBean {

    private static final long serialVersionUID = -6325817542157903271L;

    private List<LinkedIssueDetail> linkedIssueDetails = null;

    public LinkedIssuesData() {
        this (new ArrayList<LinkedIssueDetail> ());
    }

    public LinkedIssuesData(List<LinkedIssueDetail> linkedIssueDetails) {
        this.linkedIssueDetails = linkedIssueDetails;
    }

    public List<LinkedIssueDetail> getLinkedIssueDetails() {
        return linkedIssueDetails;
    }

    public void setLinkedIssueDetails(List<LinkedIssueDetail> linkedIssueDetails) {
        this.linkedIssueDetails = linkedIssueDetails;
    }
}
