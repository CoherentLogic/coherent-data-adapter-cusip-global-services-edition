package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domicile;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domiciles;
import com.coherentlogic.coherent.data.model.core.adapters.InOutAdapterSpecification;
import com.coherentlogic.cusip.global.services.model.DomicileList;
import com.coherentlogic.cusip.global.services.model.DomicilesResponse;
import com.coherentlogic.cusip.global.services.model.GetDomicilesResponse;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Component
public class DomicilesAdapter implements InOutAdapterSpecification<GetDomicilesResponse, Domiciles> {

    private static final Logger log = LoggerFactory.getLogger(DomicilesAdapter.class);

    static Domicile adapt (com.coherentlogic.cusip.global.services.model.DomicileDetail source) {

        String code = source.getDomicileCode();
        String description = source.getDomicileDescr();
        String isoCode = source.getDomicileIso();

        Domicile target = new Domicile ();

        target.setCode(code);
        target.setDescription(description);
        target.setIsoCode(isoCode);

        return target;
    }

    @Override
    public void adapt(GetDomicilesResponse getDomicilesResponse, Domiciles domiciles) {

        DomicilesResponse domicilesResponse = getDomicilesResponse.getReturn();

        DomicileList modelDomicileList = domicilesResponse.getDomicileList();

        List<com.coherentlogic.cusip.global.services.model.DomicileDetail> modelDomicileDetails =
            modelDomicileList.getDomicileDetail();

        modelDomicileDetails.forEach(
            nextDomicileDetail -> {

                Domicile domicileDetail = adapt (nextDomicileDetail);

                List<Domicile> domicileList = domiciles.getDomiciles();

                domicileList.add(domicileDetail);
            }
        );
    }
}
