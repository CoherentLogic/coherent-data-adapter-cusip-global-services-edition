package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CUSIPTickerIssueDetail extends IdentityBean {

    private Integer issueCheck;

    private String description;

    private Integer number;

    private String status;

    /**
     * @deprecated Not available in the domain CUSIPTickerIssueDetails domain specification!
     */
    public Integer getIssueCheck() {
        return issueCheck;
    }

    /**
     * @deprecated Not available in the domain CUSIPTickerIssueDetails domain specification!
     */
    public void setIssueCheck(Integer issueCheck) {
        this.issueCheck = issueCheck;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "CUSIPTickerIssueDetail [issueCheck=" + issueCheck + ", description=" + description + ", number="
                + number + ", status=" + status + "]";
    }
}