package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class FacilityDetail extends IdentityBean {

    private static final long serialVersionUID = -5006911801090178392L;

    private String amount;

    public void setAmount (String amount) {
        this.amount = amount;
    }

    public String getAmount () {
        return amount;
    }

    private String facilityEstimatedFlag;

    public void setFacilityEstimatedFlag (String facilityEstimatedFlag) {
        this.facilityEstimatedFlag = facilityEstimatedFlag;
    }

    public String getFacilityEstimatedFlag () {
        return facilityEstimatedFlag;
    }

    private String facilityPurpose;

    public void setFacilityPurpose (String facilityPurpose) {
        this.facilityPurpose = facilityPurpose;
    }

    public String getFacilityPurpose () {
        return facilityPurpose;
    }

    private String facilityType;

    public void setFacilityType (String facilityType) {
        this.facilityType = facilityType;
    }

    public String getFacilityType () {
        return facilityType;
    }

    private String isoCurrencyCode;

    public void setIsoCurrencyCode (String isoCurrencyCode) {
        this.isoCurrencyCode = isoCurrencyCode;
    }

    public String getIsoCurrencyCode () {
        return isoCurrencyCode;
    }

    private String issueCheck;

    public void setIssueCheck (String issueCheck) {
        this.issueCheck = issueCheck;
    }

    public String getIssueCheck () {
        return issueCheck;
    }

    private String issueIsinNumber;

    public void setIssueIsinNumber (String issueIsinNumber) {
        this.issueIsinNumber = issueIsinNumber;
    }

    public String getIssueIsinNumber () {
        return issueIsinNumber;
    }

    private String issueMaturityDate;

    public void setIssueMaturityDate (String issueMaturityDate) {
        this.issueMaturityDate = issueMaturityDate;
    }

    public String getIssueMaturityDate () {
        return issueMaturityDate;
    }

    private String issueNumber;

    public void setIssueNumber (String issueNumber) {
        this.issueNumber = issueNumber;
    }

    public String getIssueNumber () {
        return issueNumber;
    }

    private String issueStatus;

    public void setIssueStatus (String issueStatus) {
        this.issueStatus = issueStatus;
    }

    public String getIssueStatus () {
        return issueStatus;
    }

    private String releaseDate;

    public void setReleaseDate (String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getReleaseDate () {
        return releaseDate;
    }
}
