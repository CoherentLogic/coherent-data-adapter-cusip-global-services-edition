package com.coherentlogic.coherent.data.adapter.cgs.core.adapters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttributes;
import com.coherentlogic.cusip.global.services.model.GetIssueAttributesResponse;

@Component
public class IssueAttributesAdapter extends AbstractAdapter<GetIssueAttributesResponse, IssueAttributes> {

    private static final Logger log = LoggerFactory.getLogger(IssueAttributesAdapter.class);

    @Autowired
    private ErrorBeanAdapter errorBeanAdapter;

    @Override
    public void adapt(
        GetIssueAttributesResponse source,
        IssueAttributes target
    ) {

        log.info("adapt: method begins; source: " + source + ", target: " + target);

        errorBeanAdapter.adapt(source.getReturn().getErrorList(), target);

        adapt (source.getReturn().getIssueAttributes(), target);

        log.info("adapt: method ends.");
    }

    void adapt (
        com.coherentlogic.cusip.global.services.model.IssueAttributes mIssueAttributes,
        com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttributes dIssueAttributes
    ) {
        String alternativeMinimumTax = mIssueAttributes.getAlternativeMinimumTax ();

        log.debug ("alternativeMinimumTax: " +  alternativeMinimumTax);

        dIssueAttributes.setAlternativeMinimumTax(alternativeMinimumTax);

        String auditor = mIssueAttributes.getAuditor ();

        log.debug ("auditor: " +  auditor);

        dIssueAttributes.setAuditor(auditor);

        String bankQualified = mIssueAttributes.getBankQualified ();

        log.debug ("bankQualified: " +  bankQualified);

        dIssueAttributes.setBankQualified(bankQualified);

        String bondCounsel = mIssueAttributes.getBondCounsel ();

        log.debug ("bondCounsel: " +  bondCounsel);

        dIssueAttributes.setBondCounsel(bondCounsel);

        String bondForm = mIssueAttributes.getBondForm ();

        log.debug ("bondForm: " +  bondForm);

        dIssueAttributes.setBondForm(bondForm);

        String callable = mIssueAttributes.getCallable ();

        log.debug ("callable: " +  callable);

        dIssueAttributes.setCallable(callable);

        String certified = mIssueAttributes.getCertified ();

        log.debug ("certified: " +  certified);

        dIssueAttributes.setCertified(certified);

        String closingDate = mIssueAttributes.getClosingDate ();

        log.debug ("closingDate: " +  closingDate);

        dIssueAttributes.setClosingDate(closingDate);

        String cusip = mIssueAttributes.getCusip ();

        log.debug ("cusip: " +  cusip);

        dIssueAttributes.setCUSIP(cusip);

        String dateCertified = mIssueAttributes.getDateCertified ();

        log.debug ("dateCertified: " +  dateCertified);

        dIssueAttributes.setDateCertified(dateCertified);

        String depositoryEligible = mIssueAttributes.getDepositoryEligible ();

        log.debug ("depositoryEligible: " +  depositoryEligible);

        dIssueAttributes.setDepositoryEligible(depositoryEligible);

        String enhancements = mIssueAttributes.getEnhancements ();

        log.debug ("enhancements: " +  enhancements);

        dIssueAttributes.setEnhancements(enhancements);

        String etfType = mIssueAttributes.getEtfType ();

        log.debug ("etfType: " +  etfType);

        dIssueAttributes.setEtfType(etfType);

        String financialAdvisor = mIssueAttributes.getFinancialAdvisor ();

        log.debug ("financialAdvisor: " +  financialAdvisor);

        dIssueAttributes.setFinancialAdvisor(financialAdvisor);

        String firstCouponDate = mIssueAttributes.getFirstCouponDate ();

        log.debug ("firstCouponDate: " +  firstCouponDate);

        dIssueAttributes.setFirstCouponDate(firstCouponDate);

        String fundDistributionPolicy = mIssueAttributes.getFundDistributionPolicy ();

        log.debug ("fundDistributionPolicy: " +  fundDistributionPolicy);

        dIssueAttributes.setFundDistributionPolicy(fundDistributionPolicy);

        String fundInvestmentPolicy = mIssueAttributes.getFundInvestmentPolicy ();

        log.debug ("fundInvestmentPolicy: " +  fundInvestmentPolicy);

        dIssueAttributes.setFundInvestmentPolicy(fundInvestmentPolicy);

        String fundType = mIssueAttributes.getFundType ();

        log.debug ("fundType: " +  fundType);

        dIssueAttributes.setFundType(fundType);

        String governmentStimulusProgram = mIssueAttributes.getGovernmentStimulusProgram ();

        log.debug ("governmentStimulusProgram: " +  governmentStimulusProgram);

        dIssueAttributes.setGovernmentStimulusProgram(governmentStimulusProgram);

        String guarantee = mIssueAttributes.getGuarantee ();

        log.debug ("guarantee: " +  guarantee);

        dIssueAttributes.setGuarantee(guarantee);

        String incomeType = mIssueAttributes.getIncomeType ();

        log.debug ("incomeType: " +  incomeType);

        dIssueAttributes.setIncomeType(incomeType);

        String initialPublicOffering = mIssueAttributes.getInitialPublicOffering ();

        log.debug ("initialPublicOffering: " +  initialPublicOffering);

        dIssueAttributes.setInitialPublicOffering(initialPublicOffering);

        String insuredBy = mIssueAttributes.getInsuredBy ();

        log.debug ("insuredBy: " +  insuredBy);

        dIssueAttributes.setInsuredBy(insuredBy);

        String isoCFICode = mIssueAttributes.getIsoCFICode ();

        log.debug ("isoCFICode: " +  isoCFICode);

        dIssueAttributes.setIsoCFICode(isoCFICode);

        String municipalSaleDate = mIssueAttributes.getMunicipalSaleDate ();

        log.debug ("municipalSaleDate: " +  municipalSaleDate);

        dIssueAttributes.setMunicipalSaleDate(municipalSaleDate);

        String ote = mIssueAttributes.getOTE ();

        log.debug ("ote: " +  ote);

        dIssueAttributes.setOTE(ote);

        String offeringAmount = mIssueAttributes.getOfferingAmount ();

        log.debug ("offeringAmount: " +  offeringAmount);

        dIssueAttributes.setOfferingAmount(offeringAmount);

        String offeringAmountCode = mIssueAttributes.getOfferingAmountCode ();

        log.debug ("offeringAmountCode: " +  offeringAmountCode);

        dIssueAttributes.setOfferingAmountCode(offeringAmountCode);

        String ownershipRestrictions = mIssueAttributes.getOwnershipRestrictions ();

        log.debug ("ownershipRestrictions: " +  ownershipRestrictions);

        dIssueAttributes.setOwnershipRestrictions(ownershipRestrictions);

        String payingAgent = mIssueAttributes.getPayingAgent ();

        log.debug ("payingAgent: " +  payingAgent);

        dIssueAttributes.setPayingAgent(payingAgent);

        String paymentFrequencyCode = mIssueAttributes.getPaymentFrequencyCode ();

        log.debug ("paymentFrequencyCode: " +  paymentFrequencyCode);

        dIssueAttributes.setPaymentFrequencyCode(paymentFrequencyCode);

        String paymentStatus = mIssueAttributes.getPaymentStatus ();

        log.debug ("paymentStatus: " +  paymentStatus);

        dIssueAttributes.setPaymentStatus(paymentStatus);

        String portal = mIssueAttributes.getPortal ();

        log.debug ("portal: " +  portal);

        dIssueAttributes.setPortal(portal);

        String preRefunded = mIssueAttributes.getPreRefunded ();

        log.debug ("preRefunded: " +  preRefunded);

        dIssueAttributes.setPreRefunded(preRefunded);

        String preferredType = mIssueAttributes.getPreferredType ();

        log.debug ("preferredType: " +  preferredType);

        dIssueAttributes.setPreferredType(preferredType);

        String putable = mIssueAttributes.getPutable ();

        log.debug ("putable: " +  putable);

        dIssueAttributes.setPutable(putable);

        String rateType = mIssueAttributes.getRateType ();

        log.debug ("rateType: " +  rateType);

        dIssueAttributes.setRateType(rateType);

        String redemption = mIssueAttributes.getRedemption ();

        log.debug ("redemption: " +  redemption);

        dIssueAttributes.setRedemption(redemption);

        String refundable = mIssueAttributes.getRefundable ();

        log.debug ("refundable: " +  refundable);

        dIssueAttributes.setRefundable(refundable);

        String remarketed = mIssueAttributes.getRemarketed ();

        log.debug ("remarketed: " +  remarketed);

        dIssueAttributes.setRemarketed(remarketed);

        String saleType = mIssueAttributes.getSaleType ();

        log.debug ("saleType: " +  saleType);

        dIssueAttributes.setSaleType(saleType);

        String sinkingFund = mIssueAttributes.getSinkingFund ();

        log.debug ("sinkingFund: " +  sinkingFund);

        dIssueAttributes.setSinkingFund(sinkingFund);

        String sourceDocument = mIssueAttributes.getSourceDocument ();

        log.debug ("sourceDocument: " +  sourceDocument);

        dIssueAttributes.setSourceDocument(sourceDocument);

        String sponsoring = mIssueAttributes.getSponsoring ();

        log.debug ("sponsoring: " +  sponsoring);

        dIssueAttributes.setSponsoring(sponsoring);

        String taxable = mIssueAttributes.getTaxable ();

        log.debug ("taxable: " +  taxable);

        dIssueAttributes.setTaxable(taxable);

        String tenderAgent = mIssueAttributes.getTenderAgent ();

        log.debug ("tenderAgent: " +  tenderAgent);

        dIssueAttributes.setTenderAgent(tenderAgent);

        String tickerSymbol = mIssueAttributes.getTickerSymbol ();

        log.debug ("tickerSymbol: " +  tickerSymbol);

        dIssueAttributes.setTickerSymbol(tickerSymbol);

        String transferAgent = mIssueAttributes.getTransferAgent ();

        log.debug ("transferAgent: " +  transferAgent);

        dIssueAttributes.setTransferAgent(transferAgent);

        String underwriter = mIssueAttributes.getUnderwriter ();

        log.debug ("underwriter: " +  underwriter);

        dIssueAttributes.setUnderwriter(underwriter);

        String usCFICode = mIssueAttributes.getUsCFICode ();

        log.debug ("usCFICode: " +  usCFICode);

        dIssueAttributes.setUsCFICode(usCFICode);

        String votingRights = mIssueAttributes.getVotingRights ();

        log.debug ("votingRights: " +  votingRights);

        dIssueAttributes.setVotingRights(votingRights);

        String warrantAssets = mIssueAttributes.getWarrantAssets ();

        log.debug ("warrantAssets: " +  warrantAssets);

        dIssueAttributes.setWarrantAssets(warrantAssets);

        String warrantStatus = mIssueAttributes.getWarrantStatus ();

        log.debug ("warrantStatus: " +  warrantStatus);

        dIssueAttributes.setWarrantStatus(warrantStatus);

        String warrantType = mIssueAttributes.getWarrantType ();

        log.debug ("warrantType: " +  warrantType);

        dIssueAttributes.setWarrantType(warrantType);

        String whereTraded = mIssueAttributes.getWhereTraded ();

        log.debug ("whereTraded: " +  whereTraded);

        dIssueAttributes.setWhereTraded(whereTraded);
    }
}
