package com.coherentlogic.coherent.data.adapter.cgs.core.adapters.json;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Error.FaultType;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.States;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class StatesJSONAdapterTest {

    private StatesJSONAdapter statesJSONAdapter = null;

    private States states = null;

    @Before
    public void setUp() throws Exception {
        statesJSONAdapter = new StatesJSONAdapter ();
        states = new States ();
    }

    @After
    public void tearDown() throws Exception {
        statesJSONAdapter = null;
        states = null;
    }

    @Test
    public void testAdapt() {
        states.setErrors(new ArrayList<Error> (2));
        states.getErrors().add(new Error("actor 1", "code 1", FaultType.I, "Informational message for 1"));
        states.getErrors().add(new Error("actor 2", "code 2", FaultType.E, "Error message for 2"));

        states.getStateList().setStateCount(2L);

        states.getStateList().getStateDetails().add(new StateDetail("WV", "West Virginia"));
        states.getStateList().getStateDetails().add(new StateDetail("VA", "Virginia"));

        String result = statesJSONAdapter.adapt(states);

        System.out.println("result: " + result);
    }
}
