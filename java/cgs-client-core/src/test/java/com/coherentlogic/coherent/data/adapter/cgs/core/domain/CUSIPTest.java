package com.coherentlogic.coherent.data.adapter.cgs.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.coherentlogic.coherent.data.adapter.cgs.core.exceptions.InvalidCUSIPException;

public class CUSIPTest {

    private CUSIP cusip = null;

    @Before
    public void setUp() throws Exception {
        cusip = new CUSIP();
    }

    @After
    public void tearDown() throws Exception {
        cusip = null;
    }

    @Test
    public void testSetCusipIsValid() {
        cusip.setCUSIP("3128Q43H0");
    }

    @Test(expected=InvalidCUSIPException.class)
    public void testSetCusipIsInvalid() {
        cusip.setCUSIP("31---43H0");
    }

    @Test(expected=InvalidCUSIPException.class)
    public void testSetCusipIsTooLong() {
        cusip.setCUSIP("3128Q43H0X");
    }

    @Test(expected=InvalidCUSIPException.class)
    public void testSetCusipIsTooShort() {
        cusip.setCUSIP("3128Q43H");
    }

    @Test(expected=InvalidCUSIPException.class)
    public void testSetCusipIsNull() {
        cusip.setCUSIP("3128Q43H");
    }
}
