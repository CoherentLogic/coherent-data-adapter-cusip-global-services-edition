/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for the list of states.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * Methods available in the SBLDataBuilder are:
 *
 * withBorrowerName(java.lang.String)
 * withDealLinkage(java.lang.String)
 * withCusip(java.lang.String)
 * withDealSort(java.lang.String)
 * withFacilitySort(java.lang.String)
 * withMaxResults(int)
 * withBorrowerIsoCountryCode(java.lang.String)
 * withBorrowerNameStartsWith(java.lang.String)
 * withBorrowerStateCode(java.lang.String)
 * withDealIsoCurrencyCode(java.lang.String)
 * withFacilityIsoCurrencyCode(java.lang.String)
 * withIssueIsinNumber(java.lang.String)
 * withIssueMaturityDateFrom(java.lang.String)
 * withIssueMaturityDateTo(java.lang.String)
 * withRequestorType(java.lang.String)
 * withReturnDealsOnlyFlag(java.lang.String)
 */

return idHubQueryBuilder
  .sblData()
  .withBorrowerNameStartsWith("MASSACHUSETTS")
  .doGet()