/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for issue attribute data.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * Methods available in the IssueAttributesBuilder are:
 *
 * IssueAttributesBuilder IssueAttributesBuilder.withCUSIP(java.lang.String)
 */

def issueAttributes = idHubQueryBuilder
    .issueAttributes()
    .withCUSIP("023135106")
    .doGet();