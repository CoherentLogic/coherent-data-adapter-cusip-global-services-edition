/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for cusip data.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * Methods available in the CUSIPDataBuilder are:
 *
 * CUSIPDataBuilder withIssueRate(java.lang.String)
 * CUSIPDataBuilder withIssueRateTo(java.lang.String)
 * CUSIPDataBuilder withCUSIP(java.lang.String)
 * CUSIPDataBuilder withIssueSort(java.lang.String)
 * CUSIPDataBuilder withIssueStatus(java.lang.String)
 * CUSIPDataBuilder withIssuerDateTo(java.lang.String)
 * CUSIPDataBuilder withIssuerGroup(java.lang.String)
 * CUSIPDataBuilder withIssuerSort(java.lang.String)
 * CUSIPDataBuilder withIssuerStatus(java.lang.String)
 * CUSIPDataBuilder withIssueCurrencyCode(java.lang.String)
 * CUSIPDataBuilder withIssueDatedDateFrom(java.util.Date)
 * CUSIPDataBuilder withIssueDatedDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssueDatedDateTo(java.util.Date)
 * CUSIPDataBuilder withIssueDatedDateTo(java.lang.String)
 * CUSIPDataBuilder withIssueDescription(java.lang.String)
 * CUSIPDataBuilder withIssueISINNumber(java.lang.String)
 * CUSIPDataBuilder withIssueLogDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssueLogDateTo(java.lang.String)
 * CUSIPDataBuilder withIssueMaturityDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssueMaturityDateTo(java.lang.String)
 * CUSIPDataBuilder withIssuePartialMaturityFrom(java.lang.String)
 * CUSIPDataBuilder withIssuePartialMaturityTo(java.lang.String)
 * CUSIPDataBuilder withIssueRateFrom(java.lang.String)
 * CUSIPDataBuilder withIssueReleaseDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssueReleaseDateTo(java.lang.String)
 * CUSIPDataBuilder withIssueTypeCode(java.lang.String)
 * CUSIPDataBuilder withIssueUpdateType(java.lang.String)
 * CUSIPDataBuilder withIssuerDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssuerDescription(java.lang.String)
 * CUSIPDataBuilder withIssuerDescriptionStartsWith(java.lang.String)
 * CUSIPDataBuilder withIssuerDomicileCode(java.lang.String)
 * CUSIPDataBuilder withIssuerLogDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssuerLogDateTo(java.lang.String)
 * CUSIPDataBuilder withIssuerReleaseDateFrom(java.lang.String)
 * CUSIPDataBuilder withIssuerReleaseDateTo(java.lang.String)
 * CUSIPDataBuilder withIssuerStateCode(java.lang.String)
 * CUSIPDataBuilder withIssuerTypeCode(java.lang.String)
 * CUSIPDataBuilder withIssuerUpdateType(java.lang.String)
 * CUSIPDataBuilder withReturnIssuersOnlyFlag(java.lang.String)
 * CUSIPDataBuilder withMaxResults(int) 
 */

return idHubQueryBuilder
    .cusipData()
    .withIssueISINNumber("US0378331005")
    .doGet()
 