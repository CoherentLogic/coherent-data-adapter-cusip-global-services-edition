/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for cusip portfolio data.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * CUSIPPortfolioDataBuilder CUSIPPortfolioDataBuilder.withCUSIP(java.lang.String)
 * CUSIPPortfolioDataBuilder CUSIPPortfolioDataBuilder.withISIN(java.lang.String)
 * CUSIPPortfolioDataBuilder CUSIPPortfolioDataBuilder.withCUSIP6(java.lang.String)
 */

return idHubQueryBuilder
    .cusipPortfolioData()
    .withCUSIP("3128Q43H0")
    .doGet();
 