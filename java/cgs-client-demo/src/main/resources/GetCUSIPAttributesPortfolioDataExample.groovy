/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for cusip attributes
 * portfolio data.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * Methods available in the CUSIPAttributesPortfolioDataBuilder are:
 *
 * CUSIPAttributesPortfolioDataBuilder withCUSIP(java.lang.String)
 */

return idHubQueryBuilder
    .cusipAttributesPortfolioData()
    .withCUSIP("023135106")
    .doGet()