/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for the list of domiciles.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * Methods available in the LinkedIssuesDataBuilder are:
 *
 * LinkedIssuesDataBuilder com.coherentlogic.coherent.data.adapter.cgs.core.builders.LinkedIssuesDataBuilder.withCUSIP(java.lang.String)
 *
 */

return idHubQueryBuilder
    .linkedIssuesData()
    .withCUSIP("458140100")
    .doGet();