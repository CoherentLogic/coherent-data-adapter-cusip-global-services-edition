/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for CUSIP data (by ticker).
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * CUSIPDataForTickerBuilder CUSIPDataForTickerBuilder.withExchangeCode(java.lang.String)
 * CUSIPDataForTickerBuilder CUSIPDataForTickerBuilder.withTickerSymbol(java.lang.String)
 */

return idHubQueryBuilder
    .cusipDataForTicker()
    .withExchangeCode("C")
    .withTickerSymbol("MSFT")
    .doGet();