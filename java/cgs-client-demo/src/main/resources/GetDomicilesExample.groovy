/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for the list of domiciles.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * The DomicilesBuilder has the doGet method only.
 */

return idHubQueryBuilder
    .domiciles()
    .doGet()