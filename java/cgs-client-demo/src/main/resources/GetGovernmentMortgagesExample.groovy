/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for the list of domiciles.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * The GovernmentMortgagesBuilder has not been developed yet so no methods are available at this time.
 */

return idHubQueryBuilder
    .governmentMortgages()
    .withTBD () // See notes above.
    .doGet()