/*
 * This example, which is written in Groovy script, sends a request for data to authenticate with Cupip Global Services.
 *
 * You are required to provide your own userId and password in order to authenticate with CUSIP Global Services (CGS) --
 * if you do not have a license with CGS one can be acquired by emailing CGS at CGS_license@cusip.com.
 *
 * Methods available in the LoginBuilder are:
 *
 * LoginBuilder withPassword(java.lang.String)
 * LoginBuilder withUserId(java.lang.String)
 */

String userId = "Enter your userId here."

def result = authenticationQueryBuilder
    .login()
    .withUserId(userId)
    .withPassword("Enter your password here.")
    .doGet()

def binarySecurityToken = result.getBinarySecurityToken ()

/*
 * Note that the binary security token needs to be set exactly once on the idHubQueryBuilder -- we do this here.
 */
authenticationHolder.userId = userId
authenticationHolder.binarySecurityToken = binarySecurityToken

return binarySecurityToken