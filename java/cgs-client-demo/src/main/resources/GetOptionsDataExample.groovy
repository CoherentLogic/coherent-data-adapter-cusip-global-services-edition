/*
 * This example, which is written in Groovy script, sends a request to CUSIP Global Services for the list of domiciles.
 *
 * Note that a precondition for running this script is that the login script has executed successfully and the
 * resultant binarySecurityToken has been set on the queryBuilder.
 *
 * Methods available in the OptionsDataBuilder are:
 *
 * OptionsDataBuilder OptionsDataBuilder.withOccCode(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withStrikePrice(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withPromptDate(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withOptionCUSIP(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withOptionISIN(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withActiveContractsOnly(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withContractNameContains(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withContractNameStartsWith(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withPutCallIndicator(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withUnderlyingCUSIP(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withUnderlyingISIN(java.lang.String)
 * OptionsDataBuilder OptionsDataBuilder.withUnderlyingSymbol(java.lang.String)
 */

return idHubQueryBuilder
    .optionsData()
    .withOptionCUSIP("00C2R3GT8")
    .doGet();