package com.coherentlogic.coherent.data.adapter.cgs.demo;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;

import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesAuthenticationQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesIdHubQueryBuilder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AuthenticationHolder;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

import groovy.lang.Binding;
import net.miginfocom.swing.MigLayout;

/**
 * The front-end for the CGS Client that allows users to directly work with the
 * {@link com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesIdHubQueryBuilder}. 
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 *
 * "classpath*:spring/application-context.xml", 
 */
@SpringBootApplication
//@EnableAspectJAutoProxy
@ComponentScan(basePackages="com.coherentlogic.coherent.data.adapter.cgs")
public class CUSIPGlobalServicesClientGUI extends JFrame implements CommandLineRunner {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(CUSIPGlobalServicesClientGUI.class);

    private String binarySecurityToken;

    @Autowired
    private CUSIPGlobalServicesAuthenticationQueryBuilder authenticationQueryBuilder;

    @Autowired
    private CUSIPGlobalServicesIdHubQueryBuilder idHubQueryBuilder;

    @Autowired
    private AuthenticationHolder authenticationHolder;

    private final Map<String, String> exampleMap;

    private static final String
        WRAP = "wrap";

    private final String
        AUTHENTICATION_HOLDER = "authenticationHolder",
        AUTHENTICATION_QUERY_BUILDER = "authenticationQueryBuilder",
        ID_HUB_QUERY_BUILDER = "idHubQueryBuilder",
        LOG = "log",
        LOGIN = "Login",
        GET_CURRENCIES = "GetCurrencies",
        GET_CUSIP_ATTRIBUTES_PORTFOLIO_DATA = "GetCUSIPAttributesPortfolioData",
        GET_CUSIP_DATA = "GetCUSIPData",
        GET_CUSIP_DATA_FOR_TICKER = "GetCUSIPDataForTicker",
        GET_CUSIP_PORTFOLIO_DATA = "GetCUSIPPortfolioData",
        GET_STATES = "GetStates",
        GET_DOMICILES = "GetDomiciles",
        GET_GOVERNMENT_MORTGAGES = "GetGovernmentMortgages",
        GET_ISSUE_ATTRIBUTES = "GetIssueAttributes",
        GET_LINKED_ISSUES_DATA = "GetLinkedIssuesData",
        GET_OPTIONS_DATA = "GetOptionsData",
        GET_SBL_DATA = "GetSblData";

    private final JTextArea outputTextArea = new JTextArea();

    private final JButton runScriptButton = new JButton("Run script");

    private final ButtonGroup requestMenuItemsGroup = new ButtonGroup();

    private final Map<ButtonModel, JRadioButtonMenuItem> radioButtonMap =
        new HashMap<ButtonModel, JRadioButtonMenuItem> ();

    private final GroovyEngine groovyEngine;

    private final ObjectStringifier objectStringifier =
        new ObjectStringifier ();

    public CUSIPGlobalServicesClientGUI() throws IOException {
        this (new GroovyEngine(new Binding ()), new HashMap<String, String> ());
    }

    /**
     * @throws IOException 
     * @todo Remove the init method from the constructor.
     */
    public CUSIPGlobalServicesClientGUI(
        GroovyEngine groovyEngine,
        Map<String, String> exampleMap
    ) throws IOException {
        this.groovyEngine = groovyEngine;
        this.exampleMap = exampleMap;

        setTitle("CUSIP Global Services Client GUI");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initialize ();
    }

    String loadExample (String fileName) throws IOException {

        ClassPathResource resource = new ClassPathResource (fileName);

        InputStream inputStream = resource.getInputStream();

        StringWriter writer = new StringWriter();

        IOUtils.copy(inputStream, writer);

        String example = writer.toString();

        System.out.println("fileName: " + fileName + ", example: " + example);

        return example;
    }

    void addExamples () throws IOException {

        String loginExample = loadExample("LoginExample.groovy");
        String getCurrenciesExample = loadExample("GetCurrenciesExample.groovy");
        String getCUSIPAttributesPortfolioDataExample = loadExample("GetCUSIPAttributesPortfolioDataExample.groovy");
        String getCUSIPDataExample = loadExample("GetCUSIPDataExample.groovy");
        String getCUSIPDataForTickerExample = loadExample("GetCUSIPDataForTickerExample.groovy");
        String getCUSIPPortfolioDataExample = loadExample("GetCUSIPPortfolioDataExample.groovy");
        String getStatesExample = loadExample("GetStatesExample.groovy");
        String getDomicilesExample = loadExample("GetDomicilesExample.groovy");
        String getGovernmentMortgagesExample = loadExample("GetGovernmentMortgagesExample.groovy");
        String getIssueAttributesExample = loadExample("GetIssueAttributesExample.groovy");
        String getLinkedIssuesDataExample = loadExample("GetLinkedIssuesDataExample.groovy");
        String getOptionsDataExample = loadExample("GetOptionsDataExample.groovy");
        String getSblDataExample = loadExample("GetSBLDataExample.groovy");

        exampleMap.put(LOGIN, loginExample);
        exampleMap.put(GET_CURRENCIES, getCurrenciesExample);
        exampleMap.put(GET_CUSIP_ATTRIBUTES_PORTFOLIO_DATA, getCUSIPAttributesPortfolioDataExample);
        exampleMap.put(GET_CUSIP_DATA, getCUSIPDataExample);
        exampleMap.put(GET_CUSIP_DATA_FOR_TICKER, getCUSIPDataForTickerExample);
        exampleMap.put(GET_CUSIP_PORTFOLIO_DATA, getCUSIPPortfolioDataExample);
        exampleMap.put(GET_STATES, getStatesExample);
        exampleMap.put(GET_DOMICILES, getDomicilesExample);
        exampleMap.put(GET_GOVERNMENT_MORTGAGES, getGovernmentMortgagesExample);
        exampleMap.put(GET_ISSUE_ATTRIBUTES, getIssueAttributesExample);
        exampleMap.put(GET_LINKED_ISSUES_DATA, getLinkedIssuesDataExample);
        exampleMap.put(GET_OPTIONS_DATA, getOptionsDataExample);
        exampleMap.put(GET_SBL_DATA, getSblDataExample);
    }

    /**
     * @throws IOException 
     * @see #initialize()
     */
    void initializeMenu (JTextArea inputTextArea) throws IOException {

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnRequest = new JMenu("Examples");
        menuBar.add(mnRequest);

        JRadioButtonMenuItem login = new JRadioButtonMenuItem(LOGIN);
        login.setSelected(true);
        mnRequest.add(login);

        login.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem currencies =
            new JRadioButtonMenuItem(GET_CURRENCIES);
        mnRequest.add(currencies);

        currencies.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem cusipAttributesPortfolioData =
            new JRadioButtonMenuItem(GET_CUSIP_ATTRIBUTES_PORTFOLIO_DATA);
        mnRequest.add(cusipAttributesPortfolioData);

        cusipAttributesPortfolioData.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem cusipData = new JRadioButtonMenuItem(GET_CUSIP_DATA);
        mnRequest.add(cusipData);

        cusipData.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem cusipDataForTicker =
            new JRadioButtonMenuItem(GET_CUSIP_DATA_FOR_TICKER);
        mnRequest.add(cusipDataForTicker);

        cusipDataForTicker.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem cusipPortfolioData =
            new JRadioButtonMenuItem(GET_CUSIP_PORTFOLIO_DATA);
        mnRequest.add(cusipPortfolioData);

        cusipPortfolioData.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem states = new JRadioButtonMenuItem(GET_STATES);
        mnRequest.add(states);

        states.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem domiciles = new JRadioButtonMenuItem(GET_DOMICILES);
        mnRequest.add(domiciles);

        domiciles.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem governmentMortgages = new JRadioButtonMenuItem(GET_GOVERNMENT_MORTGAGES);
        mnRequest.add(governmentMortgages);

        governmentMortgages.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem issueAttributes = new JRadioButtonMenuItem(GET_ISSUE_ATTRIBUTES);
        mnRequest.add(issueAttributes);

        issueAttributes.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem linkedIssuesData = new JRadioButtonMenuItem(GET_LINKED_ISSUES_DATA);
        mnRequest.add(linkedIssuesData);

        linkedIssuesData.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem optionsData = new JRadioButtonMenuItem(GET_OPTIONS_DATA);
        mnRequest.add(optionsData);

        optionsData.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        JRadioButtonMenuItem sblData = new JRadioButtonMenuItem(GET_SBL_DATA);
        mnRequest.add(sblData);

        sblData.addActionListener(
            new MenuItemSelectedActionListener (exampleMap, inputTextArea));

        requestMenuItemsGroup.add(login);
        requestMenuItemsGroup.add(currencies);
        requestMenuItemsGroup.add(cusipAttributesPortfolioData);
        requestMenuItemsGroup.add(cusipData);
        requestMenuItemsGroup.add(cusipDataForTicker);
        requestMenuItemsGroup.add(cusipPortfolioData);
        requestMenuItemsGroup.add(states);
        requestMenuItemsGroup.add(domiciles);
        requestMenuItemsGroup.add(governmentMortgages);
        requestMenuItemsGroup.add(issueAttributes);
        requestMenuItemsGroup.add(linkedIssuesData);
        requestMenuItemsGroup.add(optionsData);
        requestMenuItemsGroup.add(sblData);

        radioButtonMap.put(login.getModel(), login);
        radioButtonMap.put(currencies.getModel(), currencies);
        radioButtonMap.put(cusipAttributesPortfolioData.getModel(), cusipAttributesPortfolioData);
        radioButtonMap.put(cusipData.getModel(), cusipData);
        radioButtonMap.put(cusipDataForTicker.getModel(), cusipDataForTicker);
        radioButtonMap.put(cusipPortfolioData.getModel(), cusipPortfolioData);
        radioButtonMap.put(states.getModel(), states);
        radioButtonMap.put(domiciles.getModel(), domiciles);
        radioButtonMap.put(governmentMortgages.getModel(), governmentMortgages);
        radioButtonMap.put(issueAttributes.getModel(), issueAttributes);
        radioButtonMap.put(linkedIssuesData.getModel(), linkedIssuesData);
        radioButtonMap.put(optionsData.getModel(), optionsData);
        radioButtonMap.put(sblData.getModel(), sblData);

//        JMenu mnAbout = new JMenu("Help");
//        menuBar.add(mnAbout);
//
//        JMenuItem mntmAbout = new JMenuItem("About");
//        mnAbout.add(mntmAbout);
    }

    /**
     * Method configures the Swing components that are added to this object's
     * JFrame.
     * @throws IOException 
     */
    public void initialize () throws IOException {

        addExamples();

        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        setExtendedState(Frame.MAXIMIZED_BOTH); 
        panel.setLayout(new MigLayout());

        JLabel enterYourQueryLabel = new JLabel(
            "Enter your query here (context contains references to: " +
            "queryBuilder, log):");

        panel.add(enterYourQueryLabel, WRAP);

        final JTextArea inputTextArea = new JTextArea();

        JScrollPane inputTextAreaScrollPane = new JScrollPane(inputTextArea);

        inputTextAreaScrollPane.
            setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        initializeMenu(inputTextArea);

        String exampleScript = exampleMap.get (LOGIN);

        log.info("exampleScript: " + exampleScript);

        inputTextArea.setText(exampleScript);

        inputTextArea.setColumns(80);
        inputTextArea.setRows(40);
        panel.add(inputTextAreaScrollPane, WRAP);

        panel.add(runScriptButton, WRAP);

        JLabel outputAppearsBelowLabel = new JLabel(
            "The output appears below:");

        panel.add(outputAppearsBelowLabel, WRAP);

        outputTextArea.setColumns(80);
        outputTextArea.setRows(40);

        JScrollPane outputTextAreaScrollPane = new JScrollPane(outputTextArea);

        outputTextAreaScrollPane.
            setVerticalScrollBarPolicy(
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        panel.add(outputTextAreaScrollPane, WRAP);

        GraphicsEnvironment env =
            GraphicsEnvironment.getLocalGraphicsEnvironment();

        Rectangle bounds = env.getMaximumWindowBounds();

        setBounds(bounds);

        runScriptButton.addActionListener(
            new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent actionEvent) {

                    String scriptText = inputTextArea.getText();

                    log.info("scriptText: " + scriptText);

                    ButtonModel buttonModel =
                        requestMenuItemsGroup.getSelection();

                    JRadioButtonMenuItem selectedMenuItem =
                        radioButtonMap.get(buttonModel);

                    String selectedText = selectedMenuItem.getText();

                    groovyEngine.setVariable(AUTHENTICATION_HOLDER, authenticationHolder);
                    groovyEngine.setVariable(AUTHENTICATION_QUERY_BUILDER, authenticationQueryBuilder);
                    groovyEngine.setVariable(ID_HUB_QUERY_BUILDER, idHubQueryBuilder);

                    groovyEngine.setVariable(LOG, LoggerFactory.getLogger("Groovy Script Log"));

                    Object result = null;

                    try {

                        result = groovyEngine.evaluate(scriptText);

                        log.info("selectedText: " + selectedText + ", result: " + result);

                        String stringifiedResult =
                            objectStringifier.toString(result);

                        outputTextArea.setText(stringifiedResult);

                    } catch (Throwable thrown) {
                        log.error("Script execution has failed -- details follow.", thrown);
                    }
                }
            }
        );
    }

    @Override
    public void run(String... args) throws Exception {
        setVisible(true);
    }

    public static void main(String[] args) {

        Monitor runMonitor = MonitorFactory.start("myFirstMonitorLabel");

        SpringApplication.run(CUSIPGlobalServicesClientGUI.class, args);

        runMonitor.stop();

        log.info("main: method ends; JAMON stats are as follows: " + runMonitor);
    }
}

/**
 * An {@link java.awt.event ActionListener} implementation that adds a given
 * example to the inputTextArea when the user selects a given
 * {@link javax.swing.JMenuItem}.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
class MenuItemSelectedActionListener implements ActionListener {

    private final Map<String, String> exampleMap;

    private final JTextArea inputTextArea;

    public MenuItemSelectedActionListener(
        Map<String, String> exampleMap,
        JTextArea inputTextArea
    ) {
        super();
        this.exampleMap = exampleMap;
        this.inputTextArea = inputTextArea;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        JMenuItem menuItem = (JMenuItem) actionEvent.getSource();

        String selectedMenu = menuItem.getText();

        String example = exampleMap.get(selectedMenu);

        inputTextArea.setText(example);
    }
}
