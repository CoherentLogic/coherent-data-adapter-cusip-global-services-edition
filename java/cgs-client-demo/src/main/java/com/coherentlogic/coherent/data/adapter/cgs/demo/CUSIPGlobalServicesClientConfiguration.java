package com.coherentlogic.coherent.data.adapter.cgs.demo;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.interceptor.AbstractMonitoringInterceptor;
import org.springframework.aop.interceptor.JamonPerformanceMonitorInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @see https://github.com/glyn/apress-spring-integration-examples/blob/master/monitoring/src/main/java/com/apress/prospringintegration/monitoring/MonitoringConfiguration.java
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
//@ComponentScan(basePackages="com.coherentlogic.coherent.data.adapter.cgs")
//@EnableAspectJAutoProxy
public class CUSIPGlobalServicesClientConfiguration {

//    @Bean
//    @Pointcut(value="execution(public com.coherentlogic.coherent.data.adapter.cgs.core.builders.LoginBuilder com.coherentlogic.coherent.data.adapter.cgs.core.builders.CUSIPGlobalServicesAuthenticationQueryBuilder.login())")
//    //@Pointcut(value="execution(public com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login com.coherentlogic.coherent.data.adapter.cgs.core.builders.LoginBuilder.doGet())")
//    public AbstractMonitoringInterceptor getPerformanceMonitorInterceptor () {
//
//        JamonPerformanceMonitorInterceptor performanceMonitorInterceptor =
//            new JamonPerformanceMonitorInterceptor (true, true);
//
//        return performanceMonitorInterceptor;
//    }

    

//    
//    public Object getPC () {
//
//        return null;
//    }

//    @Bean
//    public BeanNameAutoProxyCreator autoProxyCreator() {
//        BeanNameAutoProxyCreator proxyCreator = new BeanNameAutoProxyCreator();
//        proxyCreator.setBeanNames(new String[]{"processMessage"});
//        proxyCreator.setInterceptorNames(new String[]{"jamon"});
//        return proxyCreator;
//    }
}
/*
    <aop:config>
        <aop:pointcut id="performanceMonitoringPointcut"
         expression="execution(public com.coherentlogic.coherent.data.adapter.cgs.core.domain.Login com.coherentlogic.coherent.data.adapter.cgs.core.builders.LoginBuilder.doGet())"/>
        <aop:advisor pointcut-ref="performanceMonitoringPointcut"
            advice-ref="performanceMonitorInterceptor" />
    </aop:config>
*/
