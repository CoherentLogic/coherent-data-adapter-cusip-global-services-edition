package com.coherentlogic.coherent.data.adapter.cgs.demo.configuration;

import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Currencies;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.model.core.cache.MapCompliantCacheServiceProvider;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
public class DemoConfiguration {

    public static final String DEFAULT_CACHE = "defaultCache",
        CURRENCIES_CACHE_SERVICE_PROVIDER = "currenciesCacheServiceProvider";

//    private final DefaultCacheManager cacheManager = new DefaultCacheManager();
//
//    @Bean(name="currenciesCache")
//    public CacheServiceProviderSpecification<String, Currencies> getCurrenciesCacheServiceProvider() {
//
//        Cache<String, Currencies> currenciesCache = cacheManager.getCache("currenciesCache");
//
//        return new MapCompliantCacheServiceProvider<String, Currencies> (currenciesCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, CUSIPAttributesPortfolioData>
//        getCUSIPAttributesPortfolioDataCacheServiceProvider() {
//
//        Cache<String, CUSIPAttributesPortfolioData> cusipAttributesPortfolioDataCache =
//            cacheManager.getCache("attributesPortfolioData");
//
//        return new MapCompliantCacheServiceProvider<String, CUSIPAttributesPortfolioData> (
//            cusipAttributesPortfolioDataCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, CUSIPData> getCUSIPDataCacheServiceProvider() {
//
//        Cache<String, CUSIPData> cusipDataCache = cacheManager.getCache("cusipData");
//
//        return new MapCompliantCacheServiceProvider<String, CUSIPData> (cusipDataCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, CUSIPDataForTicker> getCUSIPDataForTickerCacheServiceProvider() {
//
//        Cache<String, CUSIPDataForTicker> cusipDataForTickerCache = cacheManager.getCache("cusipDataForTicker");
//
//        return new MapCompliantCacheServiceProvider<String, CUSIPDataForTicker> (cusipDataForTickerCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, CUSIPPortfolioData> getCUSIPPortfolioDataCacheServiceProvider() {
//
//        Cache<String, CUSIPPortfolioData> cusipPortfolioDataCache = cacheManager.getCache("cusipPortfolioData");
//
//        return new MapCompliantCacheServiceProvider<String, CUSIPPortfolioData> (cusipPortfolioDataCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, Domiciles>
//        getDomicilesCacheServiceProvider() {
//
//        Cache<String, Domiciles> domicilesCache = cacheManager.getCache("domiciles");
//
//        return new MapCompliantCacheServiceProvider<String, Domiciles> (domicilesCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, States> getStatesCacheServiceProvider() {
//
//        Cache<String, States> statesCache = cacheManager.getCache("states");
//
//        return new MapCompliantCacheServiceProvider<String, States> (statesCache);
//    }
//
//    @Bean
//    public CacheServiceProviderSpecification<String, IssueAttributes> getIssueAttributesCacheServiceProvider() {
//
//        Cache<String, IssueAttributes> issueAttributesCache = cacheManager.getCache("issueAttributes");
//
//        return new MapCompliantCacheServiceProvider<String, IssueAttributes> (issueAttributesCache);
//    }
}
