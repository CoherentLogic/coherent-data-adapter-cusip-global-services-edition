package com.coherentlogic.coherent.data.adapter.cgs.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * 
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
@ImportResource({"classpath*:spring/client-demo-application-context.xml"})
public class XMLConfiguration {

}
