package com.coherentlogic.coherent.data.adapter.cgs.core.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.coherentlogic.coherent.data.adapter.cgs.core.domain.AuthenticationHolder;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPAttributesPortfolioData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPDataForTicker;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPPortfolioData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.CUSIPTickerIssuerDetails;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Currencies;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.DealDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domicile;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.Domiciles;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.FacilityDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.GovernmentMortgageDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.GovernmentMortgages;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttribute;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueAttributes;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssueDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.IssuerDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.LinkedIssueDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.LinkedIssuesData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.OptionDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.OptionsData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.SBLData;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.StateDetail;
import com.coherentlogic.coherent.data.adapter.cgs.core.domain.States;

/**
 * Integration test for the {@link CUSIPGlobalServicesIdHubQueryBuilder} class.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/spring/application-context.xml")
@SpringBootApplication(scanBasePackages = {"com.coherentlogic.coherent.data.adapter.cgs"})
public class CUSIPGlobalServicesQueryBuilderTest {

    @Autowired
    private CUSIPGlobalServicesAuthenticationQueryBuilder cgsAuthenticationQueryBuilder;

    @Autowired
    private CUSIPGlobalServicesIdHubQueryBuilder idHubQueryBuilder;

    @Autowired
    private AuthenticationHolder authenticationHolder;

    private static final String USER_ID = System.getenv("CGS_USERID");

    private static final String PASSWORD = System.getenv("CGS_PASSWORD");

    @Before
    public void setUp () {

        // This is actually a unit test since we need the binarySecurityToken, otherwise there's no point even running
        // the other unit tests because they'll be guaranteed to fail. It would be good to run this test first however
        // JUnit doesn't guarantee the order by which tests are run, nor does it have a mechanism for specifying that
        // something needs to run first.

        String binarySecurityToken =
            cgsAuthenticationQueryBuilder
            .login()
            .withUserId(USER_ID)
            .withPassword(PASSWORD)
            .doGet()
            .getBinarySecurityToken();

        assertNotNull(binarySecurityToken);

        authenticationHolder.setUserId(USER_ID);
        authenticationHolder.setBinarySecurityToken(binarySecurityToken);
    }

    @After
    public void tearDown () {
        cgsAuthenticationQueryBuilder = null;
        idHubQueryBuilder = null;
    }

    @Test
    public void testCurrencies() {

        Currencies currencies = idHubQueryBuilder.currencies().doGet();

        assertTrue(currencies.getErrors().isEmpty());
        assertTrue(0 < currencies.getCurrencyDetails().size());
        assertEquals("Afghanistan", currencies.getCurrencyDetails().get(0).getDescription());
    }

    @Test
    public void testCUSIPAttributesPortfolioData() {

        String cusip = "023135106";

        CUSIPAttributesPortfolioData result =
            idHubQueryBuilder
                .cusipAttributesPortfolioData()
                .withCUSIP(cusip)
                .doGet();

        IssueAttribute issueAttribute = result.getIssueAttributes().get(0);

        assertTrue(issueAttribute.getBondForm().equals("Registered"));

        assertTrue(issueAttribute.getCUSIP().equals("02313510"));
    }

    @Test
    public void testCUSIPData() {

        CUSIPData cusipData = idHubQueryBuilder.cusipData().withIssueISINNumber("US0378331005").doGet();

        assertEquals(0, cusipData.getErrors().size());
        assertEquals(1, cusipData.getIssuerDetails().size());
        IssueDetail issueDetail = cusipData.getIssuerDetails().get(0).getIssueDetailList().get(0);
        assertEquals("333", issueDetail.getIssueCurrencyCode());
        assertEquals("E", issueDetail.getIssueTypeCode());
        IssuerDetail issuerDetail = cusipData.getIssuerDetails().get(0);
        assertTrue(issuerDetail.getIssuerAdditionalInfo().contains("APPLE"));
        assertEquals("C", issuerDetail.getIssuerTypeCode());
    }

    @Test
    public void testCUSIPDataForTicker() {

        CUSIPDataForTicker result = idHubQueryBuilder
            .cusipDataForTicker()
            .withExchangeCode("C")
            .withTickerSymbol("MSFT")
            .doGet();

        CUSIPTickerIssuerDetails issuerDetail = result.getCUSIPTickerIssuerDetails().get(0);

        Integer number = issuerDetail.getNumber();

        assertEquals (Integer.valueOf("594918"), number);

        assertTrue (issuerDetail.getDescription().contains("CORP"));
    }

    @Test
    public void testCusipPortfolioData() {

        CUSIPPortfolioData result = idHubQueryBuilder
            .cusipPortfolioData()
            .withCUSIP("3128Q43H0")
            .doGet();

        IssuerDetail issuerDetail = result.getIssuerDetails().get(0);

        assertTrue (issuerDetail.getIssuerLogDate().contains("2005"));

        String issueISINNumber = issuerDetail.getIssueDetailList().get(0).getIssueISINNumber();

        assertTrue (issueISINNumber.contains("312"));
    }

    @Test
    public void testDomiciles() {

        Domiciles domiciles = idHubQueryBuilder
            .domiciles()
            .doGet();

        Domicile domicile = domiciles.getDomiciles().get(0);

        assertEquals(260, domiciles.getDomiciles().size());

        assertEquals("016", domicile.getCode());
        assertEquals("Afghanistan", domicile.getDescription());
        assertEquals("AF ", domicile.getIsoCode());
    }

    @Test
    public void testStates() {

        States states = idHubQueryBuilder
            .states()
            .doGet();

        StateDetail stateDetail = states.getStateList().getStateDetails().get(0);

        assertEquals ((Long) 55L, states.getStateList().getStateCount());

        assertTrue (stateDetail.getCode().equals("AL"));
        assertTrue (stateDetail.getDescription().equals("Alabama"));
    }

    @Test
    public void testSBLData () {

        SBLData sblData = idHubQueryBuilder
          .sblData()
          .withBorrowerNameStartsWith("MASSACHUSETTS")
          .doGet();

        assertEquals(6, sblData.getDealDetails().size());

        DealDetail dealDetail = sblData.getDealDetails().get(5);

        assertTrue(dealDetail.getAgent().contains("B0"));
        assertTrue(dealDetail.getIssuerNumber().contains("75"));

        List<FacilityDetail> facilityDetails = dealDetail.getFacilityDetails();

        FacilityDetail facilityDetail = facilityDetails.get(1);

        assertEquals(2, facilityDetails.size());
        assertTrue(facilityDetail.getAmount().contains("000"));
        assertTrue(facilityDetail.getReleaseDate().contains("20"));
    }

    @Test
    public void testLinkedIssuesData () {

        LinkedIssuesData linkedIssuesData = idHubQueryBuilder
          .linkedIssuesData()
          .withCUSIP("458140100")
          .doGet();

        assertEquals(0, linkedIssuesData.getErrors().size());

        assertTrue(linkedIssuesData.getLinkedIssueDetails().size() == 2);

        LinkedIssueDetail linkedIssueDetail = linkedIssuesData.getLinkedIssueDetails().get(1);

        assertTrue(linkedIssueDetail.getLinkIssueDescription().contains("COM"));
        assertTrue(linkedIssueDetail.getLinkIssuerNumber().contains("581"));
    }

    @Test
    public void testGovernmentMortgagesData () {

        GovernmentMortgages governmentMortgages = idHubQueryBuilder
          .governmentMortgages()
          .withAgencyCode("FD")
          .withPoolNumber("R0-5501")
          .doGet();

        GovernmentMortgageDetail governmentMortgageDetail = governmentMortgages.getGovernmentMortgageDetails().get(0);

        assertEquals("FD", governmentMortgageDetail.getAgencyCode());
        assertEquals("333", governmentMortgageDetail.getIssueDomicileCode());
        assertEquals("R0-5501", governmentMortgageDetail.getPoolNumber());
    }

    @Test
    public void testGetOptionsData () {

        OptionsData optionsData = idHubQueryBuilder
            .optionsData()
            .withOptionCUSIP("00C2R3GT8")
            .doGet();

        assertEquals (0, optionsData.getErrors().size());

        int expectedSize = 6, actualSize = optionsData.getOptionDetails().size();

        assertEquals(expectedSize, actualSize);

        OptionDetail optionDetail = optionsData.getOptionDetails().get(1);

        assertEquals("Amend", optionDetail.getActionIndDesc());
        assertEquals("OCASPS", optionDetail.getUsCfiCode());
    }

    @Test
    public void testIssueAttributes () {

        IssueAttributes issueAttributes = idHubQueryBuilder.issueAttributes().withCUSIP("023135106").doGet();

        assertEquals(0, issueAttributes.getErrors().size());

        assertEquals("Registered", issueAttributes.getBondForm());
        assertEquals("Nasdaq", issueAttributes.getWhereTraded());
    }

//    @Test
//    public void testInternationalIdentifiers () {
//        // No longer supported at CGS.
//    }
}
